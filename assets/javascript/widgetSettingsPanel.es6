var WidgetSettingsPanel = class WidgetSettingsPanel extends React.Component {
  constructor(props){
    super(props);

    this.getClassFromSetting = this.getClassFromSetting.bind(this);
    this.save = this.save.bind(this);
    this.update = this.update.bind(this);
    this.isValid = this.isValid.bind(this);

    let settings = $.extend(true, {}, this.props.settings);
    Object.keys(this.props.settings).forEach(settingName => {
      settings[settingName].valid = false;
      settings[settingName]._settingName = settingName;
    });
    this.state = {settings};
  }

  getClassFromSetting(setting){
    let className = `WidgetSettings${setting.type}`;
    if(window[className]) return window[className];
    else throw new Error(`Tried to load component named ${className}, but it doesn't exist or couldn't be found`);
  }

  isValid(){
    return Object.keys(this.state.settings).every(setting => {return this.state.settings[setting].valid;});
  }

  save(){
    // clean
    let dirtySettings = $.extend(true, {}, this.state.settings);
    let cleanSettings = {};
    Object.keys(dirtySettings).forEach(settingName => {
      let {update, valid, _settingName, ...settings} = dirtySettings[settingName];
      cleanSettings[settingName] = settings;
    });
    this.props.updateSettings(cleanSettings);
    this.props.closeSettings();
  }

  update(setting){
    if(setting.type === undefined) throw new Error(`A component did not return a type field when updating. Check the list of components in this settings window.`);
    if(setting.valid === undefined) throw new Error(`Component ${setting.type} did not report whether its state was valid. In each update, an additional boolean field 'valid' must be returned.`);
    if(setting._settingName === undefined) throw new Error(`Component ${setting.type} sent an update without sharing its id in '_settingName'. This is required to apply the update.`);

    this.setState(s => {
      let settings = s.settings;
      settings[setting._settingName] = setting;

      return {settings: s.settings};
    });
  }

  render(){
    let elements = [],
      key = 0;

    Object.keys(this.state.settings).forEach(settingName => {
      let setting = this.state.settings[settingName];
      setting._settingName = settingName;
// jshint ignore:start
      let component = React.createElement(this.getClassFromSetting(setting), $.extend(true,{key: key++, update: this.update},setting));
// jshint ignore:end

      elements.push(component);
    });

    let buttonElements = [];
    let valid = this.isValid();
    buttonElements.push(React.createElement('div', {onClick: (valid ? this.save : null), className: `widget-settings-panel-save-button widget-settings-panel-button button ${valid ? '' : 'disabled'}`, key: key++}, 'Save'));
    buttonElements.push(React.createElement('div', {onClick: this.props.closeSettings, className: 'widget-settings-panel-button button', key: key++}, 'Cancel'));
    let buttons = React.createElement('div', {className: 'widget-settings-panel-buttons', key: key++}, buttonElements);

    let elementsContainer = React.createElement('div', {className: 'widget-settings-panel-elements-container'}, elements);
    let container = React.createElement('div', {className: 'widget-settings-panel-container'}, elementsContainer, buttons);

    return React.createElement(PopOutContainer, {
      content: container,
      additionalClassName: 'widget-settings-panel',
      dismissable: true,
      closePopup: this.props.closeSettings
    });

  }
};
