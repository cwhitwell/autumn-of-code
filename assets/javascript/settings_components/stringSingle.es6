var WidgetSettingsStringSingle = class WidgetSettingsStringSingle extends React.Component{
  constructor(props){
    super(props);
    this.changeEvent = this.changeEvent.bind(this);
    this.updateValue = this.updateValue.bind(this);
    this.update = this.update.bind(this);
    this.state = {
      value: this.props.value,
      valid: true
    };
  }

  update(){
    this.props.update($.extend(true,{},this.props,this.state));
  }

  componentDidMount(){
    this.update();
  }

  changeEvent(){
    this.updateValue(this.refs.stringSingle.value);
  }

  updateValue(newVal){
    this.setState({value:newVal},this.update);
  }

  render(){
    let elements = [], key = 0;

    elements.push(React.createElement('div', {className: 'widget-settings-stringsingle-caption', key: key++}, this.props.caption));

    let stringSingle = React.createElement('input',{
      key: "stringSingle",
      className:'stringSingle',
      placeholder: this.props.placeholder,
      ref: 'stringSingle',
      onChange: this.changeEvent,
      defaultValue: this.props.value
    });

    elements.push(React.createElement('div', {key:'stringsingle_2',className: 'optionConfig'},
      React.createElement('div', {key:'stringsingle_3',className: 'fieldContainer'},stringSingle)
    ));

    return React.createElement('div', {className: 'widget-settings-stringsingle ' + this.props.parentClass}, elements);
  }
};
