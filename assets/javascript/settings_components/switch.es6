var WidgetSettingsSwitch = class WidgetSettingsSwitch extends React.Component {
  constructor(props){
    super(props);

    this.update = this.update.bind(this);
    this.updateValue = this.updateValue.bind(this);

    this.state = {
      value: this.props.value,
      valid: true // Boths values are valid
    };
  }

  componentDidMount(){
    this.update();
  }

  update(){
    let merge = $.extend(true,{},this.props,this.state);
    this.props.update(merge);
  }

  updateValue(){
    this.setState(s => {return {value: !s.value};}, this.update);
  }

  render(){
    let elements = [],
      key = 0;

    elements.push(React.createElement('div', {className: 'widget-settings-swtich-caption', key: key++}, this.props.caption));
    elements.push(React.createElement(TickBoxSwitch, {default: this.state.value, captionOn: this.props.labelOn, captionOff: this.props.labelOff, updateValue: this.updateValue, renderSmall: true, key: key++}));

    return React.createElement('div', {className: 'widget-settings-switch'}, elements);
  }
};
