/*
    Example use:

    statusFilter:{
      type:'StatusSelector',
      caption:'Filter by status',
      statuses:{
        running: 'Running',
        paused: 'Paused',
        in_progress: 'In progress',
        published: 'Published',
        approved: 'Approved',
        editing: 'Editing',
        deleted: 'Deleted',
        expired: 'Expired',
        preview: 'Preview',
        other: 'Other'
      },
      value:{
        in_progress: true,
        editing: true,
        deleted: false,
        expired: false,
        approved: true,
        published: true,
        running: true,
        paused: true,
        preview: false,
        other: false
      }
    }
*/
var WidgetSettingsStatusSelector = class WidgetSettingsStatusSelector extends React.Component{
  constructor(props){
    super(props);
    this.toggleStatus = this.toggleStatus.bind(this);
    this.updateValue = this.updateValue.bind(this);
    this.update = this.update.bind(this);
    this.state = {
      value: this.props.value,
      valid: true
    };
  }

  update(){
    this.props.update($.extend(true,{},this.props,this.state));
  }

  componentDidMount(){
    this.update();
  }

  updateValue(newVal){
    this.setState({value:newVal},this.update);
  }

  toggleStatus(e){
    let clone = $.extend(true,{},this.state.value);
    clone[e] = !clone[e];
    this.updateValue(clone);
  }

  render(){
    let key = 0;
    let statusItems = [];
    for(let status in this.props.statuses){
      statusItems.push(React.createElement(StatusSelector, {
          key: status,
          selected: this.state.value[status],
          status: status,
          caption: this.props.statuses[status],
          toggleStatus: this.toggleStatus
      }));
    }
    let statusList = React.createElement('div', {className: 'status-list filter-list', key: 'status-list'}, statusItems);
    let caption = React.createElement('div', {className:'widget-settings-statusselector-caption'},this.props.caption);
    return React.createElement('div', {className: 'widget-settings-statusselector ' + this.props.parentClass},caption, statusList);
  }
};

var StatusSelector = class StatusSelector extends React.Component {
    constructor() {
        super();
        this.toggleStatus = this.toggleStatus.bind(this);
    }

    toggleStatus() {
        if(this.props.toggleStatus) this.props.toggleStatus(this.props.status);
    }

    render() {
        var button = React.createElement('div', {className: 'statusButton'+(this.props.selected?' selected':'') },React.createElement('img', {src: 'assets/images/core/icon_tick.png'}));
        var swatch = !this.props.disableSwatch ? React.createElement('div', {className: 'statusBlob ' + this.props.status }) : null;
        var label = React.createElement('div', {className: 'statusLabel'}, this.props.caption);
        return (React.createElement('div', { className: 'statusSelector clickable', onClick: this.toggleStatus}, button, swatch, label));
    }
};
