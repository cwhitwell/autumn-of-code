var WidgetSettingsColourPalette = class WidgetSettingsColourPalette extends React.Component{
  constructor(props){
    super(props);

    this.update = this.update.bind(this);
    this.selectColour = this.selectColour.bind(this);

    this.defaultPalette = ['ffb3ba', 'ffdfba', 'ffffba', 'baffc9', 'bae1ff'];

    this.state = {
      value: this.props.value,
      valid: true,
      palette: (props.palette === 'default' ? this.defaultPalette : props.palette)
    };

    this.update();
  }

  update(){
    this.props.update(Object.assign({},this.props,this.state));
  }

  selectColour(colour){
    this.setState({value: colour}, this.update);
  }

  render(){
    let elements = [],
      key = 0;

    elements.push(React.createElement('div', {className: 'widget-settings-colour-palette-caption', key: key++}, this.props.caption));

    let palette = [];
    this.state.palette.forEach(colour => {
      palette.push(React.createElement('div', {className: 'colour-palette-colour-container', key: key++}, React.createElement('div', {
        className: `colour-palette-colour ${this.props.value === colour ? 'colour-palette-colour-selected' : ''}`,
        style: {backgroundColor: `#${colour}`},
        onClick: () => this.selectColour(colour),
        key: key++
      })));
    });
    elements.push(React.createElement('div', {className: 'widget-settings-colour-palette-container', key: key++}, palette));

    return React.createElement('div', {className: 'widget-settings-colour-palette'}, elements);
  }
};
