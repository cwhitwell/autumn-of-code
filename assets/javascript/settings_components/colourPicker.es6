var WidgetSettingsColourPicker = class WidgetSettingsColourPicker extends React.Component{
  constructor(props){
    super(props);
    this.changeEvent = this.changeEvent.bind(this);
    this.updateValue = this.updateValue.bind(this);
    this.update = this.update.bind(this);
    this.state = {
      value: this.props.value,
      valid: true
    };
  }

  update(){
    this.props.update($.extend(true,{},this.props,this.state));
  }

  componentDidMount(){
    jscolor.installByClassName('jscolor');
    $('.jscolor').keydown(function(e) {
      if(e.keyCode === 27 || e.keyCode === 13 || e.keyCode === 9){
        e.target.jscolor.hide();
      }
    }.bind(this));
    this.update();
  }

  changeEvent(e){
    this.updateValue($(this.refs.colourPicker).val());
  }

  updateValue(newVal){
    this.setState({value:newVal},this.update);
  }

  render(){
    let elements = [], key = 0;

    elements.push(React.createElement('div', {className: 'widget-settings-colourpicker-caption', key: key++}, this.props.caption));
    let colourPicker = React.createElement('input', {key:key++, id:'colourPicker',className: 'jscolor', ref: 'colourPicker', required:false, defaultValue: this.state.value, onChange: this.changeEvent, onBlur:this.changeEvent});
    elements.push(React.createElement('div', {key:'color_2',className: 'optionConfig'},
      React.createElement('div', {key:'color_3',className: 'fieldContainer'},
        React.createElement('div', {key:'color_4'}, '#'),colourPicker)
      )
    );

    return React.createElement('div', {className: 'widget-settings-colourpicker'}, elements);
  }
};
