var WidgetSettingsLabel = class WidgetSettingsLabel extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      value: this.props.value,
      valid: true
    };
  }

  update(){
    this.props.update($.extend(true,{},this.props,this.state));
  }

  componentDidMount(){
    this.update();
  }
  render(){
    let elements = [], key = 0;

    elements.push(React.createElement('div', {className: 'widget-settings-label-caption', key: key++}, this.props.value));
    return React.createElement('div', {className: 'widget-settings-label ' + this.props.parentClass}, elements);
  }
};
