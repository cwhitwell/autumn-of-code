/*
  Example usage:
  viewType: {
    type: 'RadioSelect',
    caption: 'Data to display',
    options: [
      {item: 'Issuances', value: 'issuance', title: 'Issuances'},
      {item: 'Redemptions', value: 'redemption', title: 'Redemptions'},
      {item: 'Both', value: 'both', title: 'Both'}
    ],
    value: 'both' //default selected value
    parentClass: 'customClassName' //OPTIONAL
  }
*/

var WidgetSettingsRadioSelect = class WidgetSettingsRadioSelect extends React.Component{
  constructor(props){
    super(props);
    this.changeEvent = this.changeEvent.bind(this);
    this.updateValue = this.updateValue.bind(this);
    this.update = this.update.bind(this);
    this.state = {
      value: this.props.value,
      valid: true
    };
  }

  update(){
    this.props.update($.extend(true,{},this.props,this.state));
  }

  componentDidMount(){
    this.update();
  }

  changeEvent(id, value, valid, enabled){
    this.updateValue(value);
  }

  updateValue(newVal){
    this.setState({value:newVal},this.update);
  }

  render(){
    let elements = [], key = 0;

    elements.push(React.createElement('div', {className: 'widget-settings-radioselect-caption', key: key++}, this.props.caption));

    let selectSingle = React.createElement(SelectSingle,{
      key: "selectSingle",
      className:'selectSingle',
      ref: 'selectSingle',
      updateStatus: this.changeEvent,
      presentation:{
        display: 'radio',
        options: this.props.options
      },
      default: this.props.value,
      mandatory:true
    });

    elements.push(selectSingle);

    return React.createElement('div', {className: 'widget-settings-radioselect ' + this.props.parentClass}, elements);
  }
};
