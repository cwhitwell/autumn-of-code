var WidgetSettingsInteger = class WidgetSettingsInteger extends React.Component{
  constructor(props){
    super(props);
    this.updateValue = this.updateValue.bind(this);
    this.update = this.update.bind(this);
    this.changeEvent = this.changeEvent.bind(this);

    this.state = {
      value: this.props.value,
      valid: true
    };
  }

  changeEvent(id,value,valid,enabled){
    if(valid){
      this.updateValue(value);
    }
  }

  update(){
    this.props.update($.extend(true,{},this.props,this.state));
  }

  updateValue(newVal){
    this.setState({value:newVal},this.update);
  }

  render(){
    let elements = [], key = 0;

    elements.push(React.createElement('div', {className: 'widget-settings-integer-caption', key: key++}, this.props.caption));

    let integer = React.createElement(Integer,{
      key: key++,
      default: this.state.value,
      presentation: {
        display: 'text'
      },
      id: ' integer-component',
      mandatory: true,
      constraints: {
        minValue: this.props.minValue,
        maxValue: this.props.maxValue
      },
      readOnly: this.props.readOnly,
      updateStatus: this.changeEvent
    });
    elements.push(integer);
    return React.createElement('div', {className: 'widget-settings-integer'}, elements);
  }
};
