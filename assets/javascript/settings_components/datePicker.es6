/*
    Example use:

    dateFrom: {
      caption: 'Start date',
      type: 'DatePicker',
      value: '01/08/18'
    }
  
*/

var WidgetSettingsDatePicker = class WidgetSettingsDatePicker extends React.Component{
  constructor(props){
    super(props);
    this.changeEvent = this.changeEvent.bind(this);
    this.updateValue = this.updateValue.bind(this);
    this.update = this.update.bind(this);
    this.state = {
      value: this.props.value,
      valid: true
    };
  }

  update(){
    this.props.update($.extend(true,{},this.props,this.state));
  }

  componentDidMount(){
    this.update();
  }

  changeEvent(id,value,valid,enabled){
    this.updateValue(value);
  }

  updateValue(newVal){
    this.setState({value:newVal},this.update);
  }

  render(){
    let elements = [], key = 0;

    elements.push(React.createElement('div', {className: 'widget-settings-datefield-caption', key: key++}, this.props.caption));

    let dateField = React.createElement(DateField,{
      key: 'dateField',
      name:'Date',
      default:this.props.value,
      presentation:{
        format:'dd/mm/yy'
      },
      mandatory:true,
      enabled:true,
      updateStatus:this.changeEvent
    });

    elements.push(dateField);

    return React.createElement('div', {className: 'widget-settings-datepicker ' + this.props.parentClass}, elements);
  }
};
