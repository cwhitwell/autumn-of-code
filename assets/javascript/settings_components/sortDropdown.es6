/*
  Example use:
  ------------
  fontSize: {
    type: 'SortDropdown',
    caption: 'Font Size',
    index: 0,
    value: 'small',
    parentClass: 'FontSizeParent',
    items: [
      {caption:"Small Text",value:'small'},
      {caption:"Medium Text",value:'medium'},
      {caption:"Large Text",value:'large'},
    ],
    remoteMethod: 'GET',                                          **OPTIONAL**
    remoteURL: 'db_home/api/getTimezoneData?group_offsets=true',  **OPTIONAL**
    remoteHandler: WidgetClockTimezoneHandler.toString()          **OPTIONAL** Must be serialised
  }
*/

var WidgetSettingsSortDropdown = class WidgetSettingsSortDropdown extends React.Component{
  constructor(props){
    super(props);
    this.changeEvent = this.changeEvent.bind(this);
    this.updateValue = this.updateValue.bind(this);
    this.update = this.update.bind(this);
    this.getRemoteData = this.getRemoteData.bind(this);

    /*
      NOTE: The serialised function 'remoteHandler' is deserialised using the Function constructor. This runs the method
      in an isolated context, without access to local variables outside of its own code block. This, combined with invoking
      the method via Function.prototype.call() (setting variable 'this' to null) means that any serialised code is unlikely
      to pose a risk, as it is essentially the same as running a code block from the JS dev console.
    */

    this.remoteHandler = new Function('return ' + this.props.remoteHandler)();

    this.state = {
      value: this.props.value,
      valid: true,
      remoteList: null,
      remoteError: null
    };
  }

  update(){
    this.props.update($.extend(true,{},this.props,this.state));
  }

  getRemoteData(){
    $.ajax({
      method: (this.props.remoteMethod!=null?this.props.remoteMethod:'GET'),
      url: this.props.remoteURL,
      success: (e) => {
        let data = this.remoteHandler.call(null,e);
        this.setState({remoteList:data.list,remoteError:data.error,valid:(data.error==null),test:function(){return 1;}},this.update);
      }
    }).fail((e) => {
      let data = this.remoteHandler.call(null,e);
      this.setState({remoteList:data.list,remoteError:data.error,valid:(data.error==null)},this.update);
    });
  }

  componentDidMount(){
    if(this.props.remoteURL!=null && this.remoteHandler!=null){
      this.getRemoteData();
    }
    this.update();
  }

  changeEvent(obj){
    let items = (this.state.remoteList!=null?this.state.remoteList:this.props.items);
    this.updateValue(obj.value,items.indexOf(obj));
  }

  updateValue(newVal,index){
    this.setState({value:newVal,index:index},this.update);
  }

  render(){
    let elements = [], key = 0;

    elements.push(React.createElement('div', {className: 'widget-settings-sortdropdown-caption', key: key++}, this.props.caption));
    let remoteMode = (this.props.remoteURL!=null && this.remoteHandler!=null);
    let items = this.props.items;
    let index = this.props.index;

    if(remoteMode){
      if(this.state.remoteList!=null){
        items = this.state.remoteList;
      }else if(this.state.remoteError!=null){
        console.error(this.state.remoteError);
        items = [{caption:'Error loading remote data!', value:null}];
        index = 0;
      }else{
        items = [{caption:'Loading...', value:null}];
        index = 0;
      }
    }else if(items.length<1) {
      if(this.state.valid){
        this.setState({valid:false},this.update);
      }
      items = [{caption:'Error no items found!', value:null}];
    }



    let sortDropdown = React.createElement(SortDropdown,{
      key: "dropper",
      preSelectedOptionIndex: (items[index]!=null?index:0),
      sortFunctions: items,
      callback: this.changeEvent
    });

    elements.push(React.createElement('div', {key:'dropper_2',className: 'optionConfig'},
      React.createElement('div', {key:'dropper_3',className: 'fieldContainer'},sortDropdown)
    ));

    return React.createElement('div', {className: 'widget-settings-sortdropdown ' + this.props.parentClass}, elements);
  }
};
