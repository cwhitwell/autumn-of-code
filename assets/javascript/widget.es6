var Widget = class Widget extends React.Component {
  constructor(props){
    super(props);
    if(props === undefined) throw new Error(`Widgets must provide the widget superclass with props from the constructor. Check your widget named ${this.constructor.name}.`)

    this.save = this.save.bind(this);
    this.close = this.close.bind(this);
    this.getErrorObject = this.getErrorObject.bind(this);
    this.clearError = this.clearError.bind(this);
    this.throwError = this.throwError.bind(this);
    this.activeError = this.activeError.bind(this);
    this.resizeEvent = this.resizeEvent.bind(this);
    this.widgetDidUpdate = this.widgetDidUpdate.bind(this);
    this.widgetDidMount = this.widgetDidMount.bind(this);
    this.widgetWillUnmount = this.widgetWillUnmount.bind(this);
    this.renderWidget = this.renderWidget.bind(this);
    this.shouldWidgetUpdate = this.shouldWidgetUpdate.bind(this);

// jshint ignore:start
    this.state = props.savedState ? $.extend(true,{},props.savedState) : undefined;
// jshint ignore:end
  }

  shouldComponentUpdate(nextProps, nextState){
    if((nextState && (nextState._error !== this.state._error || nextState._errorMessage !== this.state._errorMessage)) || this.shouldWidgetUpdate(nextProps,nextState)){
      return true;
    }else{
      return false;
    }
  }
  shouldWidgetUpdate(){return true;}

  resizeEvent(e){
    this.onWidgetResize(e);
  }

  onWidgetResize(){}
  // Widgets should call custom methods, to allow core class to do any general steps
  componentDidMount(){
    this.widgetDidMount();
  }
  widgetDidMount(){}

  componentWillUnmount(){
    this.widgetWillUnmount();
  }
  widgetWillUnmount(){}

  componentDidUpdate(){
    this.widgetDidUpdate();
  }
  widgetDidUpdate(){}

  save(state){
    state = $.extend(true, {}, state);
    this.props.saveWidget({
      id: this.props.id,
      state
    });
  }

  close(id = this.props.id){
    this.props._close(id);
  }

  throwError(message = ""){
    this.setState({_error: true, _errorMessage: message});
  }

  clearError(){
    this.setState({_error: false, _errorMessage: undefined});
  }

  getErrorObject(){
    let message = this.state._errorMessage || "";
    return React.createElement(WidgetError, {message, id: this.props.id, close: this.close});
  }

  activeError(){
    return this.state && this.state._error;
  }

  render (){
    // Errors
    if(this.activeError()){
      return this.getErrorObject();
    }
    return this.renderWidget();
  }
};

// Acts as an error boundary, and inserts standard edit components while in edit mode
var WidgetContainer = class WidgetContainer extends React.Component {
  constructor(){
    super();

    this.getWidgetSize = this.getWidgetSize.bind(this);
    this.isInView = this.isInView.bind(this);

    this.canDrag = false;
    this.widgetSize = {};

    this.state = {hasError: false};
  }

  componentDidCatch(error, info){
    // Display fallback UI
    this.setState({hasError: true});
  }

  useStandardProps(customProps){
    let defaultProps = {
      className: ''
    };

    let classProps = {
      className: this.props.className,
      onMouseDown: this.props.onMouseDown,
      onMouseUp: this.props.onMouseUp,
      onTouchEnd: this.props.onTouchEnd,
      onTouchStart: this.props.onTouchStart,
      style: this.props.style
    } || {};
    let mergeProps = {};

    // Specific merges
    let providedPropClassNames = (classProps && classProps.className) ? classProps.className.split(' ') : '';
    let propClassNames = defaultProps.className.split(' ');
    let customPropClassNames = (customProps && customProps.className) ? customProps.className.split(' ') : '';
    mergeProps.className = [...new Set(propClassNames.concat(customPropClassNames, providedPropClassNames))].join(' ');

    if(customProps) return Object.assign({}, classProps, defaultProps, customProps, mergeProps);
    return Object.assign({}, classProps, defaultProps, mergeProps);
  }

  getWidgetSize(){
    let widget = this.refs.widgetContainer;
    if(widget!=null){
      if(widget.clientWidth!=null && widget.clientHeight!=null){
        let size = {width: widget.clientWidth, height:widget.clientHeight};
        if(!deepEqual(size,this.widgetSize) && this.props.widgetRef!=null && this.props.widgetRef.hasOwnProperty('resizeEvent')){
          this.widgetSize = $.extend(true,{},size);
          this.props.widgetRef.resizeEvent(size);
        }
      }
    }
  }

  componentDidUpdate(){
    this.getWidgetSize();
  }

  componentDidMount(){
    this.getWidgetSize();
    /* Set resize event handlers */
    this.widgetSize = {};
    let resizeHandle = this.refs.widgetContainer.querySelector('.react-resizable-handle');
    let windowElement = $(window);
    if(resizeHandle!=null){
      resizeHandle.onmousedown = (e) => { this.canDrag = true; };
      windowElement.mouseup((e) => { this.canDrag = false; });
      windowElement.mousemove((e) => {
        if(this.canDrag){
          this.getWidgetSize();
        }
      });
    }

    if(this.props.new){
      let widgetBounds = this.refs.widgetContainer.getBoundingClientRect();
      if(!this.isInView(widgetBounds)){
        setTimeout(() => {
          this.refs.widgetContainer.scrollIntoView({ block: 'end',  behavior: 'smooth' });
          setTimeout(() => {this.props.disableAddScreenTransparency();}, 1000);
        }, 220);
      }
      else setTimeout(() => {this.props.disableAddScreenTransparency();}, 1000);
    }
  }

  isInView(widgetBounds){
    return widgetBounds.top >= 0 &&
    widgetBounds.left >= 0 &&
    widgetBounds.bottom <= (window.innerHeight) &&
    widgetBounds.right <= (window.innerWidth);
  }

  render(){
    let menuObjects = [],
      key = 0;

    if(this.props.editMode){
      menuObjects.push(React.createElement('img', {
        className: 'widget-icon widget-icon-close',
        onClick: () => {this.props.removeBlock(this.props.id);},
        src: 'assets/images/core/dashboard_icon_close.png',
        key: key++
      }));
      if(this.props.toggleSettings){
        menuObjects.push(React.createElement('img', {
          className: 'widget-icon widget-icon-options',
          onClick: () => this.props.toggleSettings(this.props.id),
          src: 'assets/images/core/dashboard_icon_cog.png',
          key: key++
        }));
      }
    }
    if (this.state.hasError){
      return React.createElement('div', this.useStandardProps({className: 'widget-container'}),
        React.createElement(WidgetError, {close: () => this.props.removeBlock(this.props.id)})
      );
    }
    else {
      return React.createElement('div', this.useStandardProps({className: `widget-container ${(this.props.new && this.props.addScreenOpen) ? 'widget-container-new' : ' '}`, ref:'widgetContainer'}), this.props.children, menuObjects);
    }
  }
};
