var ChangeTracker = class ChangeTracker extends React.Component {

  constructor(){
    super();

    this.getSpinnerGif = this.getSpinnerGif.bind(this);
    this.getText = this.getText.bind(this);
  }

  getSpinnerGif(){
      let src = '';
      return src;
  }

  getText(){
    //Replace with locale text
    let text = false;

    if(!this.props.saving && !this.props.changed) text = "OnPoint Dashboard - Standalone Mode";
    if(this.props.saving || this.props.changed) text = "Saving...";

    return text;
  }

  render(){
    let elements = [], key = 0;

    let canSave = LocalStorageObj().canUse;

    if(this.props.changed && canSave){
      elements.push(React.createElement('img', {src: 'assets/images/core/small_spinner.gif', className: 'change-tracker-spinner', key: key++}));
    }

    let text = this.getText();
    if(text) elements.push(React.createElement('div', {className: 'change-tracker-text ' + (!canSave ? 'error-text ' : ''), key: key++}, (canSave ? text : 'Unable to save Dashboard. Browser does not support local storage.')));

    return React.createElement('div', {className: 'change-tracker'}, elements);
  }
};
