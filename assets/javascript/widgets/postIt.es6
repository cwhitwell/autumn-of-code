var PostIt = class PostIt extends Widget {
  constructor(props){
    super(props);

    this.localSave = this.localSave.bind(this);
    this.getColour = this.getColour.bind(this);
    this.getRandomColour = this.getRandomColour.bind(this);
    this.getTextColour = this.getTextColour.bind(this);
    this.setColour = this.setColour.bind(this);
    this.updateData = this.updateData.bind(this);
    this.toggleEditing = this.toggleEditing.bind(this);
    this.getTarget = this.getTarget.bind(this);

    this.state = $.extend({text: '', colour: this.getColour(), editing: false}, this.state);
  }

  localSave(){
    this.save({text: this.state.text, colour: this.state.colour});
  }

  getColour(){
    return '#' + this.props.userSettings.backgroundColor.value.replace('#','');
  }

  setColour(bg){
    let newBg = '#' + bg.replace('#','');
    if(this.state.colour!=newBg){
      this.setState({colour: newBg});
    }
  }

  widgetDidUpdate(){
    this.setColour(this.getColour());
  }

  widgetDidMount(){
    this.setColour(this.getColour());
  }

  getRandomColour(){
    return '#' + ('00000'+(Math.random()*(1<<24)|0).toString(16)).slice(-6);
  }

  getColourLum(hex){
    let relativeLuminanceW3C =  function (R8bit, G8bit, B8bit) {
      //Credit: https://gist.github.com/jfsiii/5641126
      var RsRGB = R8bit/255;
      var GsRGB = G8bit/255;
      var BsRGB = B8bit/255;

      var R = (RsRGB <= 0.03928) ? RsRGB/12.92 : Math.pow((RsRGB+0.055)/1.055, 2.4);
      var G = (GsRGB <= 0.03928) ? GsRGB/12.92 : Math.pow((GsRGB+0.055)/1.055, 2.4);
      var B = (BsRGB <= 0.03928) ? BsRGB/12.92 : Math.pow((BsRGB+0.055)/1.055, 2.4);

      // For the sRGB colorspace, the relative luminance of a color is defined as:
      var L = 0.2126 * R + 0.7152 * G + 0.0722 * B;

      return L;
    };

    let hexToRgb = function(hex) {
      var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
      return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
      } : null;
    };

    let rgb = hexToRgb(hex);
    if(rgb!=null){
      return relativeLuminanceW3C(rgb.r,rgb.g,rgb.b);
    }
  }

  updateData(){
    this.setState({
      text: this.refs.textarea.value
    }, this.localSave);
  }

  getTextColour(bgHex){
    let bgLum = this.getColourLum(bgHex);
    return ((bgLum > Math.sqrt(1.05 * 0.05))?'#000000':'#FFFFFF');
  }

  updateTextareaSelection(value){
    this.refs.textarea.setSelectionRange(value, value);
  }

  getTarget(){
    return this.refs.textarea!=null ? this.refs.textarea : this.refs.div;
  }

  toggleEditing(t){
    let offset;

    if(!this.state.editing){
      let divContent = window.getSelection();
      let text = (divContent.anchorNode!=null ? divContent.anchorNode.nodeValue : '');
      offset = divContent.anchorOffset + this.state.text.indexOf(text);
    }

    this.setState(s => {return {editing: !s.editing, scrollTop: this.getTarget().scrollTop};},
      () => {if(this.state.editing){
        this.refs.textarea.focus();
        this.updateTextareaSelection(offset);
      }
      this.getTarget().scrollTop = this.state.scrollTop;
    });
  }

  renderWidget(){
    let settingFontSize = this.props.userSettings.fontSize;
    let settingTextAlign = this.props.userSettings.textAlign;
    let textStyle = {
      color: this.getTextColour(this.state.colour),
      borderColor: this.getTextColour(this.state.colour),
      fontSize: (settingFontSize!=null?settingFontSize.value:16) + 'pt',
      textAlign: (settingTextAlign!=null?settingTextAlign.value:'left')
    };

    let content;
    if(this.state.editing){
      content = React.createElement('textarea',{
        className: 'widget-postit-editing widget-postit-content widget-item-textarea',
        ref: 'textarea',
        style: textStyle,
        value: this.state.text,
        onChange: this.updateData,
        onBlur: this.toggleEditing,
        placeholder: "Type a note..."
      });
    }
    else {
      content = React.createElement('div', {
        className: 'widget-postit-viewing widget-postit-content',
        ref: 'div',
        style: textStyle,
        onClick: this.toggleEditing
      }, React.createElement(Linkify, {}, (this.state.text === '' ? "Type a note..." : this.state.text)));
    }

    return React.createElement('div', {
      style: {
        backgroundColor: this.state.colour
      },
      className: 'widget-postit'
    }, content);
  }
};

// Widget Settings
PostIt.config = {
  maxH: 11,
  maxW: 16,
  minH: 4,
  minW: 6,
  userSettings: {
    backgroundColor: {
      caption: 'Background colour',
      type: 'ColourPalette',
      palette: 'default',
      value: 'ffffba'
    },
    fontSize: {
      type: 'SortDropdown',
      caption: 'Font Size',
      value: 16,
      index: 1,
      items: [
        {caption:'Small', value: 12},
        {caption: 'Medium', value: 16},
        {caption: 'Large', value: 20},
        {caption: 'Extra Large', value: 26}
      ]
    },
    textAlign: {
      type: 'SortDropdown',
      caption: 'Align Text',
      value: 'left',
      index: 0,
      items: [
        {caption:'Left', value: 'left'},
        {caption: 'Right', value: 'right'},
        {caption: 'Center', value: 'center'}
      ]
    }
  }
};

//Widget default values - these will override other settings
PostIt.defaults = {
  h: 4,
  w: 6
};
