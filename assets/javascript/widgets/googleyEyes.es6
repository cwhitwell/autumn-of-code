var GoogleyEyes = class GoogleyEyes extends Widget{

  constructor(props){
    super(props);
  }

  randomColour(){
    return '#' + ('00000'+(Math.random()*(1<<24)|0).toString(16)).slice(-6);
  }

  renderWidget(){
    let isRandom = this.props.userSettings.randomColour.value;
    let leftEyeColour = '#' + this.props.userSettings.leftRingColour.value;
    let leftPupilColour = '#' + this.props.userSettings.leftPupilColour.value;
    let rightEyeColour = '#' + this.props.userSettings.rightRingColour.value;
    let rightPupilColour = '#' + this.props.userSettings.rightPupilColour.value;
    let mouthColour = '#' + this.props.userSettings.mouthColour.value;

    let leftEye = React.createElement(EyeRing,{key:'LeftEye', type:'left', colour:(isRandom?this.randomColour():leftEyeColour), pupilColour:(isRandom?this.randomColour():leftPupilColour)});
    let rightEye = React.createElement(EyeRing,{key:'RightEye', type:'right', colour:(isRandom?this.randomColour():rightEyeColour), pupilColour:(isRandom?this.randomColour():rightPupilColour)});
    let mouth = React.createElement(Mouth,{key:'Mouth',colour:(isRandom?this.randomColour():mouthColour)});
    let eyesParent = React.createElement('div',{className:'eyesParent'},leftEye,rightEye);
    let parent = React.createElement('div',{className:'parent'},eyesParent,mouth);
    return React.createElement('div',{className:'GoogleyEyes'},parent);
  }
};

var Mouth = class Mouth extends React.Component{
  constructor(props){
    super(props);
    this.svgLoad = this.svgLoad.bind(this);
    this.setColour = this.setColour.bind(this);
    this.state = {
      svgDoc: null,
      colour: null
    };
    this.active = false;
  }

  svgLoad(e){
    let doc = e.target.contentDocument;
    if(this.active) this.setState({svgDoc:doc});
  }

  componentWillUnmount(){
    this.active = false;
  }

  componentDidMount(){
    this.active = true;
    this.setColour(this.props.colour);
  }

  componentDidUpdate(){
    this.setColour(this.props.colour);
  }

  setColour(colour){
    if(this.state.svgDoc!=null && this.state.colour!=colour && colour!=null){
      let collection = this.state.svgDoc.getElementsByClassName('svgObj');
      for(let i=0;i<collection.length;i++){
        let cur = collection[i];
        cur.setAttribute('fill',colour);
        cur.setAttribute('stroke',colour);
        if(this.active) this.setState({colour:colour});
      }
    }
  }

  render(){
    return React.createElement('div',{className:'mouthParent'},React.createElement('object',{className:'mouth',data:'assets/images/db_home/googley_mouth_1.svg',onLoad:this.svgLoad}));
  }
};

var EyeRing = class EyeRing extends React.Component{
  constructor(props){
    super(props);
    this.getSize = this.getSize.bind(this);
    this.svgLoad = this.svgLoad.bind(this);
    this.state = {
      uuid:Math.floor(Math.random()*Math.random()*1000),
      size: null,
      svgDoc: null,
      colour: null
    };
    this.active = false;
  }

  componentWillUnmount(){
    this.active = false;
  }

  getSize(){
    let size = $(this.refs.eyeRing).width();
    if(size != this.state.size && this.active){
      this.setState({size:size});
    }
  }
  componentDidUpdate(){
    this.setColour(this.props.colour);
    this.getSize();
  }
  componentDidMount(){
    this.active = true;
    this.setColour(this.props.colour);
    this.getSize();
  }

  svgLoad(e){
    let doc = e.target.contentDocument;
    if(this.active) this.setState({svgDoc:doc});
  }

  setColour(colour){
    if(this.state.svgDoc!=null && this.state.colour!=colour && colour!=null){
      let collection = this.state.svgDoc.getElementsByClassName('svgObj');
      for(let i=0;i<collection.length;i++){
        let cur = collection[i];
        cur.setAttribute('fill',colour);
        cur.setAttribute('stroke',colour);
        if(this.active) this.setState({colour:colour});
      }
    }
  }

  render(){
    let type = this.props.type;
    let uuid = this.state.uuid;
    let pupil = React.createElement(EyeBall,{key:type+'EyeBall',size:this.state.size,uuid:uuid+type,colour:this.props.pupilColour});
    return React.createElement('div',{className:'eyeParent', id:uuid+type+'_eyeParent'},
      React.createElement('object',{type:"image/svg+xml",className:'eyeRing', onLoad:this.svgLoad, data:'assets/images/db_home/eye_ring_1.svg',ref:'eyeRing',id:uuid+type+'_eyeRing'}),pupil
    );
  }
};

var EyeBall = class EyeBall extends React.Component{
  constructor(props){
    super(props);
    this.getEye = this.getEye.bind(this);
    this.moveEye = this.moveEye.bind(this);
    this.svgLoad = this.svgLoad.bind(this);
    this.setColour = this.setColour.bind(this);

    this.state = {
      pos:{top:null,left:null},
      mousePos:{x:null,y:null},
      svgDoc: null,
      colour: null
    };

    this.active = false;
  }

  isInside(pupilX,pupilY,pupilR,ringX,ringY,ringR,mousePos){
    let isInside = true;
    for(let i=0; i<=360; i++){
      let r = pupilR;
      let xCalc = r * Math.cos(i * Math.PI/180);
      let yCalc = r * Math.sin(i * Math.PI/180);
      let x = (xCalc<0?r-(xCalc*-1):r+xCalc);
      let y = (yCalc<0?(yCalc*-1)+r:r-yCalc);
      let absX = Math.floor(pupilX + x);
      let absY = Math.floor(pupilY + y);

      let evalx = Math.pow((absX - (ringX+ringR)),2);
      let evaly = Math.pow((absY - (ringY+ringR)),2);
      if((evalx+evaly >= Math.pow(ringR,2))){
        isInside = false;
        break;
      }
    }

    return isInside;
  }

  componentDidMount(){
    this.active = true;
    this.setColour(this.props.colour);
    let onMove = function(e){
      if(this.active) this.setState({mousePos:{x:Math.floor(e.clientX),y:Math.floor(e.clientY)}});
    }.bind(this);

    $('body').mousemove(onMove);
    $(this.refs.objectWrap).mousemove(onMove);
  }

  componentWillUnmount(){
    this.active = false;
    clearTimeout(this.timer);
  }

  svgLoad(e){
    let doc = e.target.contentDocument;
    if(this.active) this.setState({svgDoc:doc});
  }

  componentDidUpdate(){
    this.setColour(this.props.colour);
    clearTimeout(this.timer);
    this.timer = setTimeout(this.moveEye,5);
  }

  shouldComponentUpdate(nextProps,nextState){
    let posChange = (this.state.pos.top != nextState.pos.top || this.state.pos.left != nextState.pos.left);
    let mouseChange = (this.state.mousePos.x != nextState.mousePos.x || this.state.mousePos.y != nextState.mousePos.y);
    if(posChange || mouseChange || this.props.size!=nextProps.size || !deepEqual(this.state.svgDocs,nextState.svgDocs)){
      return true;
    }else{
      return false;
    }
  }

  setColour(colour){
    if(this.state.svgDoc!=null && this.state.colour!=colour && colour!=null){
      let collection = this.state.svgDoc.getElementsByClassName('svgObj');
      for(let i=0;i<collection.length;i++){
        let cur = collection[i];
        cur.setAttribute('fill',colour);
        cur.setAttribute('stroke',colour);
        if(this.active) this.setState({colour:colour});
      }
    }
  }

  moveEye(){
    try{
      let mousePos = this.state.mousePos;
      let eyeRing = $(`#${this.props.uuid}_eyeParent #${this.props.uuid}_eyeRing`);
      var eyePupil = $(this.refs.eyePupil);
      let pupilR = eyePupil.width()/2;
      let pupilX = eyePupil.offset().left;
      let pupilY = eyePupil.offset().top;

      let margin = 5;
      let ringR = (eyeRing.width()/2)-margin;
      let ringX = eyeRing.offset().left+margin;
      let ringY = eyeRing.offset().top+margin;

      if(this.isInside(pupilX,pupilY,pupilR,ringX,ringY,ringR) && this.active){
        let step = 1;
        let targetX = pupilX+pupilR; /*---- Pupil Focus Point ----*/
        let targetY = pupilY+pupilR; /*---------(center)----------*/
        let offsetY = this.state.pos.top+(mousePos.y>Math.floor(targetY)?step:(mousePos.y<Math.floor(targetY)?-step:0));
        let offsetX = this.state.pos.left+(mousePos.x>Math.floor(targetX)?step:(mousePos.x<Math.floor(targetX)?-step:0));
        let newX = (pupilX-eyePupil.position().left) + offsetX;
        let newY = (pupilY-eyePupil.position().top) + offsetY;

        if(this.isInside(newX,newY,pupilR,ringX,ringY,ringR)){
          this.setState({pos:{top:offsetY,left:offsetX}});
        }else if(this.isInside(newX,pupilY,pupilR,ringX,ringY,ringR)){
          this.setState({pos:{top:this.state.pos.top,left:offsetX}});
        }else if(this.isInside(pupilX,newY,pupilR,ringX,ringY,ringR)){
          this.setState({pos:{top:offsetY,left:this.state.pos.left}});
        }
      }else{
        //Center when out of bounds
        if(this.active) this.setState({pos:{top:Math.floor(this.props.size/4),left: Math.floor(this.props.size/2)}});
      }
    }catch (e){

    }
  }

  getEye(){
    return React.createElement('div',{className:'objectWrap',ref:'objectWrap'},React.createElement('object',{onLoad:this.svgLoad,type:"image/svg+xml",className:'eyePupil',data:'assets/images/db_home/eye_pupil_1.svg',id:this.props.uuid+'_eyePupil', style:{top:this.state.pos.top,left:this.state.pos.left}, ref:'eyePupil'}));
  }

  render(){
    return this.getEye();
  }
};

// Widget constraints
GoogleyEyes.config = {
  maxH: 8,
  maxW: 10,
  minH: 4,
  minW: 6,
  userSettings: {
    leftRingColour: {
      caption: 'Left Eye Colour',
      type: 'ColourPicker',
      value: '000000'
    },
    leftPupilColour: {
      caption: 'Left Pupil Colour',
      type: 'ColourPicker',
      value: '7d68fb'
    },
    rightRingColour: {
      caption: 'Right Eye Colour',
      type: 'ColourPicker',
      value: '000000'
    },
    rightPupilColour: {
      caption: 'Right Pupil Colour',
      type: 'ColourPicker',
      value: '7d68fb'
    },
    mouthColour: {
      caption: 'Mouth Colour',
      type: 'ColourPicker',
      value: '7d68fb'
    },
    randomColour: {
      caption: 'Random Colours',
      labelOn: 'On',
      labelOff: 'Off',
      type: 'Switch',
      value: false
    }
  }
};

//Widget default size
GoogleyEyes.defaults = {
  h: 4,
  w: 6
};
