var ToDo = class ToDo extends Widget {
  constructor(props){
    super(props);

    this.updateTitle = this.updateTitle.bind(this);
    this.updateList = this.updateList.bind(this);

    this.TIME_DELAY = 1000;
    this.saveLocal = this.saveLocal.bind(this);

// jshint ignore:start
    this.state = $.extend(true,{title: "To Do", toDoList: []},this.state)
// jshint ignore:end
  }

  widgetDidMount(){
  }

  widgetWillUnmount(){
  }

  saveLocal(){
    if(this.timer){
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(() => this.save(this.state), this.TIME_DELAY);
  }

  updateTitle(title){
    this.setState({title}, this.saveLocal);
  }

  updateList(toDoList){
    this.setState({toDoList}, this.saveLocal);
  }

  renderWidget(){
    let components = [],
      key = 0;

    components.push(React.createElement(ToDoTitle, {title: this.state.title, updateTitle: this.updateTitle, key: key++}));
    components.push(React.createElement(ToDoItemGroup, {toDoList: this.state.toDoList, updateList: this.updateList, key: key++}));

    return React.createElement('div', {className: 'widget widget-todo'}, React.createElement('div', {className: 'widget-todo-container'}, components));
  }
};

// Widget Settings
ToDo.config = {
  maxH: 30,
  maxW: 10,
  minH: 4,
  minW: 6
};

//Widget default values - these will override other settings
ToDo.defaults = {
  h: 10,
  w: 6
};


class ToDoTitle extends React.Component {
  constructor(props){
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.toggleEditMode = this.toggleEditMode.bind(this);
    this.onEnterPress = this.onEnterPress.bind(this);
    this.focusContainer = this.focusContainer.bind(this);
    this.defocus = this.defocus.bind(this);
    this.save = this.save.bind(this);

    this.state = {editMode: false, title: props.title, focused: false};
  }

  handleChange(e){
    this.setState({title: e.target.value}, this.save);
  }

  toggleEditMode(){
    this.setState(s => {return {editMode: !s.editMode};});
  }

  onEnterPress(e){
    if(e.keyCode == 13) {
      this.refs.input.blur();
      this.setState({focused: false});
    }
  }

  focusContainer(){
    this.setState({focused: true});
  }

  defocus(){
    this.setState({focused: false});
  }

  save(){
    this.props.updateTitle(this.state.title);
  }

  render(){
    return React.createElement('div', {className: 'widget-todo-title-container' + (this.state.focused ? ' widget-todo-title-container-focused' : '')},
      React.createElement('input', {className: 'widget-todo-title' + (this.state.focused ? ' widget-todo-title-focused' : ''), value: this.state.title, ref: 'input', onBlur: this.defocus, onChange: this.handleChange, onClick: this.focusContainer, onKeyDown: this.onEnterPress}));
  }
}

class ToDoItemGroup extends React.Component {
  constructor(props){
    super(props);

    this.getListClone = this.getListClone.bind(this);
    this.save = this.save.bind(this);
    this.focusChild = this.focusChild.bind(this);
    this.changeFocusChildSelection = this.changeFocusChildSelection.bind(this);
    this.clearChildFocus = this.clearChildFocus.bind(this);
    this.updateItemValue = this.updateItemValue.bind(this);
    this.updateItemText = this.updateItemText.bind(this);
    this.removeItem = this.removeItem.bind(this);
    this.addItem = this.addItem.bind(this);
    this.splitItem = this.splitItem.bind(this);
    this.merge = this.merge.bind(this);
    this.mergeAbove = this.mergeAbove.bind(this);
    this.mergeBelow = this.mergeBelow.bind(this);
    this.changeSelectedItem = this.changeSelectedItem.bind(this);
    this.selectionUp = this.selectionUp.bind(this);
    this.selectionDown = this.selectionDown.bind(this);
    this.applyChildSelection = this.applyChildSelection.bind(this);
    this.setChildRef = this.setChildRef.bind(this);

    this.selectPos = null;

    this.state = {toDoList: props.toDoList, selectPos:null};
  }

  setChildRef(ref){
    this.childToFocus = ref;
    if(ref!=null && ref.tagName === 'INPUT' && this.state.selectPos!=null){
      this.applyChildSelection();
    }
  }

  getListClone(){
    return JSON.parse(JSON.stringify(this.state.toDoList));
  }

  getRandomKey(){
    return `todoItem-${Math.random()}`;
  }

  save(){
    this.props.updateList(this.state.toDoList);
  }

  updateItemValue(id){
    let toDoList = this.getListClone();
    let item = toDoList.find(item => { return item.id === id; });

    item.done = !item.done;

    this.setState({toDoList}, this.save);
  }

  updateItemText(id, value){
    let toDoList = this.getListClone();
    let item = toDoList.find(item => { return item.id === id; });

    item.text = value;

    this.setState({toDoList}, this.save);
  }

  removeItem(id){
    let toDoList = this.getListClone();
    toDoList = toDoList.filter(item => { return item.id !== id; });
    // Balance ids
    toDoList.forEach((item, index) => {
      item.id = index;
    });
    this.setState({toDoList}, this.save);
  }

  focusChild(key){
    this.childToFocus.focus();
    this.childToFocus.click();
  }

  applyChildSelection(){
    if(this.childToFocus.setSelectionRange) {
      this.childToFocus.setSelectionRange(this.state.selectPos, this.state.selectPos);
      this.focusChild();

      let toDoList = this.getListClone();
      toDoList.forEach((e)=>{if(e.ref==this.childToFocus) e.ref = this.setChildRef;});

      this.setState({selectPos:null, toDoList:toDoList});
    }
  }

  changeFocusChildSelection(pos){
    this.setState({selectPos: pos+1});
  }

  clearChildFocus(id){
    let toDoList = this.getListClone();

    let child = toDoList[id];
    child.ref = undefined;

    this.setState({toDoList});
  }

  addItem(){
    let toDoList = this.getListClone();
    let newID = toDoList.length;
    toDoList.push({id: newID, text: '', done: false, key: this.getRandomKey(), ref: this.setChildRef});
    this.setState({toDoList}, () => {this.save(); this.focusChild(); this.clearChildFocus(newID);});
  }

  splitItem(id, start, end){
    let toDoList = this.getListClone();
    let key = this.getRandomKey();

    let itemToSplit = toDoList[id];
    let origText = itemToSplit.text;
    let oldText = origText.slice(0, start);
    let newText = origText.slice(end);

    itemToSplit.text = oldText;
    itemToSplit.key = this.getRandomKey();

    toDoList.splice(id + 1, 0, {id: id + 1, text: newText, done: false, key: this.getRandomKey(), ref: this.setChildRef});

    // Balance ids and refs
    toDoList.forEach((item, index) => {
      item.id = index;
    });

    this.setState({toDoList}, () => {this.save(); this.focusChild(); this.clearChildFocus(id + 1);});
  }

  merge(id1, id2){
      let toDoList = this.getListClone();

      let newSelection = toDoList[id1].text.length - 1;

      toDoList[id1].text = toDoList[id1].text + toDoList[id2].text;
      toDoList[id1].ref = this.setChildRef;
      toDoList[id1].key = this.getRandomKey();

      toDoList = toDoList.filter(item => { return item.id !== id2; });
      // Balance ids
      toDoList.forEach((item, index) => {
        item.id = index;
      });

      this.setState({toDoList},  () => {
        this.save();
        this.focusChild();
        this.changeFocusChildSelection(newSelection);
        //this.clearChildFocus(id1);
      });
  }

  mergeAbove(id){
    if(id > 0){
      this.merge(id - 1, id);
    }
  }

  mergeBelow(id){
    if(id < this.state.toDoList.length - 1){
      this.merge(id, id + 1);
    }
  }

  changeSelectedItem(id, endSelection = 0){
    let toDoList = this.getListClone();

    toDoList[id].ref = this.setChildRef;

    this.setState({toDoList}, () => {
      this.focusChild();
      this.changeFocusChildSelection(endSelection);
      this.clearChildFocus(id);
    });
  }

  selectionUp(id){
    if(id > 0){
      this.changeSelectedItem(id - 1);
    }
  }

  selectionDown(id){
    if(id < this.state.toDoList.length - 1){
      let endSelection = this.state.toDoList[id + 1].text.length - 1;
      this.changeSelectedItem(id + 1);
    }
  }

  render(){
    let components = [],
      key = 0;

    this.state.toDoList.forEach(toDoItem => {
      components.push(React.createElement(ToDoItem, {
        text: toDoItem.text,
        done: toDoItem.done,
        toggle: this.updateItemValue,
        updateText: this.updateItemText,
        removeItem: this.removeItem,
        id: toDoItem.id,
        focusRef: (toDoItem.ref ? toDoItem.ref : undefined),
        splitItem: this.splitItem,
        mergeAbove: this.mergeAbove,
        mergeBelow: this.mergeBelow,
        selectionUp: this.selectionUp,
        selectionDown: this.selectionDown,
        onEnterPress: this.onEnterPress,
        key: toDoItem.key
      }));
    });
    components.push(React.createElement(AddToDoItem, {addItem: this.addItem, key: key++}));

    return React.createElement('div', {className: 'widget-todo-item-container'}, components);
  }
}

class ToDoItem extends React.Component {
  constructor(props){
    super(props);

    this.onChange = this.onChange.bind(this);
    this.delete = this.delete.bind(this);
    this.onBackspace = this.onBackspace.bind(this);
    this.onEnter = this.onEnter.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onUpKey = this.onUpKey.bind(this);
    this.onDownKey = this.onDownKey.bind(this);

    this.state = {
      text: props.text
    };
  }

  onChange(){
    this.props.toggle(this.props.id);
  }

  delete(){
    this.props.removeItem(this.props.id);
  }

  onBackspace(textInput){
    if(textInput.selectionEnd === 0){
      textInput.blur();
      return {focused: false, callback: () => this.props.mergeAbove(this.props.id)};
    }
  }
  onEnter(textInput){
    let selectStart = textInput.selectionStart;
    let selectEnd = textInput.selectionEnd;
    textInput.blur();
    return {focused: false, callback: () => this.props.splitItem(this.props.id, selectStart, selectEnd)};
  }
  onDelete(textInput){
    let start = textInput.selectionStart;
    let end = textInput.selectionEnd;
    let text = textInput.value;

    // Confirm that the cursor is at the end of the item
    if(start === text.length && end === text.length){
      return {focused: true, callback: () => this.props.mergeBelow(this.props.id)};
    }
    return {focused: true, callback: () => {}};
  }
  onUpKey(textInput){
    if(textInput.selectionStart === 0 && textInput.selectionEnd === 0){
      return {focused: false, callback: () => this.props.selectionUp(this.props.id)};
    }
    return {focused: true, callback: () => {}};
  }
  onDownKey(textInput){
    let lastPos = textInput.value.length;
    if(textInput.selectionStart === lastPos && textInput.selectionEnd === lastPos){
      return {focused: false, callback: () => this.props.selectionDown(this.props.id)};
    }
    return {focused: true, callback: () => {}};
  }

  render(){
    let components = [],
      key = 0;

    components.push(React.createElement(ToDoTickBox, {className: 'widget-todo-item-checkbox multi-edit-tick-box', value: this.props.done, update: this.onChange, key: key++}));
    components.push(React.createElement(WidgetTextInput, {
      text: this.state.text,
      focusRef: this.props.focusRef,
      onUpKey: this.onUpKey,
      onDownKey: this.onDownKey,
      onEnter: this.onEnter,
      onBackspace: this.onBackspace,
      onDelete: this.onDelete,
      key: key++,
      updateText: text => this.props.updateText(this.props.id, text)
    }));
    components.push(React.createElement('div', {className: 'widget-todo-item-delete-container', onClick: this.delete, key: key++}, React.createElement('div', {className: 'widget-todo-item-delete', key: key++}, 'x')));

    return React.createElement('div', {className: 'widget-todo-item'}, components);
  }
}

class AddToDoItem extends React.Component {
  render(){
    return React.createElement('div', {className: 'widget-todo-add-item', onClick: this.props.addItem},
      React.createElement('div', {className: 'widget-todo-add-item-plus', key: '+'}, '+'),
      React.createElement('div', {className: 'widget-todo-add-item-text', key: 'text'}, 'Add item')
    );
  }
}

class ToDoTickBox extends React.Component {
  render(){
    var value = this.props.value;

    var button = React.createElement('div', {className: 'to-do-tick-option-button' + (value ? ' selected' : ''), onClick: this.props.update }, React.createElement('img', {src: 'assets/images/core/icon_tick.png'}));

    var selector =  React.createElement('div', { className: 'to-do-tick' }, button);

    return React.createElement('div', {className: 'to-do-tick-box'}, selector);
  }
}
