var WidgetError = class WidgetError extends React.Component {
  render(){
    let components = [],
      key = 0;

    components.push(React.createElement('img', {className: 'widget-error-icon', src: 'assets/images/core/icon_error.png', key: key++}));
    components.push(React.createElement('div', {className: 'widget-error-title', key: key++}, 'Error'));
    if(this.props.message) components.push(React.createElement('div', {className: 'widget-error-message', key: key++}, this.props.message));

    let closeIcon = React.createElement('img', {
      className: 'widget-icon widget-icon-close',
      onClick: () => this.props.close(this.props.id),
      src: 'assets/images/core/dashboard_icon_close.png',
      key: key++
    });

    return React.createElement('div', {className: 'widget-error-body-container'},
      React.createElement('div', {className: 'widget-error-body'}, components, closeIcon)
    );
  }
};
