/*
  For custom behaviour on key presses, give a prop for the key press.
  Currently supported: Enter, Backspace, Delete, Up, Down

  In the provided function, you control whether the input is focused on not at finish

  Function format:

  methodName(widgetTextInputRef){
    // Logic, can reference TextInput ref for actions like blur, or info like value

    // Must then return in given format
    return {focused: boolean [, callback: thingToDoAfterFocusChange]};
  }

  Here is a simple example from todo.es6:

  onBackspace(textInput){
    if(textInput.selectionEnd === 0){
      textInput.blur();
      return {focused: false, callback: () => this.props.mergeAbove(this.props.id)};
    }
  }

  Actions, and really all props, are completely optional. Without, you can simply use the class as a text
  box that will report on its value when it changes.
*/


var WidgetTextInput = class WidgetTextInput extends React.Component {
  constructor(props){
    super(props);

    this.getInputRef = this.getInputRef.bind(this);
    this.focus = this.focus.bind(this);
    this.defocus = this.defocus.bind(this);
    this.updateText = this.updateText.bind(this);
    this.onKeyUp = this.onKeyUp.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);

    this.state = {text: props.text};
  }

  getInputRef(){
    return this.props.focusRef || 'input';
  }

  focus(e){
    let divContent = window.getSelection();
    let text = (divContent.anchorNode!=null ? divContent.anchorNode.nodeValue : '');
    let offset = divContent.anchorOffset + this.state.text.indexOf(text);
    this.setState({focused: true},
      () => {
        let ref = this.props.focusRef || 'input';
        if(this.refs[ref]){
          this.refs[ref].focus();
          this.refs[ref].click();
          this.refs[ref].setSelectionRange(offset, offset);
        }
      }
    );
  }

  defocus(){
    this.setState({focused: false});
  }

  updateText(e){
    if(this.props.updateText){
      this.setState({text: e.target.value}, () => this.props.updateText(this.state.text));
    }
    else {
      console.error('Trying to update text, but no method provided. Check your declartion for WidgetTextInput.');
    }
  }

  keyAction(action){
    if(action){
      let ref = this.getInputRef();
      if(this.refs[ref]!=null){
        let {focused, callback} = action(this.refs[ref]);
        this.setState({focused}, callback);
      }
    }
  }

  onKeyUp(e){
    // Enter
    if(e.keyCode == 13) this.keyAction(this.props.onEnter);

    // Backspace
    if(e.keyCode == 8) this.keyAction(this.props.onBackspace);

    // Delete 46
    if(e.keyCode == 46) this.keyAction(this.props.onDelete);
  }

  onKeyDown(e){
    // Up 38
    if(e.keyCode == 38) this.keyAction(this.props.onUpKey);

    // Down 40
    if(e.keyCode == 40) this.keyAction(this.props.onDownKey);
  }

  render(){
    let content;
    if(this.state.focused){
      content = React.createElement('input', {
        className: 'widget-item-text',
        value: this.state.text,
        onChange: this.updateText,
        ref: this.getInputRef(),
        onBlur: this.defocus,
        onKeyUp: this.onKeyUp,
        onKeyDown: this.onKeyDown,
        key: 'input'
      });
    }
    else {
      content = React.createElement('div',
        {
          className: 'widget-item-text widget-item-linkify-container',
          onClick: this.focus,
          ref: this.getInputRef()
        },
        React.createElement(Linkify, {}, this.state.text)
      );
    }

    return React.createElement('div',
      {
        className: `widget-item-text-container ${this.state.focused ? ' widget-item-text-container-focused' : ''}`,
        key: 'container'
      },
      content
    );
  }
};
