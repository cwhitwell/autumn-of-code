var GraphCamp = class GraphCamp extends React.Component {
  constructor() {
        super();
        this.initializeChart = this.initializeChart.bind(this);

        this.state = {
            chartInstance:null
        };
    }

    componentDidUpdate() {
        if (!!this.props.updateCsvData && !!this.state.chartInstance){
            this.props.updateCsvData(this.state.chartInstance.getCSV());
        }
        if(this.state.chartInstance) this.state.chartInstance.reflow();
    }

    componentDidMount() {
        this.initializeChart(this.props);
    }

    componentWillReceiveProps(nextProps) {
        if(this.state.chartInstance) this.state.chartInstance.destroy();
        this.initializeChart(nextProps);
    }

    initializeChart(props) {

        function toFixed(num, fixed) {
            fixed = fixed || 0;
            fixed = Math.pow(10, fixed);
            return Math.floor(num * fixed) / fixed;
        }
        // try {
        // Build the series...
        var seriesItems = [];
        var max=0;
        var min=999999999999999;

        var transactionData = [];
        var issuanceData = [];
        var redeemedData = [];
        var penetrationData = [];

        let data = props.data.data;
        let format = '{value}';

        if (props.type === 'estate_wide') {
            for(var loop in data) {
                let date = data[loop].date * 1000;
                transactionData.push([date, data[loop].transactions]);
                issuanceData.push([date, data[loop].issuances]);
            }

            seriesItems = [
                {
                    name: 'Transactions',
                    data: transactionData,
                    animation: false,
                    lineWidth: 2,
                    color: props.colours[0],
                    fillColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, Highcharts.Color(props.colours[0]).setOpacity(0.1).get('rgba')],
                            [1, Highcharts.Color(props.colours[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    lineColor: Highcharts.Color(props.colours[0]).setOpacity(0.5).get('rgba')
                },
                {
                    name: 'Issuances',
                    data: issuanceData,
                    animation: false,
                    lineWidth: 2,
                    color: props.colours[1],
                    fillColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, Highcharts.Color(props.colours[1]).setOpacity(0.1).get('rgba')],
                            [1, Highcharts.Color(props.colours[1]).setOpacity(0).get('rgba')]
                        ]
                    },
                    lineColor: Highcharts.Color(props.colours[1]).setOpacity(0.5).get('rgba')
                }

            ];

            max = Math.max(Math.max.apply(Math, transactionData.map(function(x) { return x[1]; })),max);
            min = Math.min(Math.min.apply(Math, transactionData.map(function(x) { return x[1]; })),min);
        } else if (props.type === 'drPenetration') {
            format = format + '%';
            for(var loop in data) {
                let date = data[loop].date * 1000;

                penetrationData.push([date, (data[loop].issuance / (data[loop].transaction_day_total === 0 ? 1 : data[loop].transaction_day_total) * 100 )]);
            }

            seriesItems = [
                {
                    name: 'Penetration',
                    data: penetrationData,
                    animation: false,
                    lineWidth: 2,
                    color: props.colours[0],
                    fillColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, Highcharts.Color(props.colours[0]).setOpacity(0.1).get('rgba')],
                            [1, Highcharts.Color(props.colours[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    lineColor: Highcharts.Color(props.colours[0]).setOpacity(0.5).get('rgba')
                }
            ];

            max = Math.max(Math.max.apply(Math, penetrationData.map(function(x) { return x[1]; })),max);
            min = Math.min(Math.min.apply(Math, penetrationData.map(function(x) { return x[1]; })),min);
        } else {

            for(var loop in data) {

                var seriesData = [];
                var name = data[loop].item;

                for(var item in data[loop].day_data) {

                    var date = data[loop].day_data[item].date * 1000;

                    switch(props.type) {
                        case 'transactions':
                            seriesData.push([date, data[loop].day_data[item].transactions]);
                            break;
                        case 'issued':
                            seriesData.push([date, data[loop].day_data[item].issued]);
                            break;
                        case 'redeemed':
                            seriesData.push([date, data[loop].day_data[item].redeemed]);
                            break;
                    }

                }

                seriesItems.push({
                    name: name,
                    data: seriesData,
                    animation: false,
                    lineWidth: 2,
                    color: props.colours[loop],
                    fillColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, Highcharts.Color(props.colours[loop]).setOpacity(0.1).get('rgba')],
                            [1, Highcharts.Color(props.colours[loop]).setOpacity(0).get('rgba')]
                        ]
                    },
                    lineColor: Highcharts.Color(props.colours[loop]).setOpacity(0.5).get('rgba')
                });

                max = Math.max(Math.max.apply(Math, seriesData.map(function(x) { return x[1]; })),max);
                min = Math.min(Math.min.apply(Math, seriesData.map(function(x) { return x[1]; })),min);

            }

        }

        if(max == 0) max = 100;
        if(min > max*0.50) min=Math.floor(max*0.5);
        if(min<0) min=0;

        var selector = ReactDOM.findDOMNode(this.refs.cpChart);

        var legend = {
            enabled: false
        };
        let margin = [10, 10, 30, 60];

        if (this.props.orientation === 'wide') {
            legend = {
                enabled: true,
                align: 'right',
                verticalAlign: 'top',
                layout: 'vertical',
                itemWidth: 150,
                itemStyle: {
                    width: 150
                },
                padding: 0,
                margin: 0
            };
            margin = [10, 180, 30, 60];
        } else if (this.props.orientation === 'tall') {
            legend = {
                enabled: true,
                //align: 'center',
                verticalAlign: 'bottom',
                layout: 'horizontal',
                alignColumns: false,
                itemWidth: 150
            };

            let dataArray = 1;
            if (props.data.data && props.data.data.length > 0) {
                dataArray = props.data.data.length;
            }
            let numCampaigns = dataArray;

            let rows = Math.ceil(numCampaigns / this.props.columns);

            let offset = (rows * 15) + 42;

            margin = [10, 10, offset, 60];
        }

        if (props.type == 'estate_wide') {
            legend.enabled = false;
        }

        var chartModel = {
            title: { text: null },
            legend,
            chart: {
                type: 'area',
                margin: margin,
                renderTo: selector
            },
            series: seriesItems,
            credits: { enabled: false },
            xAxis: {
                gridLineColor: '#F1F1F1',
                allowDecimals: false,
                type: 'datetime',
                labels: {
                    enabled: true,
                    style: { color: '#b1c1ca', fontSize: '10px' }
                },
                dateTimeLabelFormats: {
                    day: '%d/%m'
                }
            },
            yAxis: {
                min: min,
                max: max,
                title: { text: null },
                gridLineColor: '#F1F1F1',
                labels: {
                    enabled: true,
                    style: { color: '#b1c1ca', fontSize: '10px' },
                    format: format
                }
            },
            tooltip: {
                headerFormat: "",
                pointFormat: "{point.x:" + '%d/%m' + "/%Y" + " - %H:%M} <br /><b>{series.name}</b>: {point.y}",
                useHTML: true
            },
            plotOptions: {
                series: {
                    lineWidth: 2,
                    marker: {
                        symbol: 'circle',
                        radius: 3,
                        states: {
                            hover: {
                                radius: 3
                            }
                        }
                    },
                    states: {
                        hover: {
                            lineWidthPlus: 0
                        }
                    }
                }
            },
            exporting: {
                chartOptions: { // specific options for the exported image
                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    }
                },
                scale: 3,
                fallbackToExportServer: false
            }
        };

        var chartInstance = new Highcharts.Chart(chartModel);
        this.setState({
          chartInstance: chartInstance
        });
    }

    render() {

        //var error = React.createElement('div', {className:'hide no_analytics', ref: 'holder'},'No'_analytics_data);
        var chart = React.createElement('div', {className: '', ref: 'cpChart'});

        return React.createElement('div',{ className: 'graphBody' + ((this.props.orientation === 'tall') ? ' tall': ' notwide')}, chart);

    }
};
