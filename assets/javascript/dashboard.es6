var Dashboard = class Dashboard extends React.Component {

  constructor(props){
    super(props);

    this.updateLayout = this.updateLayout.bind(this);
    this.updateSettings = this.updateSettings.bind(this);
    this.findItemInLayout = this.findItemInLayout.bind(this);
    this.generatePointLayout = this.generatePointLayout.bind(this);
    this.findSpace = this.findSpace.bind(this);
    this.addWidget = this.addWidget.bind(this);
    this.addBlock = this.addBlock.bind(this);
    this.removeBlock = this.removeBlock.bind(this);
    this.clearLayout = this.clearLayout.bind(this);
    this.deleteLayout = this.deleteLayout.bind(this);
    this.resetToDefaultLayout = this.resetToDefaultLayout.bind(this);
    this.toggleSettings = this.toggleSettings.bind(this);
    this.togglePopout = this.togglePopout.bind(this);
    this.saveWidget = this.saveWidget.bind(this);
    this.toggleEditMode = this.toggleEditMode.bind(this);
    this.toggleAddMode = this.toggleAddMode.bind(this);
    this.toggleDeleteMode = this.toggleDeleteMode.bind(this);
    this.updateWidth = this.updateWidth.bind(this);
    this.serialiseLayout = this.serialiseLayout.bind(this);
    this.deserialiseLayout = this.deserialiseLayout.bind(this);
    this.buildLayoutFromDefaultConfig = this.buildLayoutFromDefaultConfig.bind(this);
    this.setRef = this.setRef.bind(this);
    this.ageWidget = this.ageWidget.bind(this);
    this.disableAddScreenTransparency = this.disableAddScreenTransparency.bind(this);
    this.checkSettingsType = this.checkSettingsType.bind(this);
    this.getNumberOfColumns = this.getNumberOfColumns.bind(this);
    this.widgetIsAuthorised = this.widgetIsAuthorised.bind(this);

    let url = window.location.href.split(`/`);
    this.boardName = 'main';
    if(url[url.length - 2] == 'b') this.boardName = url[url.length - 1];

    this.saveLayout = this.saveLayout.bind(this);
    this._saveLayout = this._saveLayout.bind(this);
    this.SAVE_DELAY = 1000;

    // In edit mode, Any CSS classes listed here will not be draggable, and inputs will work
    this.nonDraggableClasses = '.widget-item-text, .widget-item-textarea';

    this.sidebarWidth = 270; // This needs to update if the sidebar changes
    this.dashboardMargin = 32;
    window.onresize = this.updateWidth;
    this.columnWidth = 50;
    this.rowHeight = 40;
    this.defaultColNumber = 19;
    this.colNumber = 19;
    this.preadjColNumber = 19;
    this.highlightDelay = 1050; // ms. How long a widget will be highlighted for when new

    // Check server message
    if(props.code === 500) throw new Error(`The server returned code 500 - ${props.message}`);
    if(props.layout === undefined) throw new Error(`Dashboard Error - No layout was provided\n${props.message}`);

    let width = this.calcWidth();
    this.getNumberOfColumns(width);

    let layout;
    let newLayout = JSON.parse(props.layout);
    if(props.default) layout = this.buildLayoutFromDefaultConfig(newLayout);
    else layout = this.deserialiseLayout(newLayout);

    this.state = {
      layout,
      gridLayout: layout,
      editMode: false,
      addMode: false,
      changed: false,
      saving: false,
      widgetSettings: null,
      widgetPopoutChildren: null,
      widgetPopoutId: null,
      width,
      widgetRefs: {}
    };
  }

  setRef(ref){
    if(ref!=null){
      this.setState(s => {
        let clone = $.extend(true,{},s.widgetRefs);
        clone[ref.props.id] = ref;
        return {widgetRefs: clone};
      });
    }
  }

  componentDidMount(){
    this.updateWidth(); // Register current width
    setTimeout(() => {this.shouldAnimate = true;}, 100); // Bit hacky, but makes it possible to stop dashboard loading transitions
  }

  componentWillUnmount(){
    clearTimeout(this.saveTimer);
  }

  // Updates the width that the dashboard should fill (assigned to onresize)
  updateWidth(){
    this.setState(s => { return {width: this.calcWidth()}; }, this.getNumberOfColumns);
  }
  calcWidth(){
    return window.innerWidth - this.sidebarWidth - this.dashboardMargin;
  }

  serialiseLayout(){
    let layout = this.state.layout.slice();
    return JSON.stringify(layout.map(widget => {
      let name = widget.name;
      let obj = JSON.parse(JSON.stringify(widget));
      obj.name = name;
      return obj;
    }));
  }
  deserialiseLayout(layout){
    try {
      return layout.reduce((newLayout, widget) => {
        widget.widget = window[widget.name];
        let inList = this.props.widgetList.some(allowedWidget => {return allowedWidget.name === widget.name;});
        if(widget.widget && inList){
          newLayout.push(widget);
        }
        else{
          console.error(`Not loading widget ${widget.name} as the class could not be found. It will be removed from the layout.`);
        }
        return newLayout;
      }, []);
    }
    catch(e) {
      swal('Error', 'Fatal error occurred', "error");
      console.error(`Couldn't load layout: ${e.message}`);

      this.resetToDefaultLayout(false);
    }
  }

  buildLayoutFromDefaultConfig(defaultLayout){
    let layout = [];

    defaultLayout.forEach(widget => {
      let height = widget.size.height;
      let width = widget.size.width;
      let widgetClass = window[widget.name];

      // Checks
      if(widgetClass == undefined) throw new Error(`Dashboard Error - Could not find a widget named ${widget.name}. Check your configuration`);
      if(height > window[widget.name].config.maxH){
        console.error(`Dashboard Error - ${widget.size.height} is larger that the max height for ${widget.name}. Using max value instead: ${widgetClass.config.maxH}`);
        height = window[widget.name].config.maxH;
      }
      if(height < window[widget.name].config.minH){
        console.error(`Dashboard Error - ${widget.size.height} is smaller that the min height for ${widget.name}. Using min value instead: ${widgetClass.config.minH}`);
        height = window[widget.name].config.minH;
      }
      if(width > window[widget.name].config.maxW){
        console.error(`Dashboard Error - ${widget.size.width} is larger that the max width for ${widget.name}. Using max value instead: ${widgetClass.config.maxW}`);
        width = window[widget.name].config.maxW;
      }
      if(width < window[widget.name].config.minW){
        console.error(`Dashboard Error - ${widget.size.width} is smaller that the min width for ${widget.name}. Using min value instead: ${widgetClass.config.minW}`);
        width = window[widget.name].config.minW;
      }

      let pointLayout = this.generatePointLayout(layout);
      let {x, y} = this.findSpace(pointLayout, {h: height, w: width});

      let configProps;
      if(widget.customName){
        let widgetInstance = this.props.widgetList.find(x => x.customName === widget.customName);
        if(widgetInstance){
          configProps = widgetInstance.props;
        }
      }

    layout.push(Object.assign({}, {widget: widgetClass, state:widget.state, name: widget.name, configProps, i: this.guid(), x, y, h: height, w: width}, widgetClass.config, (widget.userSettings!=null ? {userSettings:widget.userSettings} : {}) ));

    });

    return layout;
  }

  guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return `${s4()}${s4()}-${s4()}-${s4()}-${s4()}-${s4()}${s4()}${s4()}`;
  }

  generatePointLayout(layout = this.state.layout){
    let arr = [];
    layout.forEach(block => {
      for(let i = 0; i < block.h; i++){
        if(arr[block.y + i] == undefined) arr[block.y + i] = new Array(this.gridWidth);
        for(let j = 0; j < block.w; j++){
          arr[block.y + i][block.x + j] = 1;
        }
      }
    });
    return arr;
  }

  findSpace(pointLayout, dimensions){
    let placeFound = false;
    let pos = {x:0, y:0};

    let checkY = pos => {if(pointLayout[pos] == undefined) pointLayout[pos] = new Array(this.preadjColNumber);};
    let checkBlockUndefined = (posX, posY, posX2, posY2) => {
      let undefinedBlock = true;
      for(let y = posY; posY <= posY2; posY++){
        for(let x = posX; posX <= posX2; posX++){
          checkY(y);
          if(pointLayout[y][x] != undefined) undefinedBlock = false; break;
        }
      }
      return undefinedBlock;
    };
    dimensions = {w: dimensions.w > 1 ? dimensions.w - 1 : 0, h: dimensions.h > 1 ? dimensions.h - 1: 0};
    while(!placeFound){
      if(pos.x == 0) checkY(pos.y);
      if(pointLayout[pos.y][pos.x] == undefined){
        // Check each corner of a potential space, for speed
        checkY(pos.y+dimensions.h);
        if(pointLayout[pos.y][pos.x+dimensions.w] == undefined &&
          pointLayout[pos.y+dimensions.h][pos.x] == undefined &&
          pointLayout[pos.y+dimensions.h][pos.x+dimensions.w] == undefined){

          if(checkBlockUndefined(pos.x, pos.y, pos.x + dimensions.w, pos.y + dimensions.y)) return Object.assign({}, pos);
        }
      }

      // Moving left to right, top to bottom
      pos.x++;
      if(pos.x + dimensions.w > this.preadjColNumber){ pos.x=0; pos.y++; }
    }

  }

  isEmpty(obj){
    if (obj === null ||
        obj === undefined ||
        Array.isArray(obj) ||
        typeof obj !== 'object'
    ) {
        return true;
    }
    return Object.getOwnPropertyNames(obj).length === 0 ? true : false;
  }

  addBlock(widgetName, props = undefined){
    let widgetClass = window[widgetName];
    let pointLayout = this.generatePointLayout();
    let dimensions = Object.assign({}, widgetClass.config, widgetClass.defaults);
    let h = dimensions.h || 1,
      w = dimensions.w || 1;

    if(h == undefined){
      if(dimensions.maxH) h = dimensions.maxH;
      if(dimensions.minH) h = dimensions.minH;
    }
    if(w == undefined){
      if(dimensions.maxH) w = dimensions.maxH;
      if(dimensions.minH) w = dimensions.minH;
    }
    let {x, y} = this.findSpace(pointLayout, {h, w});
    let layout = this.state.layout.slice();
    let config = Object.assign({}, widgetClass.config);

    if(config.userSettings && !this.isEmpty(config.userSettings)){
      let userConfig = config.userSettings;
      let userSettings = Object.entries(userConfig).reduce((filteredConfig, [settingName, setting]) => {
        if(this.checkSettingsType(setting.type)) filteredConfig[settingName] = setting;
        return filteredConfig;
      }, {});
      config.userSettings = userSettings;
    }

    let guid = this.guid();

    layout.push(Object.assign(
      {widget: widgetClass, name: widgetName},
      config,
      {i: guid, x, y, h, w},
      widgetClass.defaults,
      {configProps: props, new: true}
    ));

    this.setState({layout, gridLayout: layout, addScreenTransparent: true}, () => {setTimeout(() => this.ageWidget(guid), this.highlightDelay);});
  }

  removeBlock(widgetKey){
    let layout = this.state.layout.slice().filter(widget => widget.i !== widgetKey);
    this.setState({layout});
  }

  ageWidget(guid){
    let layout = this.state.layout.slice();
    let widget = layout.find(widget => widget.i === guid);
    if(widget) widget.new = false;
    this.setState({layout});
  }

  disableAddScreenTransparency(){
    this.setState({addScreenTransparent: false});
  }

  clearLayout(){
    this.setState({layout: [], gridLayout: []});
  }

  deleteLayout(){
    swal({
        title: 'Are you sure?',
        text: 'This will erase all active widgets',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes',
        cancelButtonText: 'Cancel',
    }).then(isConfirm => {
        if (isConfirm) {
          this.saveLocalStorage({
            layout: '',
            board_name: this.getBoardName()
          });
          this.setState({layout:[],deleteMode:false});
        }
    });
  }

  /*
    React Grid Layout seems to have some issues with large changes, so for simplicity's sake
    we update and restart.

    If you're wondering why Ganymede#reload has to be called inside its own fat arrow method, it's
    because this ensures that the method will be run within Ganymede's own context, instead of
    Dashboard. Bit hacky, but better than changing application.js for now.
  */
  resetToDefaultLayout(confirm = true){
    /*DISABLED
    let go = () => $.ajax({
      method: "GET",
      url: `db_home/api/default/${this.getBoardName()}`,
      contentType: "application/json",
      dataType: 'json',
      success: data => {
        if(data.layout){
          let layout = JSON.parse(data.layout);
          this.setState({layout: this.buildLayoutFromDefaultConfig(layout)},
            () => this._saveLayout(
              () => window.location.reload()
            )
          );
        }
      },
      error: data => {
        console.error(data);
        swal('Error', 'Something went wrong!', "error");
      }
    });*/

    if(confirm){
      swal({
          title: 'Are you sure?',
          text: 'This is reset the dashboard to the original default layout.',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
          cancelButtonText: 'Cancel',
      }).then(isConfirm => {
        if (isConfirm) {
          this.saveLocalStorage({});
          window.location.reload();
        }
      });
    }
    else window.location.reload();
  }

  toggleSettings(widgetKey){
    if(this.state.widgetSettings === widgetKey){
      this.setState({widgetSettings: null});
    }
    else {
      this.setState({widgetSettings: widgetKey});
    }
  }

  togglePopout(widgetKey, children) {
    if(this.state.widgetPopoutId === widgetKey){
      this.setState({widgetPopoutId: null, widgetPopoutChildren: null});
    }
    else {
      this.setState({widgetPopoutId: widgetKey, widgetPopoutChildren: children});
    }
  }

  updateLayout(nextLayout){
    // merging objects ensures that we keep our custom class definitions in the layout
    let newLayout = [];
    // loop through next layout (this one should be authority on conflicts)
    // Check keys to establish the records are the same
    // merge records and add to newLayout if they are
    nextLayout.forEach(item => {
      let oldItem = this.findItemInLayout(item.i);
      if(oldItem) newLayout.push(Object.assign({}, oldItem, item));
      else newLayout.push(oldItem);
    });
    this.setState({
      layout: newLayout,
      gridLayout: nextLayout
    }, this.saveLayout);
  }

  updateSettings(id, settings){
    let layout = this.state.layout.slice();

    layout.forEach(item => {
      if(item.i === id){
        if(item.state == undefined) item.state = {};
        item.userSettings = settings;
      }
    });

    this.setState({layout}, this.saveLayout);
  }

  saveWidget(widgetData){
    let layout = this.state.layout.slice();
    let layoutWidget = layout.find(widget => widget.i === widgetData.id);

    if(layoutWidget){
      layoutWidget.state = widgetData.state;
      this.setState({layout}, this.saveLayout);
    }
    else {
      throw new Error(`Tried to save a widget that doesn't exist. Check that widget with supposed id '${widgetData.id}' is correctly passing its information to the main dashboard class.`)
    }
  }

  // Only saveLayout() should ever be called by any other method, letting it manage when to really save
  saveLayout(){
    this.setState({changed: true});
    if(this.saveTimer){
      clearTimeout(this.saveTimer);
    }
    this.saveTimer = setTimeout(this._saveLayout, this.SAVE_DELAY);
  }
  _saveLayout(callback){
    /* DISABLED - c.w
    this.setState({saving: true});
    $.ajax({
        type: "POST",
        url: `/db_home/api/save`,
        data: {
          layout: this.serialiseLayout(),
          board_name: this.getBoardName()
        },
        cache: false,
        success: data => {
            this.setState({saving: false, changed: false}, () => {if(callback) callback();});
        },
        error: data => {
            this.setState({saving: false, changed: false});
            let error = data;
            if(data.responseJSON) error = data.responseJSON;
            else if(data.responseText) error = data.responseText;
            console.error(`Error: ${error}`);
            swal('Error', 'Error', 'error');
        }
    });
    */

    //LOCAL STORAGE
    this.saveLocalStorage({
      layout: this.serialiseLayout(),
      board_name: this.getBoardName()
    });
    this.setState({saving: false, changed: false}, () => {if(callback) callback();});

  }

  saveLocalStorage(object){
    if(LocalStorageObj().canUse){
      LocalStorageObj().setItem('dashboard_data',JSON.stringify(object));
      console.log('saved data to localStorage.');
    }else{
      console.warn("LOCAL STORAGE UNAVAILABLE.");
    }
  }

  getBoardName(){
    let name = 'main';

    let url = window.location.href.split(`/`);
    if(url[url.length - 2] == 'b') name = url[url.length - 1];

    return name;
  }

  findItemInLayout(key){
    let returnItem;
    this.state.layout.forEach(item => {
      if(item.i === key) returnItem = Object.assign({}, item);
    });
    return returnItem;
  }

  toggleEditMode(){
    this.shouldAnimate = false;
    this.setState(s => {
      return {editMode: !s.editMode};
    }, () => {setTimeout(() => {this.shouldAnimate = true;}, 200);});
  }

  toggleAddMode(){
    this.shouldAnimate = false;
    this.setState(s => {
      return {addMode: !s.addMode};
    }, () => {setTimeout(() => {this.shouldAnimate = true;}, 200);});
  }

  toggleDeleteMode(){
    this.setState(s => {return {deleteMode: !s.deleteMode};});
  }

  findSettingsConfigFromWidgetID(id){
    let widget = this.findItemInLayout(id);

    if(widget == undefined) return false;
    return widget.userSettings;
  }

  checkSettingsType(type){
    let className = `WidgetSettings${type}`;
    if(window[className]) return true;
  }

  getNumberOfColumns(width = this.state.width){
    // Get ideal number of columns
    this.colNumber = Math.floor(width / this.columnWidth) || this.defaultColNumber;
    this.preadjColNumber = this.colNumber;
    // Adjust if necessary
    if(this.state && this.state.layout){
      this.state.layout.forEach(widget => {
        if(widget.x + widget.w > this.colNumber) this.colNumber = widget.x + widget.w;
      });
    }

    return this.colNumber;
  }

  addWidget(name, props){
    this.addBlock(name, props);
  }

  widgetIsAuthorised(widget){
    return true;
  }

  render(){
    let reactComponents = [];

    let topBar = React.createElement(DashboardTopBar, {
      toggleEditMode: this.toggleEditMode,
      toggleAddMode: this.toggleAddMode,
      toggleDelete: this.toggleDeleteMode,
      changeTracker: {
        changed: this.state.changed,
        saving: this.state.saving
      },
      editMode: this.state.editMode,
      key: 'dashoard-top-bar'
    });

    if(this.state.addMode) {
      let widgetList = [];
      this.props.widgetList.forEach(widget => {
        // Check if at least one permission is met
        if(this.widgetIsAuthorised(widget)){
          if(window[widget.name] == undefined) console.error((`Dashboard Error - Could not find widget named ${widget.name}`));
          else widgetList.push(widget);
        }
      });
      reactComponents.push(React.createElement(DashboardAddScreen, {widgetList, addWidget: this.addWidget, toggleAddMode: this.toggleAddMode, transparent: this.state.addScreenTransparent, key: 'dashboard-add-screen'}));
    }

    if(this.state.deleteMode) {
      let deleteButton = React.createElement('div', {className: 'dashboard-delete-screen-button button button-warning', onClick: this.deleteLayout}, 'Delete All Widgets');
      let resetButton = React.createElement('div', {className: 'dashboard-delete-screen-button button', onClick: this.resetToDefaultLayout}, 'Reset Dashboard to Default');
      let deleteScreen = React.createElement('div', {className: 'dashboard-delete-screen'}, resetButton, deleteButton);
      reactComponents.push(React.createElement(PopOutContainer, {content: deleteScreen, dismissable: true, additionalClassName: 'dashboard-delete-screen-popout', closePopup: this.toggleDeleteMode, key: 'dashboard-delete-mode'}));
    }

    // Main grid
    let widgets = [];
    if(this.state.layout) this.state.layout.forEach(widget => {
      if(widget.userSettings == undefined){
        if(widget.widget.userSettings){
          let settings = Object.entries(widget.widget.userSettings).reduce((settingsList, [settingName, setting]) => {
            if(this.checkSettingsType(setting.type)) settingsList[settingName] = setting;
            return settingsList;
          }, {});
          widget.userSettings = settings;
        }
      }

      let widgetProps = {
        id: widget.i,
        _close: this.removeBlock,
        savedState: widget.state,
        userSettings: widget.userSettings,
        editMode: this.state.editMode,
        saveWidget: this.saveWidget,
        togglePopout: this.togglePopout,
        ref:this.setRef
      };
      if(widget.configProps) widgetProps = Object.assign(widgetProps, widget.configProps);

      widgets.push(
        React.createElement(WidgetContainer, {
          key: widget.i,
          id: widget.i,
          widgetRef: this.state.widgetRefs[widget.i],
          editMode: this.state.editMode,
          removeBlock: this.removeBlock,
          toggleSettings: ((widget.userSettings && !this.isEmpty(widget.userSettings)) ? this.toggleSettings : undefined),
          new: widget.new,
          addScreenOpen: this.state.addMode,
          disableAddScreenTransparency: this.disableAddScreenTransparency,
        },
        React.createElement(widget.widget, widgetProps))
      );
    });

    let cols = this.getNumberOfColumns();
    let width = this.columnWidth * cols;
    let grid = React.createElement(ReactGridLayout, {
      className: 'dashboard-grid' + (this.shouldAnimate ? ' animated' : ''),
      key: 'dashboard-grid',
      layout: this.state.gridLayout,
      onLayoutChange: this.updateLayout,
      draggableCancel: this.nonDraggableClasses,
      isDraggable: this.state.editMode,
      isResizable: this.state.editMode,
      cols,
      rowHeight: this.rowHeight,
      width
    }, widgets);
    reactComponents.push(React.createElement('div', {className: 'dashboard-container', key: 'dashboard-container'}, grid));

    // Settings Panel
    if(this.state.widgetSettings){
      let widgetSettings = this.findSettingsConfigFromWidgetID(this.state.widgetSettings);
      if(widgetSettings){
        reactComponents.push(React.createElement(WidgetSettingsPanel, {settings: widgetSettings, updateSettings: settings => {this.updateSettings(this.state.widgetSettings, settings);}, closeSettings: () => {this.toggleSettings(this.state.widgetSettings);}, key: 'widget-settings'}));
      }
      else{
        throw new Error('Settings panel requested but no settings config could be found. Does this widget have a correctly formatted settings config in Widget.config?')
      }
    }

    //Popout Panel
    if(this.state.widgetPopoutId) {
      let popoutContent = React.createElement('div', {className: 'popout'}, this.state.widgetPopoutChildren);
      let popOutContainer = React.createElement(PopOutContainer, {content: popoutContent, additionalClassName: 'widget-popout-container', dismissable: true, closePopup: this.togglePopout, key: "uniqueLoading"});
      reactComponents.push(popOutContainer);
    }

    return React.createElement('div', {className: 'dashboard'}, topBar, reactComponents);
  }
};

var LocalStorageObj = () => {
  var result = false;
  var targetKey = null;
  if(typeof Storage !== 'undefined'){
    try {
      var storage = window.localStorage,
      x = '__storage_test__';
      storage.setItem(x, x);
      storage.removeItem(x);
      targetKey = 'localstorage';
      result = true;
    }
    catch(e) {
      result = e instanceof DOMException && (
        // everything except Firefox
        e.code === 22 ||
        // Firefox
        e.code === 1014 ||
        // test name field too, because code might not be present
        // everything except Firefox
        e.name === 'QuotaExceededError' ||
        // Firefox
        e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
        // acknowledge QuotaExceededError only if there's something already stored
        storage.length !== 0;
    }
  }else{
    result = false;
  }

  //Window fallback option
  if(targetKey==null && window.hasOwnProperty('name')){
    result = true;
    targetKey = 'window';
  }

  let targets = {
    localstorage: (targetKey === 'localstorage' ? window.localStorage : {}),
    window: {
      getItem: (key) => {
        try{
          let json = JSON.parse(window.name);
          return json[key];
        }catch(e){
          return null;
        }
      },
      setItem: (key,value) => {
        let json;
        try{
          if(window.name == null){
            window.name = '{}';
          }

          json = JSON.parse(window.name);
          json[key] = value;

          window.name = JSON.stringify(json);
        }catch(e){
          window.name = '{}';
        }

        json = JSON.parse(window.name);
        json[key] = value;

        window.name = JSON.stringify(json);
      }
    }
  };

  let target = (targetKey !== null ? targets[targetKey] : {});
  return $.extend(target,{canUse:result});
};



var props = {code:500,message:'no config found.'};
if(dashboardConfig!=null){
  let layout = JSON.stringify(dashboardConfig.default);
  let def = true;
  let localData = (LocalStorageObj().canUse && LocalStorageObj().getItem('dashboard_data')!=null ? JSON.parse(LocalStorageObj().getItem('dashboard_data')) : null);
  if(LocalStorageObj().canUse && localData!= null && localData.layout != null){
    layout = localData.layout;
    def = false;
  }

  props = {
    default: def,
    layout: layout,
    editable: dashboardConfig.editable,
    widgetList: dashboardConfig.widgets,
    code: 200
  };
}

//LOAD DASHBOARD
ReactDOM.render( React.createElement(Dashboard, props), document.getElementById('react-root') );
