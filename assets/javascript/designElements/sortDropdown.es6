class SortDropdown extends React.Component {
    render() {
        var reactComponents = [];
        var key=0;

        function makeAction(callback,sort) {
            return function(){
                if(callback) callback(sort);
            }.bind(this);
        }

        var options = [];
        for(key in this.props.sortFunctions) {
            var caption = this.props.sortFunctions[key].caption;
            if (typeof caption === 'function') caption = caption();

            options.push({
                title: caption,
                renderTemplate: 'simple',
                renderData: {
                    title: caption
                },
                action: makeAction(this.props.callback,this.props.sortFunctions[key])
            })
        }

        var dropDownProperties = {
            title: '',
            preSelectedOptionIndex: this.props.preSelectedOptionIndex,
            size: this.props.size || 'medium',
            options: options
        };

        return React.createElement('div',{style: {float:'left', display:'inline-block'}},React.createElement(DropDown, dropDownProperties));
    }
}
