(function(){
    try {
        window.StringMulti = class StringMulti extends GanymedeComponent {
            constructor(props) {
                super(props);
                this.onBlur = this.onBlur.bind(this);
                this.onChange = this.onChange.bind(this);
                this.validationResult = this.validationResult.bind(this);
                this.isValid = this.isValid.bind(this);

                // Update/add our state vars
                this.name = "StringMulti";
                this.state.value = (this.props.default || '');
            }

            // onBlur() {
            //     this.props.updateStatus(this.props.id, this.state.value, this.isValid(this.state.value), this.state.enabled || this.props.mandatory);
            // }

            // Currently this allows the instant updating of all values but does repeated calls
            // back to the parent every time things changed, in the old Raju based patterns this
            // could cause update storm issues but hopefully now it won't as we're more controlled, however
            // if still storms then onBlur must be put back and only then is the value updated and
            // the callback triggered.

            onChange() {
                let inputValue = this.refs.inputField.value;
                this.setState({touched:true, value: inputValue});
            }

            onBlur() {
                if(!this.state.touched) this.setState({touched: true});
            }

            validationResult(data) {
                return Ganymede.formValidator(data, this.state.newConstraints);
            }

            isValid(data) {
                return !this.validationResult(data);
            }

            render() {
                if(Ganymede.pageData.debug) console.log("StringMulti Render("+this.props.id+") - ",this.state.value);

                let mandatory = !!this.props.mandatory;
                let presentation = this.props.presentation || {};
                let id = 'stringSingle'+this.props.id;
                let displayClass = (!!presentation.hidden)?' hide':'';
                let name = this.props.name;
                let defaultValue = this.props.default || '';
                let validation = this.validationResult(this.state.value);

                let content = [];

                content.push(React.createElement('textarea', {
                    key:'component',
                    tabIndex: this.props.enabled?1:-1,
                    className: id,
                    readOnly:this.props.readOnly,
                    placeholder: presentation.placeholder,
                    defaultValue: defaultValue,
                    ref:'inputField',
                    onBlur: this.onBlur,
                    onChange: this.onChange
                }));

                if (this.state.touched && !!validation){
                    content.push(React.createElement('div', {key:'error', className: 'error hasError '+id, ref:'errorField'},validation));
                }

                let container;
                let metadataMessage = '';
                if(this.state.newConstraints.maxLength!==Ganymede.formConstraints.maxLength && this.state.touched) metadataMessage = this.state.value.length + ' out of ' + this.state.newConstraints.maxLength;

                if (!!presentation.caption && presentation.caption.trim() === ''){
                    container = React.createElement(SimpleContainer, {readOnly: this.props.readOnly, content: content, elementId: id, metadataMessage: metadataMessage});
                }
                else {
                    if (mandatory){
                        container = React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, metadataMessage: metadataMessage});
                    }
                    else {
                        container = React.createElement(OptionalContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, onToggleEnable: this.onToggleEnable, metadataMessage: metadataMessage, enabled: this.state.enabled});
                    }
                }


                if (this.props.simplified) {
                    let container = React.createElement(StringMultiSimplified, {readOnly: this.props.readOnly, value: defaultValue, caption: presentation.caption});
                    return React.createElement('div',{className: 'textBox stringMulti '+id+displayClass+' '+name + ' simplified'}, container);
                } else {
                    return React.createElement('div', {className: 'textBox stringMulti '+id+displayClass+' '+name, onClick: this.onTouch }, container);
                }

            }
        };

        class StringMultiSimplified extends React.Component {

            render() {
                let content = React.createElement('div', {}, this.props.value);
                return React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: this.props.caption, content: content, simplified: true});
            }
        }


    } catch(e){
        console.error('an error was found:',e);
    }

}());
