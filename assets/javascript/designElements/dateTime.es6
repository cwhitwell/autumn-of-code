(function(){
try {
    window.DateTime = class DateTime extends GanymedeComponent {
        constructor(props) {
            super(props);
            this.isValid = this.isValid.bind(this);

            this.state.value = (this.props.default || (new Date()).toISOString());
        }


        componentWillReceiveProps(nextProps) {
            GanymedeComponent.prototype.componentWillReceiveProps.call(this,nextProps);

            if (!!nextProps.shouldUpdateValueOnNewDefaultValue && !!nextProps.default){
                $( this.refs.dateTime ).find('.dateTimeContainer input').datetimepicker('setDate',new Date(nextProps.default));
                this.setState({value: nextProps.default});
            }
        }

        componentDidMount() {
            $( this.refs.dateTime ).find('.dateTimeContainer input').datetimepicker({
                timeFormat: "HH:mm",
                dateFormat: this.props.presentation.format || 'dd/mm/yy',
                changeMonth: true,
                numberOfMonths: 1,
                showOn: (this.props.readOnly ? '' : 'focus'),
                onClose: function( selectedDate ) {
                    let date = $( this.refs.dateTime ).find('.dateTimeContainer input').datetimepicker('getDate');
                    this.setState({value: date.toISOString()});
                }.bind(this)
            });

            // The super secret property that stop dates in the future for some usages
            if(!!this.props.pastOnly) $( this.refs.dateTime ).find('.dateTimeContainer input').datetimepicker("option", "maxDate", new Date(this.state.value));

            $( this.refs.dateTime ).find('.dateTimeContainer input').datetimepicker('setDate',new Date(this.state.value));
        }

        isValid(data) {
            return !isNaN(Date.parse(data));
        }

        render() {
            if(Ganymede.pageData.debug) console.log("DateTime Render("+this.props.id+") - ",this.state.value);

            let mandatory = !!this.props.mandatory;
            let presentation = this.props.presentation || {};
            let id = 'dateTime'+this.props.id;
            let displayClass = (!!presentation.hidden)?' hide':'';
            let name = this.props.name;

            let content = [];
            content.push(React.createElement(DateTimeRender, {key:'component', tabIndex: this.props.enabled?1:-1, caption: presentation.caption, data: this.state.value, readOnly: this.props.readOnly}));
            if (this.state.touched && !this.isValid(this.state.value)){
                content.push(React.createElement('div', {key:'error', className: 'error hasError'+id, ref:'errorField'}));
            }

            let container;
            let metadataMessage = '';
            if (!!presentation.caption && presentation.caption.trim() === ''){
                container = React.createElement(SimpleContainer, {readOnly: this.props.readOnly, content: content, elementId: id, metadataMessage: metadataMessage});
            } else {
                if (mandatory){
                    container = React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, metadataMessage: metadataMessage});
                } else {
                    container = React.createElement(OptionalContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, onToggleEnable: this.onToggleEnable, metadataMessage: metadataMessage, enabled: this.state.enabled});
                }
            }

            if (this.props.simplified) {
                let container = React.createElement(DateTimeSimplified, {readOnly: this.props.readOnly, value: this.state.value, caption: presentation.caption});
                return React.createElement('div',{className: 'textBox date '+id+displayClass+' '+name + ' simplified'}, container);
            } else {
                return React.createElement('div', {className: 'textBox date '+id+displayClass+' '+name + ((this.props.simplified)?' simplified':''), ref:'dateTime', onClick:this.onTouch}, container);
            }
        }
    };

    class DateTimeSimplified extends React.Component {
        render() {
            let processedDate = new Date(this.props.value);
            let content = React.createElement('div', {className: 'dateFromTo'}, processedDate.toLocaleString('en-GB', { timeZone: 'GMT'}));
            return React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: this.props.caption, content: content, simplified: true});
        }
    }

    class DateTimeRender extends React.Component {
        constructor(props) {
            super(props);

            this.state = {
                data: this.props.data,
            };
        }

        render() {

            let content = React.createElement(FromDate, {tabIndex: this.props.tabIndex, data: this.state.data, readOnly: this.props.readOnly});
            return React.createElement('div', {className: 'dateTime'}, content);
        }
    }

    class FromDate extends React.Component {
        render() {

            let content = React.createElement('div', {className: 'dateContainer'},
                React.createElement('div', {className: 'containerContent'},
                    //React.createElement('div', {className: 'heading'}, this.props.caption),
                    React.createElement('div', {className: 'inputField'},
                        React.createElement('input', {tabIndex: this.props.tabIndex, ref:'inputField', readOnly: this.props.readOnly})
                    )
                )
            );

            return React.createElement('div', {className: 'dateTimeContainer'}, content);
        }
    }
} catch(e){
    console.error('an error was found:',e);
}
}());
