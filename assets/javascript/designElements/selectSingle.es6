(function() {
    try {
        window.SelectSingle = class SelectSingle extends GanymedeComponent {
            constructor(props) {
                super(props);
                this.updateValue = this.updateValue.bind(this);
                this.isValid = this.isValid.bind(this);
                // This is a select single, there must ALWAYS be something selected.....
                //...unless you don't want that
                let value;
                if(this.props.allowNoSelection){
                    value = this.props.default;
                }
                else {
                    value = this.props.default || this.props.presentation.options[0].value || '';
                }

                this.state.enabled = this.props.enabled;
                this.state.value = value;
            }

            updateValue(value) {
                if(value!==this.state.value || this.state.touched!==true)
                this.setState({ value: value, touched:true });
            }

            isValid(value) {
                if(this.props.allowNoSelection) {
                    if(this.state.newConstraints.minSelect <= 0) return true;
                    return (this.state.value!==null && this.state.value!==undefined);
                }
                else {
                    return true;
                }
            }

            render() {
                if(Ganymede.pageData.debug) console.log("SelectSingle Render("+this.props.id+") - ",this.state.value);

                let mandatory = !!this.props.mandatory;
                let presentation = this.props.presentation || {};
                let id = 'selectSingle' + this.props.id;
                let displayClass = (!!presentation.hidden) ? ' hide' : '';
                let name = this.props.name;

                let simpleValue;

                let content = [];

                if (!!presentation.display) {

                    let defaultValue = null;

                    for (let x = 0; x < presentation.options.length; x++) {
                        if (presentation.options[x].value === this.state.value) {
                            defaultValue = x;
                        }
                    }

                    if (!defaultValue) {
                        defaultValue = 0;
                    }

                    if (presentation.display.toLowerCase() === 'dropdown') {

                        let newDropDownProperties = {
                            key: 'dropdown',
                            id: id,
                            preSelectedOptionIndex: defaultValue,
                            options: presentation.options,
                            tabIndex: this.props.enabled ? 1 : -1,
                            heading: presentation.heading || presentation.caption,
                            updateValue: this.updateValue,
                            readOnly: this.props.readOnly
                        };

                        content.push(React.createElement(SelectSingleDropDown, newDropDownProperties));

                        simpleValue = presentation.options[defaultValue].value;

                    } else if (presentation.display.toLowerCase() === 'radio') {

                        let buttons = [];
                        if (!presentation.hasOwnProperty('default')) {
                            presentation['default'] = presentation.options[0].value;
                        }

                        for (let x = 0; x < presentation.options.length; x++) {
                            let option = presentation.options[x];
                            option['title'] = option.item;
                            buttons.push(option);
                        }
                        content.push(React.createElement(SelectSingleRadioButtons, {
                            key: 'radio',
                            tabIndex: this.props.enabled ? 1 : -1,
                            collectionName: id,
                            selected: this.state.value,
                            buttons: buttons,
                            updateValue: this.updateValue,
                            readOnly: this.props.readOnly
                        }));

                        buttons.forEach(button => {
                            if (button.value === this.state.value) {
                                simpleValue = button.title;
                            }
                        });
                    }
                    else if (presentation.display.toLowerCase() === 'radiobig') {

                        let buttons = [];
                        if (!presentation.hasOwnProperty('default')) {
                            presentation['default'] = presentation.options[0].value;
                        }

                        for (let x = 0; x < presentation.options.length; x++) {
                            let option = presentation.options[x];
                            option['title'] = option.item;
                            buttons.push(option);
                        }
                        content.push(React.createElement(SelectSingleTextRadioButtons, {
                            key: 'radiobig',
                            tabIndex: this.props.enabled ? 1 : -1,
                            collectionName: id,
                            selected: this.state.value,
                            text: {
                                on: "Selected",
                                off: "Select"
                            },
                            buttons: buttons,
                            updateValue: this.updateValue,
                            readOnly: this.props.readOnly
                        }));

                        buttons.forEach(button => {
                            if (button.value === this.state.value) {
                                simpleValue = button.title;
                            }
                        });

                    }
                    else if (presentation.display.toLowerCase() === 'buttonselect') {

                        let buttons = [];
                        if (!presentation.hasOwnProperty('default')) {
                            presentation['default'] = presentation.options[0].value;
                        }

                        for (let x = 0; x < presentation.options.length; x++) {

                            let option = presentation.options[x];
                            option['title'] = option.item;
                            buttons.push(option);
                        }
                        content.push(React.createElement(SelectSingleButtonSelect, {
                            key: 'butonselect',
                            tabIndex: this.props.enabled ? 1 : -1,
                            collectionName: id,
                            selected: this.state.value,
                            buttons: buttons,
                            updateValue: this.updateValue,
                            readOnly: this.props.readOnly
                        }));

                        buttons.forEach(button => {
                            if (button.value === this.state.value) {
                                simpleValue = button.title;
                            }
                        });

                    } else {
                        console.error('selectSingle display type is not valid.');
                    }
                }

                if (!this.isValid(this.state.value) && this.state.touched) {
                    let validateFieldResponse = Ganymede.formValidator(this.state.value, this.state.newConstraints);
                    content.push(React.createElement('div',{ key: 'error', className: 'error hasError ' + id },validateFieldResponse));
                }

                let container;

                if (!!presentation.caption && presentation.caption.trim() === '') {
                    container = React.createElement(SimpleContainer, {
                        readOnly: this.props.readOnly,
                        content: content,
                        elementId: id
                    });
                }
                else {
                    if (mandatory) {
                        container = React.createElement(MandatoryContainer, {
                            readOnly: this.props.readOnly,
                            caption: presentation.caption,
                            content: content,
                            elementId: id,
                            metadataMessage: this.state.metadataMessage
                        });
                    } else {
                        container = React.createElement(OptionalContainer, {
                            readOnly: this.props.readOnly,
                            caption: presentation.caption,
                            content: content,
                            elementId: id,
                            onToggleEnable: this.onToggleEnable,
                            metadataMessage: this.state.metadataMessage,
                            enabled: this.state.enabled
                        });
                    }
                }

                if (this.props.simplified) {
                    let container = React.createElement(SelectSingleSimplified, {readOnly: this.props.readOnly, value: simpleValue, caption: presentation.caption});
                    return React.createElement('div',{className: 'selectSingle ' + id + displayClass + ' ' + name + ' simplified'}, container);
                } else {

                    return React.createElement('div', {
                        className: 'selectSingle ' + id + displayClass + ' ' + name
                    }, container);
                }
            }
        };

        class SelectSingleSimplified extends React.Component {
            render() {
                let content = React.createElement('div', {}, this.props.value);
                return React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: this.props.caption, content: content, simplified: true});
            }
        }



        window.SelectSingleDropDown = class SelectSingleDropDown extends React.Component {
            constructor(props) {
                super(props);
                this.togglePopoutVisibility = this.togglePopoutVisibility.bind(this);
                this.toggleSelected = this.toggleSelected.bind(this);
                this.updateSelectedOption = this.updateSelectedOption.bind(this);
                this.toggleOptionsVisibility = this.toggleOptionsVisibility.bind(this);
                this.selectSingleDropDown = {};

                this.state = {
                    preselectedOption: this.props.preSelectedOptionIndex || 0,
                    popOutContainerVisibility: false,
                    selectedValueIsSelected: false,
                    selectSingleDropDown: {}
                };
            }

            togglePopoutVisibility() {
                if(!this.props.readOnly){
                  this.setState({ popOutContainerVisibility: !this.state.popOutContainerVisibility });
                }
            }

            toggleSelected() {
              if(!this.props.readOnly) {
                this.setState({ selectedValueIsSelected: !this.state.selectedValueIsSelected });
              }
            }

            updateSelectedOption(newSelectedIndex) {
              if(!this.props.readOnly) {
                this.setState({ preselectedOption: newSelectedIndex });
              }
            }

            componentDidMount() {
                this.selectSingleDropDown = this.refs.selectSingleDropDown;
            }

            toggleOptionsVisibility() {
                this.togglePopoutVisibility();
                this.toggleSelected();
            }

            render() {
                let dropDownId = this.props.id;
                let heading = this.props.heading || '';

                let content = React.createElement('div', { className: 'dropDownContent' },
                    React.createElement('div', { className: 'heading' }, heading),
                    React.createElement('div', { className: 'content'},
                        React.createElement(SelectSingleContent, {
                            options: this.props.options,
                            preselectedOption: this.state.preselectedOption,
                            id: dropDownId,
                            selectSingleDropDown: this.selectSingleDropDown,
                            updateSelectedOption: this.updateSelectedOption,
                            popOutContainerVisibility: this.state.popOutContainerVisibility,
                            selectedValueIsSelected: this.state.selectedValueIsSelected,
                            updateValue: this.props.updateValue,
                            toggleOptionsVisibility: this.toggleOptionsVisibility
                        })));

                return React.createElement('div', {
                    className: (this.props.readOnly ? '' : 'clickable ') + 'selectSingleDropDown noSelect ' + dropDownId,
                    onClick: this.toggleOptionsVisibility,
                    ref: 'selectSingleDropDown'
                }, content);
            }
        };

        class SelectSingleContent extends React.Component {
            constructor() {
                super();
                this.setPopOutContainer = this.setPopOutContainer.bind(this);

                this.state = {
                    selectSinglePopOutContainer: {}
                };
            }

            setPopOutContainer(element) {
                this.setState({
                    selectSinglePopOutContainer: element
                });
            }

            componentDidUpdate() {
                if(Object.keys(this.state.selectSinglePopOutContainer).length >0 && Object.keys(this.props.selectSingleDropDown).length > 0) {
                    $(this.state.selectSinglePopOutContainer).outerWidth($(this.props.selectSingleDropDown).outerWidth());
                }
            }

            render() {

                let options = this.props.options;
                let preselectedOption = this.props.preselectedOption || 0;
                let defaultValue = React.createElement('div', {
                    className: 'selectedValue ' + (this.props.selectedValueIsSelected ? '' : 'selected')
                }, options[preselectedOption].item);
                let listItemRenderer = (this.props.hasOwnProperty('listItemRenderer')) ? this.props.listItemRenderer : function(data) {
                    let selectedClass = (data.isSelected) ? ' selected' : '';
                    return React.createElement('div', {
                        className: 'selectSinglePopOutListItemData' + selectedClass
                    }, data.item);
                };

                let popOut = React.createElement(SelectSinglePopOut, {
                    setPopOutContainer: this.setPopOutContainer,
                    options: options,
                    preSelectedOption: preselectedOption,
                    listItemRenderer: listItemRenderer,
                    id: this.props.id,
                    updateSelectedOption: this.props.updateSelectedOption,
                    popOutContainerVisibility: this.props.popOutContainerVisibility,
                    updateValue: this.props.updateValue
                });

                let backGround = React.createElement('div', {
                    className: 'selectSinglePopOutContainerBackground' + (this.props.popOutContainerVisibility ? '' : ' hide'),
                    onClick: this.props.toggleOptionsVisibility
                });

                return React.createElement('div', {
                    className: 'selectSingleContent ' + this.props.id
                }, defaultValue, backGround, popOut);
            }
        }

        class SelectSinglePopOut extends React.Component {
            componentDidMount() {
                this.props.setPopOutContainer(this.refs.selectSinglePopOutContainer);
            }

            render() {

                let popOutMenu = React.createElement(SelectSinglePopOutList, {
                    options: this.props.options,
                    preSelectedOption: this.props.preselectedOption,
                    listItemRenderer: this.props.listItemRenderer,
                    id: this.props.id,
                    updateSelectedOption: this.props.updateSelectedOption,
                    updateValue: this.props.updateValue
                });

                return React.createElement('div', {
                    className: 'selectSinglePopOutContainer ' + (this.props.popOutContainerVisibility ? '' : 'hide'),
                    ref: 'selectSinglePopOutContainer'
                }, popOutMenu);
            }
        }

        class SelectSinglePopOutList extends React.Component {
            constructor() {
                super();

                this.state = {
                    identifier: 'SelectSinglePopOutList' + Math.floor(Math.random()*Math.random()*1000)
                };
            }

            render() {

                let content = [];
                for (let x = 0; x < this.props.options.length; x++) {

                    let preselectedOption = this.props.preselectedOption;
                    let isSelected = (x === preselectedOption);
                    let key = this.state.identifier + x;

                    let data = this.props.options[x];
                    data.isSelected = isSelected;
                    data.index = x;

                    content.push(React.createElement(SelectSinglePopOutListItem, {
                        key: key,
                        data: data,
                        options: this.props.options,
                        listItemRenderer: this.props.listItemRenderer,
                        id: this.props.id,
                        updateSelectedOption: this.props.updateSelectedOption,
                        updateValue: this.props.updateValue
                    }));
                }

                return React.createElement('ul', {
                    className: 'selectSinglePopOutList'
                }, content);
            }
        }

        class SelectSinglePopOutListItem extends React.Component {
            constructor() {
                super();
                this.selectOption = this.selectOption.bind(this);
            }

            selectOption() {

                this.props.updateValue(this.props.options[this.props.data.index].value);
                this.props.updateSelectedOption(this.props.data.index);
                // $('.selectSingleDropDown.'+this.props.id+' .selectSinglePopOutContainer').toggleClass('hide');
                // $('.selectSingleDropDown.'+this.props.id+' .selectedValue').toggleClass('selected');
            }

            render() {

                let listItem = this.props.listItemRenderer(this.props.data);

                return React.createElement('li', {
                    className: 'selectSinglePopOutListItem',
                    onClick: this.selectOption
                }, listItem);
            }
        }

        window.SelectSingleRadioButtons = class SelectSingleRadioButtons extends React.Component {
            render() {

                let buttons = [];
                for (let x = 0; x < this.props.buttons.length; x++) {
                    let key = this.props.collectionName + x;
                    buttons.push(React.createElement(SelectSingleRadioButton, {
                        key: key,
                        tabIndex: this.props.tabIndex,
                        collectionName: this.props.collectionName,
                        selected: this.props.selected,
                        button: this.props.buttons[x],
                        updateValue: this.props.updateValue,
                        readOnly: this.props.readOnly
                    }));
                }

                return React.createElement('div', {
                    className: 'radioButtons'
                }, buttons);
            }
        };

        class SelectSingleRadioButton extends React.Component {
            constructor() {
                super();
                this.selectRadioButton = this.selectRadioButton.bind(this);

                this.state = {
                    identifier: Math.floor(Math.random()*Math.random()*1000)
                };
            }

            selectRadioButton() {
                //this.props.button.elementData.value = this.props.button.value;
                this.props.updateValue(this.props.button.value);
            }

            render() {

                let checked = ((this.props.button.value + '').toLowerCase() === (this.props.selected + '').toLowerCase()) ? true : false;

                let button = React.createElement('input', {
                    tabIndex: this.props.tabIndex,
                    type: 'radio',
                    name: this.props.collectionName,
                    id: this.state.identifier,
                    value: this.props.button.value,
                    checked: checked,
                    onChange: this.selectRadioButton,
                    disabled: this.props.readOnly
                });
                let customButton = React.createElement('label', {
                    className: 'radioButton ' + this.props.button.value,
                    htmlFor: this.state.identifier
                });
                let label = React.createElement('label', {
                    className: 'radioLabel ' + (this.props.readOnly ?  '' : 'clickable '),
                    htmlFor: this.state.identifier
                }, this.props.button.title);
                return React.createElement('div', {
                    className: 'radioButtonContainer noSelect',
                    ref: 'radioButton'
                }, button, customButton, label);
            }
        }

        window.SelectSingleTextRadioButtons = class SelectSingleTextRadioButtons extends React.Component {
            // componentWillMount: function() {
            //     if (typeof(this.props.selected) != "string") {
            //         debugger;
            //         this.props.updateValue(this.props.buttons[0].value);
            //     }
            // },

            render() {


                let buttons = [];
                for (let x = 0; x < this.props.buttons.length; x++) {
                    buttons.push(React.createElement(SelectSingleTextRadioButton, {
                        tabIndex: this.props.tabIndex,
                        collectionName: this.props.collectionName,
                        selected: (this.props.selected === "") ? this.props.buttons[0].value : this.props.selected,
                        text: this.props.text,
                        button: this.props.buttons[x],
                        updateValue: this.props.updateValue,
                        key: 'singleselectTextRadioButton' + x,
                        readOnly: this.props.readOnly
                    }));
                }
                return React.createElement('div', {
                    className: 'textRadioSelect noSelect',
                    key: 'selectsingletextradiobutton'
                }, buttons);
            }
        };

        class SelectSingleTextRadioButton extends React.Component {
            constructor() {
                super();
                this.selectRadioButton = this.selectRadioButton.bind(this);

                this.state = {
                    identifier: Math.floor(Math.random()*Math.random()*1000)
                };
            }

            selectRadioButton() {
                if(!this.props.readOnly){
                  //this.props.button.elementData.value = this.props.button.value;
                  this.props.updateValue(this.props.button.value);
                }
            }

            render() {

                let checked = (this.props.button.value === this.props.selected) ? true : false;
                let button = React.createElement('input', {
                    tabIndex: this.props.tabIndex,
                    type: 'radio',
                    name: this.props.collectionName,
                    id: this.state.identifier,
                    value: this.props.button.value,
                    checked: checked,
                    onChange: this.selectRadioButton,
                    disabled: this.props.readOnly
                });
                let label = React.createElement('label', {
                        className: 'radioLabel ' + (this.props.readOnly ? '' : 'clickable '),
                        htmlFor: this.state.identifier
                    },
                    React.createElement('div', {}, checked ? this.props.text.on : this.props.text.off)
                );
                let titleText = React.createElement('label', {
                    className: 'ruleLabel ' + (this.props.readOnly ? '' : 'clickable '),
                    htmlFor: this.state.identifier
                }, this.props.button.title);

                return React.createElement('div', {
                    className: 'textRadioSelectContainer',
                    onClick: this.selectRadioButton
                }, titleText, button, label);
            }
        }

        class SelectSingleButtonSelect extends React.Component {
            render() {

                let buttons = [];

                for (let x = 0; x < this.props.buttons.length; x++) {
                    buttons.push(React.createElement(SelectSingleButtonSelectButton, {
                        tabIndex: this.props.tabIndex,
                        collectionName: this.props.collectionName,
                        selected: this.props.selected,
                        button: this.props.buttons[x],
                        updateValue: this.props.updateValue,
                        readOnly: this.props.readOnly
                    }));
                }

                return React.createElement('div', {
                    className: 'buttonSelect noSelect'
                }, buttons);
            }
        }

        class SelectSingleButtonSelectButton extends React.Component {
            constructor() {
                super();
                this.selectRadioButton = this.selectRadioButton.bind(this);

                this.state = {
                    identifier: Math.floor(Math.random()*Math.random()*1000)
                };
            }

            selectRadioButton() {
                //this.props.button.elementData.value = this.props.button.value;
                if(!this.props.readOnly){
                  this.props.updateValue(this.props.button.value);
                }
            }

            render() {

                let checked = (this.props.button.value === this.props.selected) ? true : false;

                let button = React.createElement('input', {
                    tabIndex: this.props.tabIndex,
                    type: 'radio',
                    name: this.props.collectionName,
                    id: this.state.identifier,
                    value: this.props.button.value,
                    //defaultChecked: checked,
                    checked: checked,
                    disabled: this.props.readOnly
                });
                let label = React.createElement('label', {
                        className: 'radioLabel ' + (this.props.readOnly ? '' : 'clickable '),
                        htmlFor: this.state.identifier
                    },
                    React.createElement('div', {}, this.props.button.title)
                );

                return React.createElement('div', {
                    className: 'buttonSelectContainer',
                    onClick: this.selectRadioButton
                }, button, label);
            }
        }
    } catch (e) {
        console.error('an error was found:', e);
    }
}());
