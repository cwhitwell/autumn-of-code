(function(){
    try {
        window.TimeFromTo = class TimeFromTo extends GanymedeComponent {
            constructor(props) {
                super(props);
                this.isValid = this.isValid.bind(this);

                this.name = 'TimeFromTo';
                this.state.value = $.extend(true, [], this.props.default);
            }

            componentDidMount() {

                let defaultValue = [];
                let self = this;

                for (let x = 0; x < this.state.value.length; x++){

                    let timeComponents = this.state.value[x].value.split(':');
                    let timeValue = (parseInt(timeComponents[0])*12)+(parseInt(timeComponents[1])/5);

                    defaultValue.push(timeValue);
                }

                function updateTextLabels(event, ui){
                    if(self.props.readOnly !== true){
                      let fromTimeValue = ((Math.floor(ui.values[0]/12) < 10)?'0':'')+Math.floor(ui.values[0]/12)+':'+((Math.floor(ui.values[0]%12)*5 < 10)?'0':'')+(Math.floor(ui.values[0]%12)*5);
                      let toTimeValue = ((Math.floor(ui.values[1]/12) < 10)?'0':'')+Math.floor(ui.values[1]/12)+':'+((Math.floor(ui.values[1]%12)*5 < 10)?'0':'')+(Math.floor(ui.values[1]%12)*5);

                      $($(self.refs.sliderInput).find('.label')[0]).html(fromTimeValue);
                      $($(self.refs.sliderInput).find('.label')[1]).html(toTimeValue);

                      let newValue = $.extend(true,[],self.state.value);
                      newValue[0].value = fromTimeValue;
                      newValue[1].value = toTimeValue;
                      self.setState({value: newValue});
                    }
                    else{
                        event.preventDefault();
                    }
                }

                $(this.refs.sliderInput).slider({
                    range: true,
                    min: 0,
                    max: 288,
                    values: defaultValue,
                    slide: updateTextLabels,
                    create: function(){
                        let handles = $(self.refs.sliderInput).find('.ui-slider-handle');

                        for (let x = 0; x < handles.length; x++){
                            let timeValue = ((Math.floor(defaultValue[x]/12) < 10)?'0':'')+Math.floor(defaultValue[x]/12)+':'+((Math.floor(defaultValue[x]%12)*5 < 10)?'0':'')+(Math.floor(defaultValue[x]%12)*5);
                            if(x%2 == 0){
                                $(handles[x]).append('<div class="label label-top">'+timeValue+'</div>');
                            }
                            else {
                                $(handles[x]).append('<div class="label label-bottom">'+timeValue+'</div>');
                            }
                        }
                        $('.ui-slider-handle').attr('tabindex', (this.state.enabled?1:-1)+'');
                    }.bind(this)
                });
            }

            isValid(data) {
                return true;
            }

            render() {
                if(Ganymede.pageData.debug) console.log("TimeFromTo Render("+this.props.id+") - ",this.state.value);
                
                let mandatory = !!this.props.mandatory;
                let presentation = this.props.presentation || {};
                let id = 'timeFromTo'+this.props.id;
                let displayClass = (!!presentation.hidden)?' hide':'';
                let name = this.props.name;

                let content = [];

                content.push(React.createElement('div', {key:'component', className: 'sliderInput '+id, ref:'sliderInput'}));

                // No error state for this component
                // if (this.state.touched){
                //     content.push(React.createElement('div', {key:'error', className: 'error '+id, ref:'errorField'}));
                // }

                let container;
                let metadataMessage = '';
                if (!!presentation.caption && presentation.caption.trim() === ''){
                    container = React.createElement(SimpleContainer, {readOnly: this.props.readOnly, content: content, elementId: id, metadataMessage: metadataMessage});
                } else {
                    if (mandatory){
                        container = React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, metadataMessage: metadataMessage});
                    } else {
                        container = React.createElement(OptionalContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, onToggleEnable: this.onToggleEnable, metadataMessage: metadataMessage, enabled: this.state.enabled});
                    }
                }
                if (this.props.simplified) {
                    let container = React.createElement(TimeFromToSimplified, {readOnly: this.props.readOnly, data: this.state.value, caption: presentation.caption});
                    return React.createElement('div',{className: 'textBox timeFromTo '+id+displayClass+' '+name + ' simplified'}, container);
                } else {
                    return React.createElement('div', {className: 'textBox timeFromTo '+id+displayClass+' '+name + ((this.props.simplified)?' simplified':''), ref:'timeFromTo'}, container);
                }
            }
        };

        class TimeFromToSimplified extends React.Component {
            constructor(props) {
                super(props);

                this.state = {
                    fromTime: this.props.data[0],
                    toTime: this.props.data[1]
                };
            }

            render() {

                let startTime = React.createElement('div', {className: 'startTime'},
                    React.createElement('div',{className: 'timeHeading'}, this.state.fromTime.item),
                    React.createElement('div', {className: 'timeValue'}, this.state.fromTime.value)
                );
                let endTime = React.createElement('div', {className: 'toTime'},
                    React.createElement('div',{className: 'timeHeading'}, this.state.toTime.item),
                    React.createElement('div', {className: 'timeValue'}, this.state.toTime.value)
                );

                let content = React.createElement('div', {className: 'timeFromToSimplified'}, startTime, endTime);
                return React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: this.props.caption, content: content, simplified: true});
            }
        }
    } catch(e){
        console.error('an error was found:',e);
    }
}());

