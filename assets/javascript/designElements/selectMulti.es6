(function(){
    try {
        window.SelectMulti = class SelectMulti extends GanymedeComponent {
            constructor(props) {
                super(props);
                this.toggleValue = this.toggleValue.bind(this);
                this.isValid = this.isValid.bind(this);

                let optionValues = $.extend(true,[],this.props.presentation.options);
                let value = [];
                let dataFormat = this.props.presentation.data || 'boolean';

                for (let x = 0; x < optionValues.length; x++){
                    if (dataFormat === 'boolean'){
                        value.push(optionValues[x].default || false);
                    }
                    else {
                        if (optionValues[x].default) {
                            value.push(optionValues[x].value || optionValues[x].item);
                        }
                    }
                }

                this.name = 'SelectMulti';
                this.state.value = value;
                this.state.optionValues = optionValues;
                this.state.dataFormat = dataFormat;
            }

            toggleValue(index) {
                let optionValues = $.extend(true,[],this.state.optionValues);
                let newValue = [];

                optionValues[index].default = !optionValues[index].default;

                for (let x = 0; x < optionValues.length; x++){
                    if (this.state.dataFormat === 'boolean'){
                        newValue.push(optionValues[x].default);
                    } else {
                        if (optionValues[x].default) {
                            newValue.push(optionValues[x].value || optionValues[x].item);
                        }
                    }
                }
                // No need to override the default change detectors are value & optionValues will always change in sync
                this.setState({value: newValue, optionValues:optionValues, touched: true});
            }


            validationResult(data) {
                let selected = data.filter(function(val){return val;}).length;
                if (selected < this.state.newConstraints.minSelect){
                    return 'Minimum items selected must be: ' + this.state.newConstraints.minSelect;
                }
                if (selected > this.state.newConstraints.maxSelect){
                    return 'Maximum items selected must be: ' + this.state.newConstraints.maxSelect;
                }
                return '';
            }

            isValid(data) {
                return this.validationResult(data)==='';
            }

            render() {
                if(Ganymede.pageData.debug) console.log("SelectMulti Render("+this.props.id+") - ",this.state.value);

                let mandatory = !!this.props.mandatory || this.props.simplified;
                let presentation = this.props.presentation || {};
                let id = 'selectMulti'+this.props.id;
                let displayClass = (!!presentation.hidden)?' hide':'';
                let name = this.props.name;

                let content = [];

                for (let x = 0; x < presentation.options.length; x++){
                    content.push(React.createElement(SelectMultiTickbox, {tabIndex: this.props.enabled?1:-1, data: this.state.optionValues[x], key: id+x, elementIndex: x, id: id+x, toggleValue: this.toggleValue, checked:!!this.state.optionValues[x].default, readOnly: this.props.readOnly}));
                }

                if (this.state.touched && !this.isValid(this.state.value)) {
                    let validateFieldResponse = this.validationResult(this.state.value);
                    content.push(React.createElement('div', {key:'error', className: 'error hasError '+id, ref:'errorField'},validateFieldResponse));
                }

                if (!!presentation.design && typeof presentation.design.length === 'number'){
                    for (let x = 0; x < presentation.design.length; x++){
                        let element = presentation.design[x];

                        let elementProperties = {
                            mandatory: (element.mandatory||false),
                            presentation: element.presentation,
                            id: element.id,
                            constraints: element.constraints,
                            name: element.name
                        };

                        content.push(React.createElement(window[element.type], elementProperties));
                    }
                }

                let container;
                let metadataMessage = '';
                if (!!presentation.caption && presentation.caption.trim() === ''){
                    container = React.createElement(SimpleContainer, {readOnly: this.props.readOnly, content: content, elementId: id, metadataMessage: metadataMessage});
                } else {
                    if (mandatory){
                        container = React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, metadataMessage: metadataMessage});
                    } else {
                        container = React.createElement(OptionalContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, onToggleEnable: this.onToggleEnable, metadataMessage: metadataMessage, enabled: this.state.enabled});
                    }
                }

                if (this.props.simplified) {
                    let container = React.createElement(SelectMultiSimplified, {readOnly: this.props.readOnly, values: this.state.optionValues, caption: presentation.caption});
                    return React.createElement('div',{className: 'selectMulti tickBox '+id+displayClass+' '+name + ' simplified'}, container);
                } else {
                    return React.createElement('div', {className: 'selectMulti tickBox '+id+displayClass+' '+name }, container);
                }
            }
        };

        class SelectMultiSimplified extends React.Component {
            render() {
                let trueValues = [];
                this.props.values.forEach(function (value) {
                    if (value.default) {
                        trueValues.push(value.item);
                    }
                });
                let trueValuesString = trueValues.join(', ');
                let content = React.createElement('div', {}, trueValuesString);
                return React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: this.props.caption, content: content, simplified: true});
            }
        }

        class SelectMultiTickbox extends React.Component {
            constructor() {
                super();
                this.toggleCheckBox = this.toggleCheckBox.bind(this);
            }

            toggleCheckBox() {
                if(!this.props.readOnly){
                  this.props.toggleValue(this.props.elementIndex);
                }
            }

            render() {

                let identifier = this.props.id;
                let defaultValue = this.props.checked;
                let key = 0;

                let tickBox = [];
                tickBox.push(React.createElement('input', {tabIndex: this.props.tabIndex, key: key++, type: 'checkbox', id: identifier, defaultChecked: defaultValue, 'disabled': this.props.readOnly}));

                tickBox.push(React.createElement('label', {key: key++, className: 'checkBox', htmlFor: identifier, onClick: this.toggleCheckBox},
                    React.createElement('img', {src: 'assets/images/core/icon_tick.png'})
                ));
                tickBox.push(React.createElement('label', {key: key++, className: 'checkBox-name', htmlFor: identifier, onClick: this.toggleCheckBox},
                    React.createElement('div', null, this.props.data.item)
                ));

                return React.createElement('div', {className: 'tickBoxContainer'}, tickBox);

            }
        }
    } catch(e){
        console.error('an error was found:',e);
    }
}());
