(function(){
try {
    window.DateTimeFromTo = class DateTimeFromTo extends GanymedeComponent {
        constructor(props) {
            super(props);
            this.isValid = this.isValid.bind(this);

            this.name = 'DateTimeFromTo';
            let value = (this.props.default || [{item:'From: ',value:(new Date()).toISOString()},{item:'To: ',value:(new Date()).toISOString()}]);
            value[0].value = value[0].value || (new Date()).toISOString();
            value[1].value = value[1].value || (new Date()).toISOString();

            this.state.value = value;
        }

        componentWillReceiveProps(nextProps) {
            GanymedeComponent.prototype.componentWillReceiveProps.call(this,nextProps);

            if (!!nextProps.shouldUpdateValueOnNewDefaultValue && !!nextProps.default){
                $( this.refs.dateFromTo ).find('.fromDateContainer input').datetimepicker('setDate',new Date(nextProps.default[0].value));
                $( this.refs.dateFromTo ).find('.toDateContainer input').datetimepicker('setDate',new Date(nextProps.default[1].value));
                this.setState({value: $.extend(true,[],nextProps.default)});
            }

        }

        componentDidMount() {
            let fromDate = $( this.refs.dateFromTo ).find('.fromDateContainer input');
            let toDate = $( this.refs.dateFromTo ).find('.toDateContainer input');

            // FROM FROM FROM
            fromDate.datetimepicker({
                timeFormat: "HH:mm",
                dateFormat: this.props.presentation.format || 'dd/mm/yy',
                changeMonth: true,
                numberOfMonths: 1,
                maxDate: new Date(this.state.value[1].value),
                onClose: (dateText, inst) => {
                    let fromDateValue = fromDate.datetimepicker('getDate');
                    let toDateValue = toDate.datetimepicker('getDate');
                    let newValue = $.extend(true, [], this.state.value);

                    if (toDate.val() != '') {
                        if (fromDateValue > toDateValue){
                            toDate.datetimepicker('setDate', fromDateValue);
                            newValue[1].value = fromDateValue.toISOString();
                        }
                    }
                    else {
                        toDate.val(dateText);
                    }

                    newValue[0].value = fromDateValue.toISOString();
                    this.setState({value: newValue});
                },
                onSelect: selectedDateTime => {
                    toDate.datetimepicker('option', 'minDate', fromDate.datetimepicker('getDate') );
                }
            });
            fromDate.datetimepicker('setDate',new Date(this.state.value[0].value));

            // TO TO TO
            toDate.datetimepicker({
                timeFormat: "HH:mm",
                dateFormat: this.props.presentation.format || 'dd/mm/yy',
                changeMonth: true,
                numberOfMonths: 1,
                minDate: new Date(this.state.value[0].value),
                onClose: (dateText, inst) => {
                    let toDateValue = toDate.datetimepicker('getDate');
                    let fromDateValue = fromDate.datetimepicker('getDate');
                    let newValue = $.extend(true, [], this.state.value);

                    if (fromDate.val() != '') {
                        if (fromDateValue > toDateValue){
                            fromDate.datetimepicker('setDate', toDateValue);
                            newValue[0].value = toDateValue.toISOString();
                        }
                    }
                    else {
                        fromDate.val(dateText);
                    }

                    newValue[1].value = toDateValue.toISOString();
                    this.setState({value: newValue});
                },
                onSelect: selectedDateTime => {
                    fromDate.datetimepicker('option', 'maxDate', toDate.datetimepicker('getDate') );
                }
            });
            // The super secret property that stop dates in the future for some usages
            if(!!this.props.pastOnly) toDate.datetimepicker("option", "maxDate", '+0');

            toDate.datetimepicker('setDate',new Date(this.state.value[1].value));
        }

        isValid(data) {
            return !isNaN(Date.parse(data[0].value)) && !isNaN(Date.parse(data[1].value));
        }

        render() {
            if(Ganymede.pageData.debug) console.log("DateTimeFromTo Render("+this.props.id+") - ",this.state.value);

            let mandatory = !!this.props.mandatory;
            let presentation = this.props.presentation || {};
            let id = 'dateFromTo'+this.props.id;
            let displayClass = (!!presentation.hidden)?' hide':'';
            let name = this.props.name;

            let content = [];
            content.push(React.createElement(DataFromToRender, {key:'component', tabIndex: this.props.enabled?1:-1, caption: presentation.caption, data: this.state.value}));
            if (this.state.touched){
                content.push(React.createElement('div', {key:'error', className: 'error '+id, ref:'errorField'}));
            }

            let container;
            let metadataMessage = '';
            if (!!presentation.caption && presentation.caption.trim() === ''){
                container = React.createElement(SimpleContainer, {readOnly: this.props.readOnly, content: content, elementId: id, metadataMessage: metadataMessage});
            } else {
                if (mandatory){
                    container = React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, metadataMessage: metadataMessage});
                } else {
                    container = React.createElement(OptionalContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, onToggleEnable: this.onToggleEnable, metadataMessage: metadataMessage, enabled: this.state.enabled});
                }
            }

            if (this.props.simplified) {
                let container = React.createElement(DateTimeFromToSimplified, {readOnly: this.props.readOnly, data: this.state.value, caption: presentation.caption});
                return React.createElement('div', {className: 'textBox date '+id+displayClass+' '+name + ' simplified', ref:'dateFromTo', onClick:this.onTouch}, container);
            } else {
                return React.createElement('div', {className: 'textBox date '+id+displayClass+' '+name, ref:'dateFromTo', onClick:this.onTouch}, container);
            }


        }
    }

    class DateTimeFromToSimplified extends React.Component {
        constructor(props) {
            super(props);

            let fdate = new Date(this.props.data[0].value);
            let fromDateYear = fdate.getFullYear();
            let fromDateMonth = (fdate.getMonth() + 1);
            if (fromDateMonth < 10) {fromDateMonth = '0' + (fdate.getMonth() + 1);}
            let fromDateDate = fdate.getDate();
            if (fromDateDate < 10) {fromDateDate = '0' + fdate.getDate().toString();}
            let fromTimeHour = fdate.getHours();
            let fromTimeMin = fdate.getMinutes();

            let tdate = new Date(this.props.data[1].value);
            let toDateYear = tdate.getFullYear();
            let toDateMonth = tdate.getMonth() + 1;
            if (toDateMonth < 10) {toDateMonth = '0' + (tdate.getMonth() + 1);}
            let toDateDate = tdate.getDate();
            if (toDateDate < 10) {toDateDate = '0' + tdate.getDate();}
            let toTimeHour = tdate.getHours();
            let toTimeMin = tdate.getMinutes();

            let fromRes = '';
            let toRes = '';

            "dd/MM/yy".split('/').forEach(element => {
                if (element.toLowerCase() === 'dd') {
                    fromRes += fromDateDate + '/';
                    toRes += toDateDate + '/';
                } else if (element.toLowerCase() === 'mm') {
                    fromRes += fromDateMonth + '/';
                    toRes += toDateMonth + '/';
                }
            });

            fromRes += fromDateYear + ' ' + fromTimeHour + ':' + fromTimeMin;
            toRes += toDateYear + ' ' + toTimeHour + ':' + toTimeMin;

            this.state = {
                fromDate: fromRes,
                toDate: toRes
            };
        }

        render() {

            let startDate = React.createElement('div', {className: 'startDate'},
                React.createElement('div',{className: 'dateHeading'}, this.props.data[0].item),
                React.createElement('div', {className: 'dateValue'}, this.state.fromDate)
            );
            let endDate = React.createElement('div', {className: 'toDate'},
                React.createElement('div',{className: 'dateHeading'}, this.props.data[1].item),
                React.createElement('div', {className: 'dateValue'}, this.state.toDate)
            );

            let content = React.createElement('div', {className: 'dateFromTo'}, startDate, endDate);
            return React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: this.props.caption, content: content, simplified: true});
        }
    }

    class DataFromToRender extends React.Component {
        constructor(props) {
            super(props);

            this.state = {
                fromData: this.props.data[0],
                toData: this.props.data[1]
            };
        }

        componentWillMount() {
            if (this.props.data.length != 2){
                console.error('the data is incorrect. expected 2 objects.', this.props.data);
            }
        }

        render() {
            let content = [];
            content.push(React.createElement(FromDate, {tabIndex: this.props.tabIndex, key:'from',caption: this.state.fromData.item , data: this.state.fromData})); //, updateData: this.updateFromData}));
            content.push(React.createElement(ToDate, {tabIndex: this.props.tabIndex, key:'to',caption: this.state.toData.item , data: this.state.toData})); //, updateData: this.updateToData}));

            return React.createElement('div', {className: 'dateTimeFromTo'}, content);
        }
    }

    class FromDate extends React.Component {
        render() {
            let content = React.createElement('div', {className: 'dateContainer'},
                React.createElement('div', {className: 'containerContent'},
                    React.createElement('div', {className: 'heading'}, this.props.caption),
                    React.createElement('div', {className: 'inputField'},
                        React.createElement('input', {tabIndex: this.props.tabIndex, ref:'inputField'})
                    )
                )
            );

            return React.createElement('div', {className: 'fromDateContainer'}, content);
        }
    }

    class ToDate extends React.Component {
        render() {
            let content = React.createElement('div', {className: 'dateContainer'},
                React.createElement('div', {className: 'containerContent'},
                    React.createElement('div', {className: 'heading'}, this.props.caption),
                    React.createElement('div', {className: 'inputField'},
                        React.createElement('input', {tabIndex: this.props.tabIndex, ref:'inputField'})
                    )
                )
            );

            return React.createElement('div', {className: 'toDateContainer'}, content);
        }
    }
} catch(e){
    console.error('an error was found:',e);
}
}());
