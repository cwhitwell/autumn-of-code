(function(){
try {
    window.DateFromTo = class DateFromTo extends GanymedeComponent {
        constructor(props) {
            super(props);
            this.dateToNeutralFormat = this.dateToNeutralFormat.bind(this);
            this.neutralFormatToDate = this.neutralFormatToDate.bind(this);
            this.validationResult = this.validationResult.bind(this);
            this.isValid = this.isValid.bind(this);

            let value = $.extend(true,[],this.props.default || [{},{}]);
            value[0].value = value[0].value || this.dateToNeutralFormat(new Date());
            value[1].value = value[1].value || this.dateToNeutralFormat(new Date());

            // Update/add our state vars
            this.name = "DateFromTo";
            this.state.value = value;
            this.state.touchedFrom=(!!this.props.default && !!this.props.default[0].value);
            this.state.touchedTo=(!!this.props.default && !!this.props.default[1].value);
        }

        // **********************************************************************************************************
        //
        // Because this component is special and has validators outside of the value itself then we cannot just
        // use the base class methods for should/did update, we need to override and implement our own that take
        // into account the fact or the non value validation vars
        //
        // **********************************************************************************************************

        shouldComponentUpdate(nextProps,nextState) {
            if(GanymedeComponent.prototype.shouldComponentUpdate.call(this,nextProps,nextState)) return true;
            if(nextState.touchedFrom!==this.state.touchedFrom) return true;
            if(nextState.touchedTo!==this.state.touchedTo) return true;
            return false;
        }

        componentDidUpdate(prevProps, prevState) {
            // If any params have changed lets call back....
            let hasChanged = (!deepEqual(this.state.value,prevState.value) ||
                this.isValid(prevState.value)!==this.isValid(this.state.value) ||
                prevState.touchedFrom!==this.state.touchedFrom ||
                prevState.touchedTo!==this.state.touchedTo ||
                prevState.enabled!==this.state.enabled ||
                prevProps.mandatory!==this.props.mandatory);

            if(hasChanged){
                if(Ganymede.pageData.debug) console.log(this.name + " calling updateStatus");
                if(!!this.props.updateStatus) {
                    this.props.updateStatus(this.props.id, this.state.value , this.isValid(this.state.value), this.state.enabled || this.props.mandatory);
                }
                else {
                    console.error("PORTING ERROR: " + this.name + " is being invoked by a caller without prop updateStatus being provided");
                }
            }
        }


        componentWillReceiveProps(nextProps) {
            GanymedeComponent.prototype.componentWillReceiveProps.call(this,nextProps);
            if (!!nextProps.shouldUpdateValueOnNewDefaultValue && !!nextProps.default){
                $( this.refs.dateFromTo ).find('.fromDateContainer input').datepicker('setDate',this.neutralFormatToDate(nextProps.default[0].value));
                $( this.refs.dateFromTo ).find('.toDateContainer input').datepicker('setDate',this.neutralFormatToDate(nextProps.default[1].value));
                this.setState({value: nextProps.default});
            }
        }

        dateToNeutralFormat(date) {
            function pad(n) {
                return (n < 10) ? ("0" + n) : n;
            }
            let day = pad(date.getDate());
            let month = pad(date.getMonth()+1);
            let year = date.getYear()-100;
            return (day+'/'+month+'/'+year);
        }

        neutralFormatToDate(date) {
            try {
                let bits = date.split('/');
                if (bits.length != 3) return new Date();
                return new Date(parseInt(bits[2]) + 2000, parseInt(bits[1]) - 1, parseInt(bits[0]));
            }
            catch(err){
                console.error(err);
                return new Date();
            }
        }

        componentDidMount() {
            // FROM FROM FROM
            $( this.refs.dateFromTo ).find('.fromDateContainer input').datepicker({
                dateFormat: this.props.presentation.format || 'dd/mm/yy',
                changeMonth: true,
                numberOfMonths: 1,
                showOn: (this.props.readOnly ? '' : 'focus'),
                maxDate: this.neutralFormatToDate(this.state.value[1].value),
                onClose: function( selectedDate ) {
                    // Get the date as a date object
                    let date = $( this.refs.dateFromTo ).find('.fromDateContainer input').datepicker('getDate');
                    // Set the to min of to to us
                    $( this.refs.dateFromTo ).find('.toDateContainer input').datepicker( "option", "minDate", date );
                    // Now back to neutral format for storage
                    date = this.dateToNeutralFormat(date);
                    let newDate = $.extend(true,[],this.state.value);
                    newDate[0].value = date;
                    this.setState({value: newDate, touchedFrom: true});
                }.bind(this)
            });
            $( this.refs.dateFromTo ).find('.fromDateContainer input').datepicker('setDate',this.neutralFormatToDate(this.state.value[0].value));

            // TO TO TO
            $( this.refs.dateFromTo ).find('.toDateContainer input').datepicker({
                dateFormat: this.props.presentation.format || 'dd/mm/yy',
                changeMonth: true,
                numberOfMonths: 1,
                showOn: (this.props.readOnly ? '' : 'focus'),
                minDate: this.neutralFormatToDate(this.state.value[0].value),
                onClose: function( selectedDate ) {
                    // Get the date as a date object
                    let date = $( this.refs.dateFromTo ).find('.toDateContainer input').datepicker('getDate');
                    // Set the to max of from to us
                    $( this.refs.dateFromTo ).find('.fromDateContainer input').datepicker( "option", "maxDate", date );
                    // Now back to neutral format for storage
                    date = this.dateToNeutralFormat(date);
                    let newDate = $.extend(true,[],this.state.value);
                    newDate[1].value = date;
                    this.setState({value: newDate, touchedTo: true});
                }.bind(this)
            });
            // The super secret property that stop dates in the future for some usages
            if(!!this.props.pastOnly) $( this.refs.dateFromTo ).find('.toDateContainer input').datepicker("option", "maxDate", '+0');

            $( this.refs.dateFromTo ).find('.toDateContainer input').datepicker('setDate',this.neutralFormatToDate(this.state.value[1].value));
        }

        validationResult(data) {
            let errorMessage = '';
            // Check aginst our internal normalised date format
            let isValid = !!data && data.constructor===Array && moment(data[0].value,'DD/MM/YY').isValid() && moment(data[1].value,'DD/MM/YY').isValid();
            if(!isValid) errorMessage = 'Both dates are required';

            if(!this.state.newConstraints.allowUntouchedDates && (!this.state.touchedTo || !this.state.touchedFrom)) {
                errorMessage = 'Both dates are required to be set';
            }
            if(!this.state.newConstraints.allowStartEndSame && (this.state.value[0].value === this.state.value[1].value)) {
                errorMessage = 'Both dates must be different';
            }
            return errorMessage;
        }

        isValid(data) {
            return this.validationResult(data)==='';
        }


        render() {
            if(Ganymede.pageData.debug) console.log("DateFromTo Render("+this.props.id+") - ",this.state.value);

            let mandatory = !!this.props.mandatory;
            let presentation = this.props.presentation || {};
            let id = 'dateFromTo'+this.props.id;
            let displayClass = (!!presentation.hidden)?' hide':'';
            let name = this.props.name;

            let content = [];

            content.push(React.createElement(DataFromToRender, { key:'component', tabIndex: this.props.enabled?1:-1, caption: presentation.caption, data: this.state.value, readOnly:this.props.readOnly}));
            if (this.state.touched && !this.isValid(this.state.value)){
                content.push(React.createElement('div', {key:'error', className: 'error hasError '+id, ref:'errorField'},this.validationResult()));
            }

            let container;
            let metadataMessage = '';
            if (!!presentation.caption && presentation.caption.trim() === ''){
                container = React.createElement(SimpleContainer, {readOnly: this.props.readOnly, content: content, elementId: id, metadataMessage: metadataMessage});
            } else {
                if (mandatory){
                    container = React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, metadataMessage: metadataMessage});
                } else {
                    container = React.createElement(OptionalContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, onToggleEnable: this.onToggleEnable, metadataMessage: metadataMessage, enabled: this.state.enabled});
                }
            }


            if (this.props.simplified) {
                let container = React.createElement(DateFromToSimplified, {readOnly: this.props.readOnly, data: this.state.value, caption: presentation.caption});
                return React.createElement('div',{className: 'textBox date '+id+displayClass+' '+name + ' simplified'}, container);
            } else {
                return React.createElement('div', {className: 'textBox date '+id+displayClass+' '+name, ref:'dateFromTo', onClick:this.onTouch}, container);
            }
        }
    };

    class DateFromToSimplified extends React.Component {
        constructor(props) {
            super(props);

            this.state = {
                fromData: this.props.data[0],
                toData: this.props.data[1]
            };
        }

        render() {

            let startDate = React.createElement('div', {className: 'startDate'},
                React.createElement('div',{className: 'dateHeading'}, this.state.fromData.item),
                React.createElement('div', {className: 'dateValue'}, this.state.fromData.value)
            );
            let endDate = React.createElement('div', {className: 'toDate'},
                React.createElement('div',{className: 'dateHeading'}, this.state.toData.item),
                React.createElement('div', {className: 'dateValue'}, this.state.toData.value)
            );

            let content = React.createElement('div', {className: 'dateFromTo'}, startDate, endDate);
            return React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: this.props.caption, content: content, simplified: true});
        }
    }

    class DataFromToRender extends React.Component {
        constructor(props) {
            super(props);

            this.state = {
                fromData: this.props.data[0],
                toData: this.props.data[1]
            };
        }

        componentWillMount() {
            if (this.props.data.length != 2){
                console.error('the data is incorrect. expected 2 objects.', this.props.data);
            }
        }

        render() {

            var content = [];

            content.push(React.createElement(FromDate, {tabIndex: this.props.tabIndex, key:'from',caption: this.state.fromData.item , data: this.state.fromData, readOnly:this.props.readOnly}));
            content.push(React.createElement(ToDate, {tabIndex: this.props.tabIndex, key:'to',caption: this.state.toData.item , data: this.state.toData, readOnly:this.props.readOnly}));

            return React.createElement('div', {className: 'dateFromTo'}, content);
        }
    }

    class FromDate extends React.Component {
        render() {

            var content = React.createElement('div', {className: 'dateContainer'},
                React.createElement('div', {className: 'containerContent'},
                    React.createElement('div', {className: 'heading'}, this.props.caption),
                    React.createElement('div', {className: 'inputField'},
                        React.createElement('input', {tabIndex: this.props.tabIndex, ref:'inputField', readOnly:this.props.readOnly})
                    )
                )
            );

            return React.createElement('div', {className: 'fromDateContainer'}, content);
        }
    }

    class ToDate extends React.Component {
        render() {

            var content = React.createElement('div', {className: 'dateContainer'},
                React.createElement('div', {className: 'containerContent'},
                    React.createElement('div', {className: 'heading'}, this.props.caption),
                    React.createElement('div', {className: 'inputField'},
                        React.createElement('input', {tabIndex: this.props.tabIndex, ref:'inputField', readOnly:this.props.readOnly})
                    )
                )
            );

            return React.createElement('div', {className: 'toDateContainer'}, content);
        }
    }
} catch(e){
    console.error('an error was found:',e);
}
}());
