(function(){
try {
    window.DateField = class DateField extends GanymedeComponent {
        constructor(props) {
            super(props);
            this.isValid = this.isValid.bind(this);
            this.dateToNeutralFormat = this.dateToNeutralFormat.bind(this);
            this.neutralFormatToDate = this.neutralFormatToDate.bind(this);

            this.name = 'DateField';
            this.state.value = this.props.default || this.dateToNeutralFormat(new Date());
        }

        isValid(data) {
            let pattern =/^([0-9]{2})\/([0-9]{2})\/([0-9]{2})$/;
            return pattern.test(data);
        }

        dateToNeutralFormat(date) {
            function pad(n) {
                return (n < 10) ? ("0" + n) : n;
            }
            let day = pad(date.getDate());
            let month = pad(date.getMonth()+1);
            let year = date.getYear()-100;
            return (day+'/'+month+'/'+year);
        }

        neutralFormatToDate(date) {
            try {
                let bits = date.split('/');
                if (bits.length != 3) return new Date();
                return new Date(parseInt(bits[2]) + 2000, parseInt(bits[1]) - 1, parseInt(bits[0]));
            }
            catch(err){
                console.error(err);
                return new Date();
            }
        }

        componentDidMount() {
            if (this.props.simplified) return;

            $( this.refs.dateField ).find('.dateContainer input').datepicker({
                dateFormat: this.props.presentation.format || 'dd/mm/yy',
                changeMonth: true,
                numberOfMonths: 1,
                showOn: (this.props.readOnly ? '': 'focus'),
                onClose: function( selectedDate ) {
                    let date = $( this.refs.dateField ).find('.dateContainer input').datepicker('getDate');
                    date = this.dateToNeutralFormat(date);
                    this.setState({value: date});
                }.bind(this)
            });

            $( this.refs.dateField ).find('.dateContainer input').datepicker('setDate',this.neutralFormatToDate(this.state.value));

            (function(self){
                let date = $( self.refs.dateField ).find('.dateContainer input').datepicker('getDate');
                date = self.dateToNeutralFormat(date);
                self.setState({value: date});
            }(this));
        }

        render() {
            if(Ganymede.pageData.debug) console.log("DateField Render("+this.props.id+") - ",this.state.value);

            let mandatory = !!this.props.mandatory;
            let presentation = this.props.presentation || {};
            let id = 'dateField'+this.props.id;
            let displayClass = (!!presentation.hidden)?' hide':'';
            let name = this.props.name;

            let content = [];

            content.push(React.createElement(DateFieldRender, {key:'component', tabIndex: this.props.enabled?1:-1, caption: presentation.caption, data: this.neutralFormatToDate(this.state.value),readOnly:this.props.readOnly}));
            if (this.state.touched && this.isValid(this.state.value)){
                content.push(React.createElement('div', {key:'error', className: 'error hasError'+id, ref:'errorField'}));
            }

            let container;
            let metadataMessage = '';
            if (!!presentation.caption && presentation.caption.trim() === ''){
                container = React.createElement(SimpleContainer, {readOnly: this.props.readOnly, content: content, elementId: id, metadataMessage: metadataMessage});
            } else {
                if (mandatory){
                    container = React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, metadataMessage: metadataMessage});
                } else {
                    container = React.createElement(OptionalContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, onToggleEnable: this.onToggleEnable, metadataMessage: metadataMessage, enabled: this.state.enabled});
                }
            }
            if (this.props.simplified) {
                let container = React.createElement(DateFieldSimplified, {readOnly: this.props.readOnly, value: this.state.value, caption: presentation.caption});
                return React.createElement('div',{className: 'textBox date '+id+displayClass+' '+name + ' simplified'}, container);
            } else {
                return React.createElement('div', {className: 'textBox date '+id+displayClass+' '+name, ref:'dateField', onClick:this.onTouch}, container);
            }
        }
    };

    class DateFieldSimplified extends React.Component {
        render() {
            let content = React.createElement('div', {}, this.props.value);
            return React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: this.props.caption, content: content, simplified: true});
        }
    }

    class DateFieldRender extends React.Component {
        constructor(props) {
            super(props);

            this.state = {
                date: this.props.data
            };
        }

        render() {
            let content = React.createElement(DateInput, {tabIndex: this.props.tabIndex, caption: this.state.date.item , data: this.state.date, readOnly: this.props.readOnly });
            return React.createElement('div', {className: 'dateFromTo'}, content);
        }
    }

    class DateInput extends React.Component {
        render() {

            let defaultValue = this.props.data.value;

            let content = React.createElement('div', {className: 'dateContainer'},
                React.createElement('div', {className: 'containerContent', htmlFor: this.refs.inputField},
                    React.createElement('div', {className: 'heading'}, this.props.caption),
                    React.createElement('div', {className: 'inputField'},
                        React.createElement('input', {tabIndex: this.props.tabIndex, ref:'inputField', onChange: this.updateValue, disabled: this.props.readOnly})
                    )
                )
            );

            return React.createElement('div', {className: 'dateContainer'}, content);
        }
    }
}
catch(e){
    console.error('An error was found:',e);
}
}());
