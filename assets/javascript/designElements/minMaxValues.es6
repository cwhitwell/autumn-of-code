(function(){
    try {
        window.MinMaxValues = class MinMaxValues extends GanymedeComponent {
            constructor(props) {
                super(props);
                this.isValid = this.isValid.bind(this);
                this.updateValue = this.updateValue.bind(this);

                this.name = "MinMaxValues";
                this.state.enabled = this.props.enabled;
                this.state.value = (!!this.props.default ? $.extend(true, {}, this.props.default) : {data:[{},{}]} );
                this.state.errorMessage = '';
            }

            shouldComponentUpdate(nextProps,nextState) {
                if(GanymedeComponent.prototype.shouldComponentUpdate.call(this,nextProps,nextState)) return true;
                if(nextState.errorMessage!==this.state.errorMessage) return true;
                return false;
            }

            isValid() {
                return !this.state.errorMessage;
            }

            updateValue(update) {
                if(this.props.readOnly !== true) {
                    let newValue = $.extend(true,{},this.state.value);
                    let propertyToUpdate = Object.keys(update).join();
                    let minData = (newValue.data[0].comparators[0] === '>=') ? newValue.data[0] : newValue.data[1];
                    let maxData = (newValue.data[0].comparators[0] === '>=') ? newValue.data[1] : newValue.data[0];
                    let inputValue = update[propertyToUpdate];
                    let errorMessage = Ganymede.formValidator(inputValue, this.state.newConstraints);

                    if (!errorMessage){
                        if (propertyToUpdate === 'minValue'){
                            if (inputValue !== '' && !(parseFloat(update[propertyToUpdate]) <= parseFloat(maxData.value))){
                                errorMessage = 'Minimum value must be smaller than the maximum value';
                            }
                            minData.value = update[propertyToUpdate];

                        } else if (propertyToUpdate === 'maxValue'){
                            if (inputValue !== '' && !(parseFloat(update[propertyToUpdate]) >= parseFloat(minData.value))){
                                errorMessage = 'Maximum value must be bigger than the minimum value';
                            }
                            maxData.value = update[propertyToUpdate];
                        }
                    }
                    else {
                        if (propertyToUpdate === 'minValue'){
                            minData.value = update[propertyToUpdate];
                        } else if (propertyToUpdate === 'maxValue'){
                            maxData.value = update[propertyToUpdate];
                        }
                    }
                    this.setState({value: newValue, errorMessage:errorMessage});
                }
            }

            render() {
                if(Ganymede.pageData.debug) console.log("MinMaxValues Render("+this.props.id+") - ",this.state.value);

                let mandatory = !!this.props.mandatory;
                let presentation = this.props.presentation || {};
                let id = 'minMaxValues'+this.props.id;
                let displayClass = (!!presentation.hidden)?' hide':'';
                let name = this.props.name;

                let content = [];
                content.push(React.createElement(MinMaxValuesRender, {tabIndex: this.props.enabled?1:-1, key:'component', id: this.props.id, caption: presentation.caption, data: this.state.value.data, updateValue: this.updateValue, presentation: presentation, constraints:this.state.newConstraints, readOnly: this.props.readOnly}));
                if (this.state.touched && !!this.state.errorMessage){
                    content.push(React.createElement('div', {key:'error', className: 'error hasError '+id, ref:'errorField'},this.state.errorMessage));
                }

                let container;
                let metadataMessage='';
                if (!!presentation.caption && presentation.caption.trim() === ''){
                    container = React.createElement(SimpleContainer, {readOnly: this.props.readOnly, content: content, elementId: id, metadataMessage: metadataMessage});
                } else {
                    if (mandatory){
                        container = React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, metadataMessage: metadataMessage});
                    } else {
                        container = React.createElement(OptionalContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, onToggleEnable: this.onToggleEnable, metadataMessage: metadataMessage, enabled: this.state.enabled});
                    }
                }
                if (this.props.simplified) {
                    let container = React.createElement(MinMaxValuesSimplified, {readOnly: this.props.readOnly, data: this.state.value.data, caption: presentation.caption});
                    return React.createElement('div',{className: 'textBox minMaxValues '+id+displayClass+' '+name + ' simplified'}, container);
                } else {
                    return React.createElement('div', {className: 'textBox minMaxValues '+id+displayClass+' '+name, ref:'minMaxValues', onClick:this.onTouch}, container);
                }
            }
        };

        class MinMaxValuesSimplified extends React.Component {
            constructor(props) {
                super(props);
                this.state = {
                    minData: $.extend({},this.props.data[0]),
                    maxData: $.extend({},this.props.data[1])
                };
            }

            render() {
                let minData = React.createElement('div', {className: 'minData'},
                    React.createElement('div',{className: 'dataHeading'}, this.state.minData.item),
                    React.createElement('div', {className: 'dataValue'}, this.state.minData.value)
                );
                let maxData = React.createElement('div', {className: 'maxData'},
                    React.createElement('div',{className: 'dataHeading'}, this.state.maxData.item),
                    React.createElement('div', {className: 'dataValue'}, this.state.maxData.value)
                );

                let content = React.createElement('div', {className: 'minMaxSimplified'}, minData, maxData);
                return React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: this.props.caption, content: content, simplified: true});
            }
        }

        class MinMaxValuesRender extends React.Component {
            constructor(props) {
                super(props);
                this.valueToLabel = this.valueToLabel.bind(this);
                this.valueToPosition = this.valueToPosition.bind(this);
                this.sliderWidth = 400.0;

                let span = (this.props.constraints.maxValue || 1) - (this.props.constraints.minValue || 0);
                let step = span/this.sliderWidth;
                // let places = Math.max(0,3-Math.ceil(span).toString().length);
                let places = 2;
                if(step>0.01) places=1;
                if(step>0.1) places=0;

                this.state = {
                    minData: $.extend({},this.props.data[0]),
                    maxData: $.extend({},this.props.data[1]),
                    span: span,
                    base: this.props.constraints.minValue || 0,
                    places: places
                };
            }

            componentWillMount() {
                if (this.props.data.length != 2){
                    console.error('the data is incorrect. expected 2 objects.', this.props.data);
                }
            }

            valueToLabel(value) {
                return (this.state.base + ((value/this.sliderWidth)*this.state.span)).toFixed(this.state.places)
            }

            valueToPosition(value) {
                return Math.ceil(((value-this.state.base)/this.state.span)*this.sliderWidth);
            }

            componentDidMount() {

                let defaultValue = [this.valueToPosition(this.state.minData.value),this.valueToPosition(this.state.maxData.value)];
                let self = this;

                function updateTextLabels(event, ui) {
                    if(self.props.readOnly !== true){
                      let fromValue = self.valueToLabel(ui.values[0]);
                      let toValue = self.valueToLabel(ui.values[1]);

                      console.log("updateTextLabels - ",ui.values);

                      $($(self.refs.sliderInput).find('.label')[0]).html(fromValue);
                      $($(self.refs.sliderInput).find('.label')[1]).html(toValue);

                      // Only update when required, reduce the re-renders
                      if(self.state.minData.value != fromValue) {
                        self.props.updateValue({minValue: fromValue});
                        self.state.minData.value = fromValue;
                      }

                      if(self.state.maxData.value != toValue) {
                        self.props.updateValue({maxValue: toValue});
                        self.state.maxData.value = toValue;
                      }
                    }else{
                      event.preventDefault();
                    }

                }

                $(this.refs.sliderInput).slider({
                    range: true,
                    min: 0,
                    max: Math.ceil(this.sliderWidth),
                    values: defaultValue,
                    slide: updateTextLabels,
                    create: function(){
                        let handles = $(this.refs.sliderInput).find('.ui-slider-handle');

                        for (let x = 0; x < handles.length; x++){
                            let handleValue = this.valueToLabel(defaultValue[x]);
                            if(x%2 == 0){
                                $(handles[x]).append('<div class="label label-top">'+handleValue+'</div>');
                            }
                            else {
                                $(handles[x]).append('<div class="label label-bottom">'+handleValue+'</div>');
                            }
                        }
                        $('.ui-slider-handle').attr('tabindex', (this.state.enabled?1:-1)+'');
                    }.bind(this)
                });
            }

            render() {

                let content = [];

                if (!!this.props.presentation.display && this.props.presentation.display==='slider'){
                    content.push(React.createElement('div', {key:'component', className: 'sliderHolder '+this.props.id },
                        React.createElement('div', {className: 'endLabel '+this.props.id},this.props.constraints.minValue.toFixed(this.state.places)),
                        React.createElement('div', {className: 'sliderInput '+this.props.id, ref:'sliderInput'}),
                        React.createElement('div', {className: 'endLabel '+this.props.id},this.props.constraints.maxValue.toFixed(this.state.places))
                    ));
                }
                else {
                    content.push(React.createElement(MinValue, {tabIndex: this.props.tabIndex, key:'min',caption: this.state.minData.item , data: this.state.minData, updateValue: this.props.updateValue, readOnly: this.props.readOnly}));
                    content.push(React.createElement(MaxValue, {tabIndex: this.props.tabIndex, key:'max',caption: this.state.maxData.item , data: this.state.maxData, updateValue: this.props.updateValue, readOnly: this.props.readOnly}));
                }

                return React.createElement('div', {className: 'minMaxValuesRender'}, content);
            }
        }

        class MinValue extends React.Component {
            constructor() {
                super();
                this.updateValue = this.updateValue.bind(this);
            }

            updateValue() {
                this.props.updateValue({minValue: $(this.refs.inputField).val()});
            }

            render() {

                let content = React.createElement('div', {className: 'valueContainer'},
                    React.createElement('div', {className: 'containerContent'},
                        React.createElement('div', {className: 'heading'}, this.props.caption),
                        React.createElement('div', {className: 'inputField'},
                            React.createElement('input', {tabIndex: this.props.tabIndex, ref:'inputField', defaultValue: this.props.data.value, onChange: this.updateValue, readOnly: this.props.readOnly})
                        )
                    )
                );

                return React.createElement('div', {className: 'minValue'}, content);
            }
        }

        class MaxValue extends React.Component {
            constructor() {
                super();
                this.updateValue = this.updateValue.bind(this);
            }

            updateValue() {
                this.props.updateValue({maxValue: $(this.refs.inputField).val()});
            }

            render() {

                let content = React.createElement('div', {className: 'valueContainer'},
                    React.createElement('div', {className: 'containerContent'},
                        React.createElement('div', {className: 'heading'}, this.props.caption),
                        React.createElement('div', {className: 'inputField'},
                            React.createElement('input', {tabIndex: this.props.tabIndex, ref:'inputField', defaultValue: this.props.data.value, onChange: this.updateValue, readOnly: this.props.readOnly})
                        )
                    )
                );

                return React.createElement('div', {className: 'manValue'}, content);
            }
        }
    } catch(e) {
        console.error('an error was found:',e);
    }
}());
