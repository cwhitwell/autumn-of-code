var TickBox = class TickBox extends GanymedeComponent {
    constructor(props) {
        super(props);

        this.isValid = this.isValid.bind(this);
        this.updateValue = this.updateValue.bind(this);

        this.name = "TickBox";
        this.state.value = this.props.default || false;
    }

    isValid(data) {
        return true;
    }

    updateValue() {
        if(!this.props.readOnly) this.setState({value: !this.state.value});
    }

    render() {
        if(Ganymede.pageData.debug) console.log("TickBox Render("+this.props.id+") - ",this.state.value);

        var mandatory = !!this.props.mandatory;
        var presentation = this.props.presentation || {};
        var id = 'tickBox'+this.props.id;
        var displayClass = (!!presentation.hidden)?' hide':'';
        var name = this.props.name;

        var key = 0;

        var content = [];

        presentation.display = presentation.display || 'tickbox';

        if (presentation.display === 'tickbox'){

            var tickboxOptions = {
                item: presentation.caption,
                value: this.state.value,
                default: this.state.value
            };

            content.push(React.createElement(TickBoxItem, {tabIndex: this.props.enabled?1:-1, data: tickboxOptions, section: this.props.section, id: this.props.id, key: key++, updateValue: this.updateValue, readOnly: this.props.readOnly}));
        } else if (presentation.display === 'switch' || presentation.display === 'switch_small'){

            var switchOptions = {
                default: this.state.value,
                captionOn: presentation.caption_on,
                captionOff: presentation.caption_off,
                section: this.props.section,
                id: this.props.id,
                tabIndex: this.state.enabled?1:-1,
                updateValue: this.updateValue,
                renderSmall: (presentation.display === 'switch_small'),
                key: key++,
                readOnly: this.props.readOnly
            };

            content.push(React.createElement(TickBoxSwitch, switchOptions));
        }

        // if (this.state.touched){
        //     content.push(React.createElement('div', {key: key++, className: 'error '+id, ref:'errorField'}));
        // }

        var container;
        let metadataMessage='';
        if (!!presentation.caption && presentation.caption.trim() === ''){
            container = React.createElement(SimpleContainer, {readOnly: this.props.readOnly, content: content, elementId: id, metadataMessage: metadataMessage, simplified: this.props.simplified});
        } else {
            if (mandatory){
                container = React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, metadataMessage: metadataMessage, simplified: this.props.simplified});
            } else {
                container = React.createElement(OptionalContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, onToggleEnable: this.onToggleEnable, metadataMessage: metadataMessage, enabled: this.state.enabled, simplified: this.props.simplified});
            }
        }
        if (this.props.simplified) {
            let container = React.createElement(TickBoxSimplified, {readOnly: this.props.readOnly, value: this.state.value, caption: presentation.caption});
            return React.createElement('div',{className: 'tickBox '+id+displayClass+' '+name + ' simplified'}, container);
        } else {
            return React.createElement('div', {className: 'tickBox '+id+displayClass+' '+name, onClick:this.onTouch}, container);
        }
    }
};

class TickBoxSimplified extends React.Component {
    render() {
        let content = React.createElement('div', {className: 'tickBoxCapitalize'}, this.props.value.toString());
        return React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: this.props.caption, content: content, simplified: true});
    }
}

var TickBoxSwitch = class TickBoxSwitch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {identifier: this.props.section+'-'+this.props.id+Math.floor(Math.random()*Math.random()*1000)};
    }

    render() {

        var sizeClass = !!this.props.renderSmall?' small':'';

        var defaultValue = this.props.default || false;

        var switchControl = [];
        switchControl.push(React.createElement('input', {tabIndex: this.props.tabIndex, key:'input', type: 'checkbox', id: this.state.identifier, defaultChecked: defaultValue, 'disabled': this.props.readOnly}));

        switchControl.push(React.createElement('label', {key:'label', htmlFor: this.state.identifier, onClick: this.props.updateValue},
            React.createElement('div', {className: 'switchControl ' + (this.props.readOnly ? '' : 'clickable ') + sizeClass},
                React.createElement('div', {className: 'slider'+sizeClass}),
                React.createElement('div', {className: 'sliderCaptions'+sizeClass},
                    React.createElement('div', {className: 'captionTrue'}, this.props.captionOn),
                    React.createElement('div', {className: 'captionFalse'}, this.props.captionOff)
                )
            )
        ));

        return React.createElement('div', {className: 'tickBox tickBoxSwitch noSelect'}, switchControl);
    }
};

class TickBoxItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {identifier: 'SelectSinglePopOutList'+this.props.id+Math.floor(Math.random()*Math.random()*1000)};
    }

    render() {

        var defaultValue = this.props.data.default || false;
        var x = 0;

        var getKey = function(){
            return this.state.identifier+(x++);
        }.bind(this);

        var tickBox = [];
        tickBox.push(React.createElement('input', {key: getKey(), tabIndex: this.props.tabIndex, type: 'checkbox', id: this.state.identifier, defaultChecked: defaultValue, disabled: this.props.readOnly}));
        tickBox.push(React.createElement('label', {key: getKey(), className: 'checkBox', htmlFor: this.state.identifier, onClick: this.props.updateValue},
            React.createElement('img', {src: 'assets/images/core/icon_tick.png'})
        ));
        tickBox.push(React.createElement('label', {key: getKey(), className: 'checkBox-name', htmlFor: this.state.identifier, onClick: this.props.updateValue},
            React.createElement('div', null, this.props.data.item)
        ));

        return React.createElement('div', {className: 'tickBoxContainer'}, tickBox);
    }
}
