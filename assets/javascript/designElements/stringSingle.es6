(function(){
    try {
        window.StringSingle = class StringSingle extends GanymedeComponent {
            constructor(props) {
                super(props);
                this.onBlur = this.onBlur.bind(this);
                this.onChange = this.onChange.bind(this);
                this.validationResult = this.validationResult.bind(this);
                this.isValid = this.isValid.bind(this);

                // Update/add our state vars
                this.name = "StringSingle";
                this.state.value = (this.props.default || '');
            }

            onChange() {
                let inputValue = this.refs.inputField.value;
                this.setState({touched:true, value: inputValue});
            }

            onBlur() {
                if(!this.state.touched) this.setState({touched: true});
            }

            validationResult(data) {
                return Ganymede.formValidator(data, this.state.newConstraints);
            }

            isValid(data) {
                return !this.validationResult(data);
            }

            render() {
                if(Ganymede.pageData.debug) console.log("StringSingle Render("+this.props.id+") - ",this.state.value);

                let mandatory = !!this.props.mandatory;
                let presentation = this.props.presentation || {};
                let id = 'stringSingle'+this.props.id;
                let displayClass = (!!presentation.hidden)?' hide':'';
                let name = this.props.name;
                let defaultValue = this.props.default || '';
                let validation = this.validationResult(this.state.value);

                let content = [];

                content.push(React.createElement('input', {
                    key:'component',
                    tabIndex: this.props.enabled?1:-1,
                    type:(this.props.type === 'password') ? 'password':'text',
                    className: id, readOnly:this.props.readOnly,
                    placeholder: presentation.placeholder,
                    defaultValue: defaultValue,
                    ref:'inputField',
                    onBlur: this.onBlur,
                    onChange: this.onChange
                }));

                if (this.state.touched && !!validation){
                    content.push(React.createElement('div', {key:'error', className: 'error hasError '+id, ref:'errorField'},validation));
                }

                let container;
                let metadataMessage = '';
                // Don't print metadata for the max length case, no point
                if(this.state.newConstraints.maxLength!==Ganymede.formConstraints.maxLength && this.state.touched) metadataMessage = this.state.value.length + ' out of ' + this.state.newConstraints.maxLength;

                if (!!presentation.caption && presentation.caption.trim() === ''){
                    container = React.createElement(SimpleContainer, {readOnly: this.props.readOnly, content: content, elementId: id, metadataMessage: metadataMessage});
                }
                else {
                    if (mandatory){
                        container = React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, metadataMessage: metadataMessage});
                    }
                    else {
                        container = React.createElement(OptionalContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, onToggleEnable: this.onToggleEnable, metadataMessage: metadataMessage, enabled: this.state.enabled});
                    }
                }

                if (this.props.simplified) {
                    let container = React.createElement(StringSingleSimplified, {readOnly: this.props.readOnly, value: defaultValue, caption: presentation.caption});
                    return React.createElement('div',{className: 'textBox stringSingle '+id+displayClass+' '+name + ' simplified'}, container);
                } else {
                    return React.createElement('div', {className: 'textBox stringSingle '+id+displayClass+' '+name, onClick:this.onTouch }, container);
                }

            }
        };

        class StringSingleSimplified extends React.Component {

            render() {
                let content = React.createElement('div', {}, this.props.value);
                return React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: this.props.caption, content: content, simplified: true});
            }
        }


    } catch(e){
        console.error('an error was found:',e);
    }

}());
