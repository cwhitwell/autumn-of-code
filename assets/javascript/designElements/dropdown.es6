class DropDown extends React.Component {
    render() {
        var containerProps = $.extend({}, this.props);
        containerProps.preSelectedOptionIndex = containerProps.preSelectedOptionIndex || 0;
        var dropDownContainer = React.createElement(DropDownContainer, containerProps);
        return React.createElement('div', {className: 'dropDown noSelect '+((!!this.props.size)?this.props.size:'')}, dropDownContainer);
    }
}

class DropDownContainer extends React.Component {
    render() {
        if(!this.props.title || this.props.title==='') {
            var dropDownMenu = React.createElement(DropDownMenu, {preSelectedOptionIndex: this.props.preSelectedOptionIndex, options: this.props.options});
            return React.createElement('div', {className: 'dropDownContainer'}, dropDownMenu);
        }
        else {
            var dropDownTitle = React.createElement(DropDownTitle, {title: this.props.title});
            var dropDownMenu = React.createElement(DropDownMenu, {preSelectedOptionIndex: this.props.preSelectedOptionIndex, options: this.props.options});
            return React.createElement('div', {className: 'dropDownContainer'}, dropDownTitle, dropDownMenu);
        }
    }
}

class DropDownTitle extends React.Component {
    render() {
        return React.createElement('div', {className: 'dropDownTitle'}, this.props.title);
    }
}

class DropDownMenu extends React.Component {
    constructor() {
        super();
        this.togglePopup = this.togglePopup.bind(this);
        this.setSelected = this.setSelected.bind(this);
        this.state = { hidden: true, selected:0 };
    }

    componentWillMount() {
        this.setState({ selected: this.props.preSelectedOptionIndex})
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ selected: nextProps.preSelectedOptionIndex})
    }

    togglePopup() {
        this.setState({ hidden: !this.state.hidden });
    }

    setSelected(selected) {
        this.setState({ selected: selected, hidden: true });
    }

    render() {
        var selectedOptionText = this.props.options[this.state.selected].title;
        var selectedOption = React.createElement('div', {className: 'selectedOption clickable'+(this.state.hidden?'':' selected'), onClick: this.togglePopup}, selectedOptionText);

        var popOutMenu = React.createElement(PopOutMenu, {options: this.props.options, currentSelectedItem: this.state.selected, selectCallback:this.setSelected});
        var popoutBackdrop = React.createElement('div', {className: 'popoutBackdrop', onClick: this.togglePopup});
        var popOutHolder = React.createElement('div',{className: 'popOutMenu'+(this.state.hidden?' hide':'')}, popoutBackdrop, popOutMenu);

        return React.createElement('div', {className: 'dropDownMenu'}, selectedOption, popOutHolder);
    }
}

class PopOutMenu extends React.Component {
    render() {

        var popOutMenuHeading = listRenderer['heading'](this.props.options[this.props.currentSelectedItem].renderData);
        var popOutMenuContainer = React.createElement(PopOutMenuContainer, {options: this.props.options, selectCallback: this.props.selectCallback});

        return React.createElement('div', {className: 'popOutMenuContent'}, popOutMenuHeading, popOutMenuContainer);
    }
}

class PopOutMenuContainer extends React.Component {
    render() {
        // The items here are dynamic and may have changed between renders so make the key
        // random ish so React thinks they are new and doesn't attempt to reuse them.

        var optionItemsArray = [];
        for (var x = 0; x < this.props.options.length; x++){
            if(this.props.options[x].renderTemplate !== 'hidden')
            {
                optionItemsArray.push(React.createElement(OptionItem, $.extend({
                    key: Math.random(),
                    selectCallback: this.props.selectCallback,
                    selectIndex: x
                }, this.props.options[x])));
            }
        }

        return React.createElement('ul', {className: 'popOutMenuContainer'}, optionItemsArray);
    }
}

class OptionItem extends React.Component {
    constructor() {
        super();
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        if(this.props.selectCallback) this.props.selectCallback(this.props.selectIndex);
        if(this.props.action) this.props.action();
    }

    render() {

        if (!listRenderer.hasOwnProperty(this.props.renderTemplate)){
            console.error('invalid render template for dropdown option:', this.props);
        }

        var itemContent = listRenderer[this.props.renderTemplate](this.props.renderData);

        return React.createElement('li', {className: 'optionItem clickable', onClick: this.handleClick}, itemContent);
    }
}

var listRenderer = {
    //hidden: function(data){
    //    return null;
    //},
    simple: function(data){
        return React.createElement('span', {className: 'simpleItem'}, data.title);
    },
    heading: function(data){
        return React.createElement('div', {className: 'headingItem'}, data.title);
    }
}
