(function(){
    try {
        window.Decimal = class Decimal extends GanymedeComponent {
            constructor(props) {
                super(props);
                this.isValid = this.isValid.bind(this);
                this.onChange = this.onChange.bind(this);
                this.onBlur = this.onBlur.bind(this);
                this.onInput = this.onInput.bind(this);

                this.name = "Decimal";
                this.state.value = (this.props.default || 0);
                this.state.newConstraints.isFloat=true;
            }

            isValid(data) {
                return !Ganymede.formValidator(data, this.state.newConstraints);
            }

            onChange() {
                let value = this.refs.inputField.value;
                let validation = Ganymede.formValidator(value, this.state.newConstraints);
                let inputValue = (!validation) ? parseFloat(value) : value;
                if(inputValue!==this.state.value || !this.state.touched) {
                    this.setState({touched: true, value: inputValue});
                    // If we have a slider we need to set it.
                    if(!validation && !!this.props.presentation.display && this.props.presentation.display==='slider'){
                        $(this.refs.sliderInput).slider('value', value);
                    }
                }
            }

            onInput() {
                if(!this.state.touched) this.setState({touched: true});
            }

            onBlur() {
                if(!this.state.touched) this.setState({touched: true});
            }

            componentDidMount() {
                if (!!this.props.presentation.display && this.props.presentation.display==='slider') {

                    let id = 'decimal'+this.props.id;
                    let defaultValue = this.props.default;
                    let minValue = this.state.newConstraints.minValue;
                    let maxValue = this.state.newConstraints.maxValue;
                    let self = this;

                    function updateTextField(e){
                        if(!self.props.readOnly){
                            let inputValue = $( self.refs.sliderInput ).slider('value');
                            inputValue = Math.max(inputValue,minValue);
                            inputValue = Math.min(inputValue,maxValue);

                            // Only set IF changed and use the state lets
                            if(inputValue!==self.state.value) {
                                // Overwrite the display value cheaty stlye
                                self.refs.inputField.value = inputValue;
                                // Set the state
                                self.setState({value:inputValue});
                            }
                        }else{
                            e.preventDefault();
                        }
                    }

                    $(function () {
                        let enabled = this.props.enabled;
                        $(this.refs.sliderInput).slider({
                            range: "min",
                            min: minValue,
                            max: maxValue,
                            value: defaultValue,
                            slide: updateTextField,
                            change: updateTextField,
                            create: function(){
                                $('.ui-slider-handle').attr('tabindex', (enabled?1:-1)+'');
                            }
                        });
                    }.bind(this));
                }
            }

            render() {
                if(Ganymede.pageData.debug) console.log("Decimal Render("+this.props.id+") - ",this.state.value);

                let mandatory = !!this.props.mandatory;
                let presentation = this.props.presentation || {};
                let id = 'decimal'+this.props.id;
                let displayClass = (!!presentation.hidden)?' hide':'';
                let name = this.props.name;

                let content = [];

                content.push(React.createElement('div', {key:'component', className: 'textInput'},
                    React.createElement('div', {className: 'heading'},presentation.heading),
                    React.createElement('div', {className: 'inputField'},React.createElement('input', {
                        key:'component',
                        tabIndex: this.props.enabled?1:-1,
                        className: id,
                        ref:'inputField',
                        readOnly:this.props.readOnly,
                        placeholder: presentation.placeholder,
                        defaultValue: this.state.value,
                        // onBlur: this.blur,
                        onInput: this.onInput,
                        onBlur: this.onBlur,
                        onChange: this.onChange}))
                ));

                if (!!presentation.display && presentation.display==='slider'){
                    content.push(React.createElement('div', {key:'slider', className: 'sliderInput '+id, ref:'sliderInput'}));
                }

                if (!this.isValid(this.state.value) && this.state.touched){
                    let validationStatus = Ganymede.formValidator(this.state.value, this.state.newConstraints);
                    content.push(React.createElement('div', {key:'error', className: 'error hasError '+id, ref:'errorField'},validationStatus));
                }

                let container;
                let metadataMessage='';
                if (!!presentation.caption && presentation.caption.trim() === ''){
                    container = React.createElement(SimpleContainer, {readOnly: this.props.readOnly, content: content, elementId: id, metadataMessage: metadataMessage});
                } else {
                    if (mandatory){
                        container = React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, metadataMessage: metadataMessage});
                    } else {
                        container = React.createElement(OptionalContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, onToggleEnable: this.onToggleEnable, metadataMessage: metadataMessage, enabled: this.state.enabled});
                    }
                }

                if (this.props.simplified) {
                    let container = React.createElement(DecimalSimplified, {readOnly: this.props.readOnly, value: this.state.value, caption: presentation.caption});
                    return React.createElement('div',{className: 'decimal textBox '+id+displayClass+' '+name + ' simplified'}, container);
                } else {
                    return React.createElement('div', {className: 'textBox decimal '+id+displayClass+' '+name, onClick: this.onTouch }, container);
                }
            }
        };

        class DecimalSimplified extends React.Component {
            render() {
                let content = React.createElement('div', {}, this.props.value);
                return React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: this.props.caption, content: content, simplified: true});
            }
        }
    } catch(e){
        console.error('an error was found:',e);
    }
}());

