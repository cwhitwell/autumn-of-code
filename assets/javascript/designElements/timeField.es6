(function(){
    try {
        window.TimeField = class TimeField extends GanymedeComponent {
            constructor(props) {
                super(props);
                this.isValid = this.isValid.bind(this);

                this.name = 'TimeField';
                this.state.value = $.extend(true, [], this.props.default);
            }

            componentDidMount() {

                let defaultValue = [];
                let self = this;

                for (let x = 0; x < this.state.value.length; x++){

                    let timeComponents = this.state.value[x].value.split(':');
                    let timeValue = (parseInt(timeComponents[0])*12)+(parseInt(timeComponents[1])/5);

                    defaultValue.push(timeValue);
                }

                function updateTextLabels(event, ui){
                    if(self.props.readOnly !== true){
                      let fromTimeValue = ((Math.floor(ui.value/12) < 10)?'0':'')+Math.floor(ui.value/12)+':'+((Math.floor(ui.value%12)*5 < 10)?'0':'')+(Math.floor(ui.value%12)*5);

                      $($(self.refs.sliderInput).find('.label')[0]).html(fromTimeValue);

                      let newValue = $.extend(true,[],self.state.value);
                      newValue[0].value = fromTimeValue;
                      self.setState({value: newValue});
                    }
                    else{
                        event.preventDefault();
                    }
                }

                $(function () {
                    $(this.refs.sliderInput).slider({
                        value: defaultValue[0],
                        min: 0,
                        max: 288,
                        slide: updateTextLabels,
                        create: function(){
                            let handles = $(self.refs.sliderInput).find('.ui-slider-handle');

                            for (let x = 0; x < handles.length; x++){
                                let timeValue = ((Math.floor(defaultValue[x]/12) < 10)?'0':'')+Math.floor(defaultValue[x]/12)+':'+((Math.floor(defaultValue[x]%12)*5 < 10)?'0':'')+(Math.floor(defaultValue[x]%12)*5);
                                if(x%2 == 0){
                                    $(handles[x]).append('<div class="label label-top">'+timeValue+'</div>');
                                }
                                else {
                                    $(handles[x]).append('<div class="label label-bottom">'+timeValue+'</div>');
                                }
                            }
                            $('.ui-slider-handle').attr('tabindex', (self.state.enabled?1:-1)+'');
                        }
                    });
                }.bind(this));
            }

            isValid(data) {
                return true;
            }

            render() {
                if(Ganymede.pageData.debug) console.log("TimeField Render("+this.props.id+") - ",this.state.value);

                let mandatory = !!this.props.mandatory;
                let presentation = this.props.presentation || {};
                let id = 'timeField'+this.props.id;
                let displayClass = (!!presentation.hidden)?' hide':'';
                let name = this.props.name;
                let content = [];
                content.push(React.createElement('div', {key:'component', className: 'sliderInput '+id, ref:'sliderInput'}));

                // No error state for this component
                // if (this.state.touched && this.isValid(this.state.value)){
                //     content.push(React.createElement('div', {key:'error', className: 'error '+id, ref:'errorField'}));
                // }

                let container;
                let metadataMessage = '';
                if (!!presentation.caption && presentation.caption.trim() === ''){
                    container = React.createElement(SimpleContainer, {readOnly: this.props.readOnly, content: content, elementId: id, metadataMessage: metadataMessage});
                } else {
                    if (mandatory){
                        container = React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, metadataMessage: metadataMessage});
                    } else {
                        container = React.createElement(OptionalContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, onToggleEnable: this.onToggleEnable, metadataMessage: metadataMessage, enabled: this.state.enabled});
                    }
                }
                if (this.props.simplified) {
                    let container = React.createElement(TimeFieldSimplified, {readOnly: this.props.readOnly, value: this.state.value[0].value, caption: presentation.caption});
                    return React.createElement('div',{className: 'textBox timeField timeFromTo '+id+displayClass+' '+name + ' simplified'}, container);
                } else {
                    return React.createElement('div', {className: 'textBox timeField timeFromTo '+id+displayClass+' '+name, ref:'timeField'}, container);
                }
            }
        };

        class TimeFieldSimplified extends React.Component {

            render() {
                let content = React.createElement('div', {className: 'timeFieldSimplified'}, this.props.value);
                return React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: this.props.caption, content: content, simplified: true});
            }
        }
} catch(e){
    console.error('an error was found:',e);
}
}());

