// (function(){

    // try {
    //     var GanymedeComponent = class GanymedeComponent extends React.Component {
        class GanymedeComponent extends React.Component {
            constructor(props) {
                super(props);
                this.onToggleEnable = this.onToggleEnable.bind(this);
                this.onTouch = this.onTouch.bind(this);
                this.isValid = this.isValid.bind(this);
                this.name = "GanymedeComponent";

                this.state = {
                    enabled: this.props.enabled,
                    value: (this.props.default || null), // User components should override
                    newConstraints: $.extend({},Ganymede.formConstraints,this.props.constraints),
                    touched: false
                };
            }

            isGanymedeComponent() { return true; }

            //
            // On mounting we call back as this will contain our value and validation status at load time please make
            // sure to only use this callback for consistency...
            //
            componentWillMount() {
                if(!!this.props.updateStatus) {
                    this.props.updateStatus(this.props.id, this.state.value , this.isValid(this.state.value), this.state.enabled || this.props.mandatory);
                }
                else {
                    console.error("PORTING ERROR: " + this.name + " is being invoked by a caller without prop updateStatus being provided");
                }
            }

            //
            // All well behaved components will handle this, in paritular it is IMPORTANT you at least handle props
            // enable here as if you are in a UI Group component you will have your enabled prop change....
            //
            componentWillReceiveProps(nextProps) {
                if(Ganymede.pageData.debug) console.log(this.name + " componentWillReceiveProps("+this.props.id+")",nextProps.enabled,this.state.enabled,this.props.enabled);
                if(nextProps.enabled !== this.state.enabled) {
                    // if(Ganymede.pageData.debug) console.log(this.name + " componentWillReceiveProps("+this.props.id+") - State trigger",nextProps.enabled,this.state.enabled,this.props.enabled);
                    this.setState({ enabled:nextProps.enabled} );
                }
            }

            //
            // The standard pattern is to call update status AFTER your component has completed its update and state
            // values have settled please try and keep to this pattern. Do not call from other places when updating and
            // you should check if things have really changed before calling back.
            //
            // The decision to render is different than the decision to updateStatus although for most cases they
            // will be identical, however sometimes your parent will ask you to render when its changed but you
            // really don't need to redraw
            //
            shouldComponentUpdate(nextProps,nextState) {
                let hasChanged = (!deepEqual(this.state.value,nextState.value) ||
                nextState.touched!==this.state.touched ||
                nextState.enabled!==this.state.enabled ||
                nextProps.mandatory!==this.props.mandatory);

                if(Ganymede.pageData.debug) console.log(this.name + " shouldComponentUpdate("+this.props.id+") - ",hasChanged);
                return hasChanged;
            }

            //
            // The standard pattern is to call update status AFTER your component has completed its update and state
            // values have settled please try and keep to this pattern. Do not call from other places when updating and
            // you should check if things have really changed before calling back.
            //
            // If you do it this way then you should not need to intercept shouldComponentUpdate for most things
            //
            // If you are not using shouldComponentUpdate then perhaps you should be carefully thinking about
            // if you should really be calling this, really should only be if something has changed....
            //
            componentDidUpdate(prevProps, prevState) {
                // If any params have changed lets call back....
                let hasChanged = (!deepEqual(this.state.value,prevState.value) ||
                    this.isValid(prevState.value)!==this.isValid(this.state.value) ||
                    prevState.enabled!==this.state.enabled ||
                    prevProps.mandatory!==this.props.mandatory);

                if(hasChanged){
                    if(Ganymede.pageData.debug) console.log(this.name + " calling updateStatus");
                    if(!!this.props.updateStatus) {
                        this.props.updateStatus(this.props.id, this.state.value , this.isValid(this.state.value), this.state.enabled || this.props.mandatory);
                    }
                    else {
                        console.error("PORTING ERROR: " + this.name + " is being invoked by a caller without prop updateStatus being provided");
                    }
                }
            }

            //
            // All components should do this as a standard, should be no
            // need to deviate or modify
                  //
            onToggleEnable() {
                this.setState({enabled: !this.state.enabled});
            }

            //
            // All components should do this as a standard, should be no need to deviate or modify, errors should only
            // display AFTER touched is true
            //
            onTouch() {
                // On touch we enable but don't validate
                if(!this.state.touched) this.setState({touched: true});
            }

            //
            // Simple true/false validator for your component, if no error then it must be good.
            //
            isValid(data) {
                console.error("GanymedeComponent - Base class isValid(data) has not been overridden - YOU MUST IMPLEMENT THIS METHOD",this.name);
                return false;
            }

            //
            // Finally the MEAT of your component.
            //
            render() {
                console.error("GanymedeComponent - Base class render has not been overridden - ",this.name);

                let mandatory = !!this.props.mandatory;
                let presentation = this.props.presentation || {};
                let id = 'stringSingle'+this.props.id;
                let displayClass = (!!presentation.hidden)?' hide':'';
                let name = this.props.name;

                let content = [];

                content.push(React.createElement('div', { key:'component', className:'GanymedeComponent'}, this.name + ' - Render not implemented GanymedeComponent base class render is exposed'));

                if (this.state.touched){
                    content.push(React.createElement('div', {key:'error', className: 'error hasError '+id, ref:'errorField'},''));
                }

                let container;
                let metadataMessage = '';

                if (!!presentation.caption && presentation.caption.trim() === ''){
                    container = React.createElement(SimpleContainer, {readOnly: this.props.readOnly, content: content, elementId: id, metadataMessage: metadataMessage});
                }
                else {
                    if (mandatory){
                        container = React.createElement(MandatoryContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, metadataMessage: metadataMessage});
                    }
                    else {
                        container = React.createElement(OptionalContainer, {readOnly: this.props.readOnly, caption: presentation.caption, content: content, elementId: id, onToggleEnable: this.onToggleEnable, metadataMessage: metadataMessage, enabled: this.state.enabled});
                    }
                }
                return React.createElement('div', {className: 'ganymedeComponent stringSingle '+id+displayClass+' '+name, onClick:this.onTouch }, container);
            }
        }
    // } catch(e){
    //     console.error('an error was found:',e);
    // }

// }());
