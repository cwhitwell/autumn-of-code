var DashboardAddScreen = class DashboardAddScreen extends React.Component {
  constructor(props){
    super(props);

    this.updateList = this.updateList.bind(this);

    this.state = {displayedWidgets: props.widgetList};
  }

  updateList(displayedWidgets){
    if(displayedWidgets && displayedWidgets.length > 0) this.setState({displayedWidgets, nothing: false});
    else this.setState({displayedWidgets: [], nothing: true});
  }

  render(){
    let reactComponents = [],
      key = 0;

    reactComponents.push(React.createElement(WidgetSearchBar, {widgets: this.props.widgetList, updateList: this.updateList, key: 'searchbar'}));

    if(this.state.nothing) reactComponents.push(React.createElement(WarningMessage, {heading: 'Filter', message: 'No widgets were found', key: 'warning-message'}));
    else {
      let widgetContainer = [];
      this.state.displayedWidgets.forEach(widget => {
        let widgetComponents = [];

        widgetComponents.push(React.createElement('div', {className: 'dashboard-add-screen-widget-img-container', key: key++},
          React.createElement(WidgetIcon, {icon: widget.icon, key: 'icon'})));

        widgetComponents.push(React.createElement('div', {className: 'dashboard-add-screen-widget-description', key: key++}, widget.description));

        widgetContainer.push(React.createElement('div', {className: 'dashboard-add-screen-widget', onClick: () => this.props.addWidget(widget.name, widget.props), key: key++}, widgetComponents));
      });
      reactComponents.push(React.createElement('div', {className: 'dashboard-add-screen-widget-container', key: 'container'},
        React.createElement('div', {className: 'dashboard-add-screen-widgets', key: 'widgets'}, widgetContainer)));
    }

    let content = React.createElement('div', {className: 'dashboard-add-screen', key: 'content'}, reactComponents);
    let popout = React.createElement(PopOutContainer,{content: content, dismissable: true, additionalClassName: 'widget-add-screen-popout', closePopup: this.props.toggleAddMode, key: 'popout'});

    return React.createElement('div', {className: `dashboard-add-screen-popout ${this.props.transparent ? 'dashboard-add-screen-popout-transparent' : ''}`}, popout);
  }
};

class WidgetSearchBar extends React.Component {
  constructor(props){
    super(props);

    this.updateText = this.updateText.bind(this);
    this.updateSearch = this.updateSearch.bind(this);

    this.state = {searchText: ''};

    this.updateSearch();
  }

  updateText(e){
    this.setState({searchText: e.target.value}, this.updateSearch);
  }

  updateSearch(){
    if(this.state.searchText === "") this.props.updateList(this.props.widgets);
    else {
      let result = fuzzysort.go(this.state.searchText, this.props.widgets, {
        keys: ['name','description', 'customName']
      }).map(result => result.obj);

      this.props.updateList((result.length === 0 ? false : result));
    }
  }

  render(){
    let reactComponents = [];

    reactComponents.push(React.createElement('input', {
      className: 'dashboard-add-screen-search-bar-input',
      onChange: this.updateText,
      placeholder: 'Search...',
      key: 'searchbar'
    }));

    return React.createElement('div', {className: 'dashboard-add-screen-search-bar'}, reactComponents);
  }
}

class WidgetIcon extends React.Component {
  render(){
    let icon = this.props.icon;
    if(icon == undefined) icon = 'assets/images/db_home/widgets/not_found.png';

    if(Array.isArray(icon)){
      let layers = [],
        key = 0;

      icon.forEach((asset, index) => layers.push(
        React.createElement('img', {
          className: 'dashboard-add-screen-widget-overlayed-img',
          src: asset,
          style: {zIndex: index + 1},
          key: `icon-${index}`
        })
      ));

      return React.createElement('div', {className: 'dashboard-add-screen-widget-overlayed-container'}, layers);
    }
    else {
      return React.createElement('img', {
        className: 'dashboard-add-screen-widget-img',
        src: icon,
        key: 'icon'
      });
    }
  }
}
