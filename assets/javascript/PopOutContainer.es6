(function(){
    try {
        window.PopOutContainer = class PopOutContainer extends React.Component {
            constructor() {
                super();
                this.closePopup = this.closePopup.bind(this);
            }

            componentDidMount() {
                if (this.props.dismissable || !this.props.hasOwnProperty('dismissable')){
                    $(document).on('keyup.popup', function(e) {
                        if (e.keyCode == 27) {
                            $(document).off('keyup.popup');
                            if (this.props.closePopup) this.props.closePopup();
                        }
                    }.bind(this));
                }
            }

            closePopup() {
                if (this.props.dismissable || !this.props.hasOwnProperty('dismissable')){
                    $(document).off('keyup.popup');
                    if (this.props.closePopup) this.props.closePopup();
                }
            }

            render() {
                
                var content = React.createElement('div', {className: 'popOutContainerContent ' + this.props.additionalClassName }, this.props.content);
                var backdrop = React.createElement('div', {className: 'popOutContainerBackdrop', onClick: this.closePopup});
                
                return React.createElement('div', {className: 'popOutContainer ' + this.props.additionalPopoutClassName}, backdrop, content);
            }
        };;
    } catch(e) {
        console.log(e);
    }
}());