var DashboardTopBar = class DashboardTopBar extends React.Component {
    render(){
        let reactElements = [],
            key=0;

        let adminChangeTracker;
        adminChangeTracker = React.createElement(ChangeTracker, {changed: this.props.changeTracker.changed, saving: this.props.changeTracker.saving, key: key++});
        let icons = React.createElement(TopBarButtonsContainer, {editMode: this.props.editMode, toggleEditMode: this.props.toggleEditMode, toggleAddMode: this.props.toggleAddMode, toggleDelete: this.props.toggleDelete});
        reactElements.push(React.createElement('div', {key: key++, className: "top-bar-holder no-select"}, adminChangeTracker, icons));

        let editModeWarning = React.createElement('div', {className: `top-bar-edit-mode ${this.props.editMode ? 'top-bar-edit-mode-show' : ''}`}, 'Edit Mode');

        let topBar = React.createElement('div', {className: `top-bar-main ${this.props.editMode ? 'top-bar-main-edit-mode' : ''}`}, reactElements);

        return React.createElement('div', {className: 'top-bar'}, topBar, editModeWarning);
    }
};

class TopBarButtonsContainer extends React.Component {
    constructor(){
        super();
        this.close = this.close.bind(this);
        this.reload = this.reload.bind(this);
    }

    close(){
        //window.histroy.back();
    }

    reload(){
        window.location.reload();
    }

    render(){
        let buttonsArray = [];

        if(this.props.editMode){
            let toggleAddMode = React.createElement('div', {className: 'top-bar-button'}, React.createElement('img', {src:'assets/images/core/icon_plus.png'}));
            buttonsArray.push(React.createElement('div', {key: 'toggleAddMode', className: 'filter-container clickable', onClick: this.props.toggleAddMode },toggleAddMode));

            let deleteButton = React.createElement('div', {className: 'top-bar-button'}, React.createElement('img', {src:'assets/images/core/icon_delete.png'}));
            buttonsArray.push(React.createElement('div', {key: 'deleteButton', className: 'filter-container clickable', onClick: this.props.toggleDelete },deleteButton));
        }

        let editMode = React.createElement('div', {className: 'top-bar-button'}, React.createElement('img', {src:'assets/images/core/icon_cog_white.png'}));
        buttonsArray.push(React.createElement('div', {key: 'editMode', className: 'filter-container clickable', onClick: this.props.toggleEditMode },editMode));

        let reload = React.createElement('div', {className: 'top-bar-button'}, React.createElement('img', {src:'assets/images/core/icon_reload.png'}));
        buttonsArray.push(React.createElement('div', {key: 'reload', className: 'filter-container clickable', onClick: this.reload },reload));

        let close = React.createElement('div', {className: 'top-bar-button'}, React.createElement('img', {src:'assets/images/core/icon_close.png'}));
        //buttonsArray.push(React.createElement('div', {key: 'close', className: 'filter-container clickable', onClick: this.close },close));

        return React.createElement('div', {className: 'top-bar-buttons-container'}, buttonsArray);
    }
}
