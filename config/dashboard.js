var dashboardConfig = {
  "widgets": [
    {
      "name": "GoogleyEyes",
      "description": "Googley Eyes",
      "tags": ["fun"],
      "authorised_module_groups": ["group_user"]
    },
    {
      "name": "ToDo",
      "description": "To Do",
      "tags": ["tools"],
      "authorised_module_groups": ["group_user"]
    },
    {
      "name": "PostIt",
      "authorised_module_groups": ["group_user"],
      "tags": [],
      "description":"PostIt"
    }
  ],
  "editable": true,
  "default": [
    {
      "name":  "PostIt",
      "size": {
        "height": 11,
        "width": 16
      },
      "state":{
        text:"How to create a widget (see README for more info):\n  1. Create a new class which extends the main Widget class.\n\n  2. When creating your widget, there are a few methods you should use that follow our \"Widget lifecycle\" which is based on the React lifecycle which you can read about here: https://reactjs.org/docs/react-component.html\n      - The renderWidget() is where you would place the code that forms the UI of your widget. It must return a valid\n        React Element.\n      - The widgetDidMount() can be used to execute code after a widget has mounted.\n      - The widgetDidUpdate() is called after any 'update' is made to the widget. I.e prop or state change.\n      - The widgetWillUnmount() is called before a widget has unmounted. This can be useful for stopping some processes\n        from running after a widget has been removed.\n      - The onWidgetResize() method is called on first mount, and each time the widget size changes. It is passed an object\n        containing values 'width' and 'height' in pixels.\n      - The shouldWidgetUpdate() method is called before an 'update' (i.e prop/state change) is made. It can be used to allow or block updates that meet some conditions. It must return a boolean (true/false) value.\n      - You can set height/width restrictions on your widget, as well as custom user settings by creating a config object outside of your class example:\n          - NameOfClass.config = {\n            minW: 2,\n            maxW: 5,\n            minH: 4,\n            maxH: 10\n          };\n\n  3. Save your widget class in a .js or .es6 file in the assets/javascript/widgets directory\n  4. Save any stylesheets (.css files) in the assets/stylesheets/widgets directory\n  5. Add your widget to the index.html file\n     - Find the section labeled: 'WIDGETS TO LOAD - ADD NEW WIDGET FILES HERE' in the <head> tag\n     - Add a new <script> tag for your .js / .es6 file\n     - Add a new <link> tag for each/any stylesheets needed (.css files).\n     - Save the index.html file.\n  6. Add your widget config to the config/dashboard.js file.\n     - You should see an object containing 2 arrays: widgets & default\n     - Add any widgets you wish to use in the dashboard to the 'widgets' array. This will ensure they are accessible and show up\n       in the 'Add Widget' window.\n     - If you want your widget to be part of the default dashboard page then add an entry into the 'default' array.\n     - NOTE: when setting the 'size' property of your widget, note that the values are representative of a 'points' / 'grid' system,\n       and are not exact pixel sizes.\n     - Save the dashboard.js file.\n  7. Open index.html in your preferred browser and you are ready to add & use your widget. (FireFox is the recommended.)"
      },
      "userSettings":{"backgroundColor":{"caption":"Background colour","type":"ColourPalette","palette":["ffb3ba","ffdfba","ffffba","baffc9","bae1ff"],"value":"ffffba"},"fontSize":{"type":"SortDropdown","caption":"Font Size","value":12,"index":0,"items":[{"caption":"Small","value":12},{"caption":"Medium","value":16},{"caption":"Large","value":20},{"caption":"Extra Large","value":26}],"remoteList":null,"remoteError":null},"textAlign":{"type":"SortDropdown","caption":"Align Text","value":"left","index":0,"items":[{"caption":"Left","value":"left"},{"caption":"Right","value":"right"},{"caption":"Center","value":"center"}],"remoteList":null,"remoteError":null}}
    },
    {
      "name":  "ToDo",
      "size": {
        "height": 7,
        "width": 6
      }
    },
    {
      "name": "GoogleyEyes",
      "size": {
        "height": 5,
        "width": 6
      }
    }
  ]
};
