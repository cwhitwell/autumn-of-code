# Autumn Of Code

## OnPoint Dashboard - Standalone

## Summary:
  This is an isolated version of the new Dashboard from OnPoint Manager. This is a javascript application running
  on the React library. Most functionality found in this standalone version will be mirrored in the real thing.
  However there are some important differences to note.

## Differences:
  - This is a Front-End only solution (i.e no controller or databases). State saving is done via the HTML5 localStorage API, which may not be compatible with all browsers.
  - No access to Ganymede object. Meaning no access to wrapped ajax calls or locale references.
  - There are only 3 preset widgets to get you started: PostIt, ToDo and GoogleyEyes.
  - No server instance running meaning that all files are accessed locally. This causes some minor issues in Chrome due to the 'file' protocol not being supported by CORS, which means files can not be loaded on the JS side, meaning that all dependancies (e.g CSS/JS files) need to be manually added to index.html.
  - **NOTE:** Due to the issue with CORS/Chrome mentioned above it is advised to use FireFox to run this application. Chrome is still supported but may contain bugs. For example, in Chrome it's not possible to change the colour of GoogleyEyes.

## How to create a widget:
  1. Create a new class which extends the main Widget class.

  2. When creating your widget, there are a few methods you should use that follow our "Widget lifecycle" which is based on the React lifecycle which you can read about here: https://reactjs.org/docs/react-component.html
      - The renderWidget() method is where you would place the code that forms the UI of your widget. It must return a valid React Element.
      - The widgetDidMount() method can be used to execute code after a widget has mounted.
      - The widgetDidUpdate() method is called after any 'update' is made to the widget. I.e prop or state change.
      - The widgetWillUnmount() method is called before a widget has unmounted. This can be useful for stopping some processes from running after a widget has been removed.
      - The onWidgetResize() method is called on first mount, and each time the widget size changes. It is passed an object containing values 'width' and 'height' in pixels.
      - The shouldWidgetUpdate() method is called before an 'update' (i.e prop/state change) is made. It can be used to allow or block updates that meet some conditions. It must return a boolean (true/false) value.
      - You can set height/width restrictions on your widget, as well as custom user settings by creating a config object outside of your class example:
          - NameOfClass.config = {
            minW: 2,
            maxW: 5,
            minH: 4,
            maxH: 10
          };

  3. Save your widget class in a .js or .es6 file in the assets/javascript/widgets directory
  4. Save any stylesheets (.css files) in the assets/stylesheets/widgets directory
  5. Add your widget to the index.html file
     - Find the section labeled: 'WIDGETS TO LOAD - ADD NEW WIDGET FILES HERE' in the <head> tag
     - Add a new <script> tag for your .js / .es6 file
     - Add a new <link> tag for each/any stylesheets needed (.css files).
     - Save the index.html file.
  6. Add your widget config to the config/dashboard.js file.
     - You should see an object containing 2 arrays: widgets & default
     - Add any widgets you wish to use in the dashboard to the 'widgets' array. This will ensure they are accessible and show up in the 'Add Widget' window.
     - If you want your widget to be part of the default dashboard page then add an entry into the 'default' array.
     - Save the dashboard.js file.
     - **NOTE:** when setting the 'size' property of your widget, note that the values are representative of a 'points' / 'grid' system and are not exact pixel sizes.

  7. Open index.html in your web browser and you are ready to add & use your widget. (FireFox is recommended.)
