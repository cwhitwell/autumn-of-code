export default function transformer(file, api) {
  const j = api.jscodeshift;
  const root = j(file.source);

  const CONSTRUCTOR_REPLACE = "getInitialState";
  const SAFE_FUNCTIONS = [
    "constructor",
    "componentWillMount",
    "render",
    "componentDidMount",
    "componentWillUnmount",
    "componentDidCatch",
    "componentWillReceiveProps",
    "shouldComponentUpdate",
    "componentWillUpdate",
    "componentDidUpdate"
  ];
  const REMOVE_LIST = [
    "displayName"
  ];

  const isPropertyFunction = f => {
    return f.type === "Property" && f.value.type === "FunctionExpression";
  };

  const isPropertyObject = o => {
    return o.type === "Property" && o.value.type !== "FunctionExpression";
  };

  const referencesProps = body => {
    return j(body).toSource().indexOf('this.props') !== -1;
  };

  const fixConstructorReturn = s => {
    if(s.type === "ReturnStatement"){
      s = j.expressionStatement(
        j.assignmentExpression(
          "=",
          j.memberExpression(j.thisExpression(), j.identifier("state")),
          s.argument
        )
      );
    }
    return s;
  };

  const buildConstructor = (body, methods, properties) => {
    let methodDeclarations = [];
    let propsRequired = body === null ? false : (referencesProps(body) || properties.some(referencesProps));

    let superLineArguments = propsRequired ? [ j.identifier("props") ] : [];
    let superLine = j.expressionStatement(
      j.callExpression(
      j.super(),
      superLineArguments
    ));

    methods.forEach(m => {
      methodDeclarations.push(j.expressionStatement(
        j.assignmentExpression(
          "=",
          j.memberExpression(
            j.thisExpression(),
            j.identifier(m)
          ),
          j.callExpression(
            j.memberExpression(
              j.memberExpression(
                j.thisExpression(),
                j.identifier(m)),
              j.identifier("bind")),
            [ j.thisExpression() ]
          )
        )
      ));
    });

    let existingBody = body === null ? [] : body.body.body.map(fixConstructorReturn);
    let blockBodyLines = [].concat(superLine, methodDeclarations, properties, existingBody);
    let blockBody = j.blockStatement(blockBodyLines);

    return j.methodDefinition("constructor",
      j.identifier("constructor"),
      j.functionExpression(null,
        (propsRequired? [{ type: "Identifier", name: "props" }] : []),
        blockBody
        )
      );
  };

  const makeProperty = p => {
    let property =  j.expressionStatement(
      j.assignmentExpression("=",
        j.memberExpression(j.thisExpression(), j.identifier(p.key.name)),
    p.value
      )
    );
    return property;
  };

  const buildES6Methods = functions => {
    if(functions.length > 0){
      let methods = [];
      let methodInfo = [];
      let methodsToBind = [];
      let propertiesToDeclare = [];

      // Collate info and prepare list of methods, and prepare properties to be placed in constructor
      functions.forEach(f => {
        let fBody = f.value;
        let fName = f.key.name;
        if(isPropertyFunction(f)){
          if(SAFE_FUNCTIONS.indexOf(fName) === -1 && fName !== CONSTRUCTOR_REPLACE){
            methodsToBind.push(fName);
          }
          methodInfo.push({ name: fName, body: fBody, comments: f.comments });
        }
        if(isPropertyObject(f)){
          if(REMOVE_LIST.indexOf(fName) === -1){
            propertiesToDeclare.push(makeProperty(f));
          }
        }
      });

      let constructorRequired = methodsToBind.length > 0 || propertiesToDeclare.length > 0;
      let constructorCreated = false;
      methodInfo.forEach(f => {
        if(f.name !== CONSTRUCTOR_REPLACE){
          // Standard use case - normal functions
          let method = j.methodDefinition("method",
            j.identifier(f.name),
            f.body
          );
          method.comments = f.comments;
          methods.push(method);
        }
        else {
          // getInitialState is replaced by constructor
          // use `this.state =` instead of return
          // bind each non-react function to if
          methods.push(buildConstructor(f.body, methodsToBind, propertiesToDeclare));
          constructorCreated = true;
        }
      });

      // Ensure constructor if needed
      if(constructorRequired && !constructorCreated){
        methods.unshift(buildConstructor(null, methodsToBind, propertiesToDeclare));
      }

      return methods;
    }
    else {
      return [];
    }
  };

  const getReactSuperClass = (name, body, window) => {
    let ident = j.identifier(name);
    let cBody = j.classBody(body);
    let memberEx = j.memberExpression(
      j.identifier("React"),
      j.identifier("Component")
    );

    if(window){
      return j.classExpression(ident, cBody, memberEx);
    }
    return j.classDeclaration(ident, cBody, memberEx);
  };

  const es6ClassBuilderOld = c => {
    // Produce class body
    let functionsToDeclare = [];
    let functions = j(c.node.declarations[0].init.arguments[0].properties);
    let classBody = buildES6Methods(functions.nodes());

    let classname = c.node.declarations[0].id.name;
    let newClass = getReactSuperClass(classname, classBody);
    return newClass;
  };

  const es6ClassBuilder = c => {
      // Produce class body
      let functionsToDeclare = [];
      let functions = j(c.node.declarations[0].init.arguments[0].properties);
      let classBody = buildES6Methods(functions.nodes());

      let classname = c.node.declarations[0].id.name;

      let classExpression = j.variableDeclaration('var',
          [j.variableDeclarator(
              j.identifier(classname),
              getReactSuperClass(classname, classBody, true)
          )]
      );

      return classExpression;

  };

  const es6WindowClassBuilder = c => {
    // Produce class body
    let functionsToDeclare = [];
    let functions = j(c.node.right.arguments[0].properties);
    let classBody = buildES6Methods(functions.nodes());

    let classname = c.node.left.property.name;
    let windowExpression = j.expressionStatement(
      j.assignmentExpression(
        "=",
        j.memberExpression(
          j.identifier("window"),
          j.identifier(classname)
          ),
        getReactSuperClass(classname, classBody, true)
        )
      );


    return windowExpression;
  };

  // Find React classes created via var React.createClass
  const reactClasses = root.find(j.VariableDeclaration, {
    declarations: [
      {
        type: "VariableDeclarator",
        init: {
          type: "CallExpression",
          callee: {
            type: "MemberExpression",
            object: { name: "React" },
            property: { name: "createClass" }
          }
        }
      }
    ]
  });

  const windowReactClasses = root.find(j.AssignmentExpression, {
    operator: "=",
    left: {
      object: { name: "window" }
    },
    right: {
      callee: {
        object: { name: "React" },
        property: { name: "createClass" }
      }
    }
  });

  reactClasses.replaceWith(es6ClassBuilder);
  windowReactClasses.replaceWith(es6WindowClassBuilder);
  return root.toSource();
}
